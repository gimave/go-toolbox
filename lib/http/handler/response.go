// Package handler é um pacote utilitário para simplificar a utilização de
// handlers HTTP.
package handler

// Response interface que representa as informações que uma struct deve
// ter para ser utilizada como uma resposta HTTP.
type Response interface {
	// CodeSuccess código de status HTTP que será usado em caso de sucesso.
	CodeSuccess() int
	// BodySuccess body da resposta que será usado em caso de sucesso.
	BodySuccess() interface{}
	// BodySuccessBlob body blob da resposta que será usado em caso de sucesso.
	BodySuccessBlob() []byte
	// BodyRedirect body da resposta que será usado em caso de redirect.
	BodyRedirect() string
	// CodeError código de status HTTP que será usado em caso de erro.
	CodeError() int
	// BodyError body da resposta que será usado em caso de erro.
	BodyError() interface{}
	// ContentType tipo de conteúdo da resposta.
	ContentType() string
	// IsBlob define a resposta como blob.
	IsBlob() bool
	// BlobFileName define o nome (e extensão) do blob.
	BlobFileName() string
	// IsRedirect define a resposta como redirect.
	IsRedirect() bool
}

// GenericResponse representa uma resposta HTTP genérica.
type GenericResponse struct {
	codeSuccess     int
	bodySuccess     interface{}
	bodySuccessBlob []byte
	bodyRedirect    string
	codeError       int
	bodyError       interface{}
	contentType     string
	isBlob          bool
	isRedirect      bool
	blobFileName    string
}

// CodeSuccess código de status HTTP que será usado em caso de sucesso.
func (resp *GenericResponse) CodeSuccess() int {
	return resp.codeSuccess
}

// BodySuccess body da resposta que será usado em caso de sucesso.
func (resp *GenericResponse) BodySuccess() interface{} {
	return resp.bodySuccess
}

// BodySuccessBlob body blob da resposta que será usado em caso de sucesso.
func (resp *GenericResponse) BodySuccessBlob() []byte {
	return resp.bodySuccessBlob
}

// BodyRedirect body da resposta que será usado em caso de redirect.
func (resp *GenericResponse) BodyRedirect() string {
	return resp.bodyRedirect
}

// CodeError código de status HTTP que será usado em caso de erro.
func (resp *GenericResponse) CodeError() int {
	return resp.codeError
}

// BodyError body da resposta que será usado em caso de erro.
func (resp *GenericResponse) BodyError() interface{} {
	return resp.bodyError
}

// ContentType tipo de conteúdo da resposta.
func (resp *GenericResponse) ContentType() string {
	return resp.contentType
}

// IsBlob define a resposta como blob.
func (resp *GenericResponse) IsBlob() bool {
	return resp.isBlob
}

// BlobFileName define nome (e extensão) do arquivo blob.
func (resp *GenericResponse) BlobFileName() string {
	return resp.blobFileName
}

// IsRedirect define a resposta como blob.
func (resp *GenericResponse) IsRedirect() bool {
	return resp.isRedirect
}

// NewGenericResponse retorna uma nova instância de GenericResponse.
func NewGenericResponse(codeSuccess int, bodySuccess interface{}, codeError int, bodyError interface{}) *GenericResponse {
	return &GenericResponse{
		codeSuccess: codeSuccess,
		bodySuccess: bodySuccess,
		codeError:   codeError,
		bodyError:   bodyError}
}

// NewResponse retorna uma nova instância de GenericResponse.
// O parâmetro 'err' será usado para definir as informações de 'codeError' e 'bodyError'.
func NewResponse(codeSuccess int, bodySuccess interface{}, err error) *GenericResponse {
	if err == nil {
		return Success(codeSuccess, bodySuccess)
	}

	return Fail(err)
}

// Success retorna uma resposta de sucesso.
func Success(code int, body interface{}) *GenericResponse {
	return &GenericResponse{codeSuccess: code, bodySuccess: body}
}

// Error retorna uma resposta de erro.
func Error(code int, body interface{}) *GenericResponse {
	return &GenericResponse{codeError: code, bodyError: body}
}

// Ok é um atalho para gerar uma resposta de sucesso.
// O código de status HTTP será 200.
func Ok(body interface{}) *GenericResponse {
	return &GenericResponse{codeSuccess: 200, bodySuccess: body}
}

// Fail é um atalho para gerar uma resposta de erro.
// O código de status HTTP e o body(conteúdo) da resposta será tratado
// e definido automaticamente pelo método FromError definido na struct
// Response no package http.
func Fail(err error) *GenericResponse {
	return &GenericResponse{bodyError: err}
}

// SuccessBlob retorna uma resposta de sucesso com blog no body.
func SuccessBlob(code int, contentType string, body []byte, args ...string) *GenericResponse {

	var blobFileName string

	if len(args) > 0 {
		blobFileName = args[0]
	}

	return &GenericResponse{
		codeSuccess:     code,
		bodySuccessBlob: body,
		contentType:     contentType,
		isBlob:          true,
		blobFileName:    blobFileName}
}

// Redirect retorna uma resposta como um redirect
func Redirect(code int, body string) *GenericResponse {
	return &GenericResponse{codeSuccess: code, bodyRedirect: body, isRedirect: true}
}

// Package http contém métodos para facilitar testes automatizados envolvendo o protocolo HTTP.
package http

import (
	"net/http/httptest"

	"github.com/labstack/echo"
)

// EchoRequest faz uma requisição HTTP com o framework echo.
func EchoRequest(service *echo.Echo, method, path string) (int, string) {
	req := httptest.NewRequest(method, path, nil)
	resp := httptest.NewRecorder()
	service.ServeHTTP(resp, req)
	return resp.Code, resp.Body.String()
}

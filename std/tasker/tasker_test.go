package tasker

import (
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestRunSync(t *testing.T) {
	times := 3
	interval := 1 * time.Second

	var wg sync.WaitGroup
	wg.Add(times)

	clockFunc := time.Now()
	clockTask := clockFunc

	result := ""
	task := func() {
		result += "__" + clockTask.Format("15:04:05")
		clockTask = clockTask.Add(interval)
		wg.Done()
	}

	RunSync(task, interval)

	wg.Wait()

	expected := ""
	for i := 1; i <= times; i++ {
		expected += "__" + clockFunc.Format("15:04:05")
		clockFunc = clockFunc.Add(interval)
	}

	assert.Equal(t, expected, result)
}

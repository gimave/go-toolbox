package objectid

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

type StructTest struct {
	Field string
}

func TestGenerateHashFor(t *testing.T) {
	assert.Equal(t, "9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08", GenerateHashFor("test"))
	assert.Equal(t, "8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92", GenerateHashFor(123456))

	structTest := StructTest{Field: "value"}
	assert.Equal(t, "3296f92ec155892f67955876994ca6b4b99d9c548c080bb7914689272f978e1a", GenerateHashFor(structTest))

	sliceStruct := []StructTest{structTest}
	assert.Equal(t, "3a73a83b37ca322108c61f4f31be8886d0086262e1a8afcd1f0b19d1cdeaabd2", GenerateHashFor(sliceStruct))
}

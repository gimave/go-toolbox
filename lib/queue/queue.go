// Package queue é uma implementação simples de uma simulação de um processo de filas.
package queue

import (
	"log"
	"sync/atomic"
	"time"
)

// Task é a tarefa que será processada cada vez que um worker da fila for executado.
//   maxProcesses: o número máximo de itens que devem ser processados a cada execução de um worker.
//   processed: a Task deve retornar a quantidade total de itens que foram processados.
type Task func(maxProcesses uint32, processed chan uint32)

// Queue é a objeto responsável pelo processo de filas.
type Queue struct {
	// o intervalo máximo entre execuções dos workers da fila
	maxInterval uint32
	// o número máximo de itens que devem ser processados a cada execução de um worker
	maxProcesses uint32
	// o número máximo de workers(consumidores) que podem rodar
	maxWorkers uint32

	running bool
}

// New retorna uma nova instância de Queue.
func New(maxInterval, maxProcesses, maxWorkers uint32) *Queue {
	return &Queue{
		maxInterval:  maxInterval,
		maxProcesses: maxProcesses,
		maxWorkers:   maxWorkers,
	}
}

// IsRunning retorna se a fila está em processamento.
func (q *Queue) IsRunning() bool {
	return q.running
}

// Run inicia o processamento da fila.
func (q *Queue) Run(task Task) {
	if q.IsRunning() {
		return
	}

	q.running = true

	var workersRunning uint32
	atomic.StoreUint32(&workersRunning, 0)
	atomic.AddUint32(&workersRunning, 1)

	processedItems := make(chan uint32, q.maxProcesses)
	go task(q.maxProcesses, processedItems)

	for {
		select {
		case processed := <-processedItems:
			go func() {
				ratio := float64(processed) / float64(q.maxProcesses)
				if ratio < 0 || ratio > 1 {
					ratio = 0
				}

				interval := (float64(q.maxInterval) * (1 - ratio))
				time.Sleep(time.Duration(interval) * time.Second)

				go task(q.maxProcesses, processedItems)
			}()

		case <-time.After(time.Duration(q.maxInterval*10) * time.Second):
			if atomic.LoadUint32(&workersRunning) >= q.maxWorkers {
				log.Println("queue: o máximo de workers para essa fila já está em execução")
			} else {
				atomic.AddUint32(&workersRunning, 1)
				go task(q.maxProcesses, processedItems)
			}
		}
	}
}

package telemetry

import (
	"fmt"
	"os"
	"path/filepath"
	"sync"
	"time"
)

// FileStorage é um serviço que armazena os dados coletados num arquivo
// texto. Esse serviço armazena os dados em buffer e faz o flush
// a cada 15 segundos para escrever os dados no arquivo texto.
type FileStorage struct {
	dirname    string
	isFlushing bool
	buffer     SyncBuffer
}

// NewFileStorage retorna uma nova instância de FileStorage.
// O parâmetro dirname define o diretório onde os arquivos serão
// armazenados.
func NewFileStorage(dirname string) *FileStorage {
	return &FileStorage{dirname: dirname}
}

// startFlushLoop inicia o loop de flush para escrever os dados do
// buffer para o arquivo texto.
// A cada 15 segundos, os dados que estão armazenados no buffer são
// escritos num arquivo texto e o buffer é resetado.
func (f *FileStorage) startFlushLoop() {
	go func(f *FileStorage) {
		ticker := time.NewTicker(15 * time.Second)

		for range ticker.C {
			var mu sync.Mutex

			mu.Lock()
			defer mu.Unlock()

			data := f.buffer.Bytes()
			f.buffer.Reset()

			if len(data) > 0 {
				var (
					file *os.File
					err  error
				)

				name := fmt.Sprintf("%s_%s.json", config.app, time.Now().Format("2006-01-02-15"))
				filename := filepath.Join(f.dirname, name)

				if file, err = os.OpenFile(filename, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0664); err != nil {
					return
				}
				defer file.Close()

				file.Write(data)
			}
		}
	}(f)
}

// Write armazena os dados no buffer.
func (f *FileStorage) Write(p []byte) (n int, err error) {
	if !f.isFlushing {
		var mu sync.Mutex
		mu.Lock()
		if !f.isFlushing {
			f.startFlushLoop()
			f.isFlushing = true
		}
		mu.Unlock()
	}

	p = append(p, []byte("\n")...)
	return f.buffer.Write(p)
}

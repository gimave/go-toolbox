package telemetry

import (
	"fmt"
	"reflect"
	"strings"
	"time"

	"bitbucket.org/eucatur/go-toolbox/lib/timetracker"
)

var tracingNullInstance = new(tracingNull)

// Tracing a interface que define um serviço de rastreamento.
type Tracing interface {
	// SetName define o nome do objeto que gerou as informações.
	SetName(name string) Tracing
	// SetTags define as tags para o rastreamento.
	SetTags(tags Args) Tracing
	// Input define os argumentos de entrada.
	Input(args Args) Tracing
	// Output define os argumentos de retorno.
	Output(args Args)
}

// tracingNull é um serviço nulo de tracing(rastreamento).
// Esse serviço é usado quando o package está desabilitado; o serviço
// foi otimizado para não consumir nenhum recurso de processamento.
type tracingNull int

func newTracingNull(t Telemetry) Tracing {
	return tracingNullInstance
}

func (t *tracingNull) SetName(name string) Tracing {
	return t
}

func (t *tracingNull) SetTags(tags Args) Tracing {
	return t
}

func (t *tracingNull) Input(args Args) Tracing {
	return t
}

func (t *tracingNull) Output(args Args) {
	return
}

// tracingFunc é um serviço de tracing(rastreamento) de funções.
type tracingFunc struct {
	running bool
	data    Args
	meta    metadata

	tracker   *timetracker.TimeTracker
	telemetry Telemetry
}

func newTracingFunc(t Telemetry) Tracing {
	meta := extractMetadataFrom(4)
	data := Args{
		"index": time.Now().UnixNano(),
		"name":  meta.funcName,
		"tags":  Args{},
	}

	return &tracingFunc{data: data, meta: meta, telemetry: t}
}

func (t *tracingFunc) SetName(name string) Tracing {
	t.data["name"] = name
	return t
}

func (t *tracingFunc) SetTags(tags Args) Tracing {
	t.data["tags"] = tags
	return t
}

func (t *tracingFunc) Input(args Args) Tracing {
	t.data["input"] = args
	t.tracker = timetracker.Start()
	t.running = true
	return t
}

func (t *tracingFunc) Output(args Args) {
	if !t.running {
		return
	}

	t.tracker.Stop()

	val := reflect.ValueOf(args["err"])
	args["err"] = nil

	if val.IsValid() {
		if val.Kind() == reflect.Ptr {
			val = val.Elem()
		}

		if val.Kind() != reflect.Interface || val.Kind() == reflect.Interface && !val.IsNil() {
			msg := strings.TrimSpace(fmt.Sprintf("%v", val.Interface()))
			if msg != "" {
				args["err"] = msg
			}
		}
	}

	// Se o package está rodando em ModeError(modo de erro) mas a função não
	// gerou nenhum erro, o processo é interrompido.
	if config.mode == ModeError && args["err"] == nil {
		return
	}

	t.data["output"] = args

	t.data["meta"] = Args{
		"file": t.meta.fileName,
		"line": t.meta.fileLine,
		"time": Args{
			"start":    t.tracker.StartedAt(),
			"end":      t.tracker.StoppedAt(),
			"duration": t.tracker.Duration().String(),
		},
	}

	t.running = false

	// Envia as informações coletadas para serviço de storage.
	t.telemetry.SendDataToStorage("tracing", "func", t.data)
}

package http

import (
	"log"

	"github.com/labstack/echo"
)

// MapRoutesGroup um mapa do grupo de rotas.
type MapRoutesGroup map[string]*echo.Group

// MakeRoutesGroup constrói e retorna uma nova instância de RoutesGroup.
//
//  // cria um grupo de rotas no echo
//  v1 := e.Group("v1")
//  // adiciona o grupo de rotas no routesGroup e retorna a instância
//  routesGroup := MakeRoutesGroup("v1", v1)
//
func MakeRoutesGroup(name string, group *echo.Group) *RoutesGroup {
	return NewRoutesGroup(MapRoutesGroup{name: group})
}

// NewRoutesGroup retorna uma nova instância de RoutesGroup.
//
//  routesGroup := NewRoutesGroup()
//
func NewRoutesGroup(mapAll ...MapRoutesGroup) *RoutesGroup {
	routesGroup := &RoutesGroup{make(MapRoutesGroup)}

	for _, all := range mapAll {
		for name, group := range all {
			routesGroup.AddGroup(name, group)
		}
	}

	return routesGroup
}

// RoutesGroup é reponsável por centralizar todos os grupos de rotas disponíveis.
type RoutesGroup struct {
	groups MapRoutesGroup
}

// AddGroup adiciona um grupo de rotas ao routesGroup.
//
//  // cria um grupo de rotas no echo
//  v1 := e.Group("v1")
//  // adiciona o grupo de rotas no routesGroup
//  routesGroup.AddGroup("v1", v1)
//
func (r *RoutesGroup) AddGroup(name string, group *echo.Group) *RoutesGroup {
	if _, ok := r.groups[name]; ok {
		log.Panicf("http: o grupo [%s] já está definido na lista de rotas", name)
	}

	r.groups[name] = group
	return r
}

// Group acessa o grupo de rotas com o nome 'name'.
//
//  v1 := routesGroup.Group("v1")
//
func (r *RoutesGroup) Group(name string) *echo.Group {
	var (
		group *echo.Group
		ok    bool
	)

	if group, ok = r.groups[name]; !ok {
		log.Panicf("http: o grupo [%s] não está definido nas rotas", name)
	}

	return group
}

// AllGroups acessa todos os grupos de rotas.
//
//  grupos := routesGroup.AllGroups()
//
func (r *RoutesGroup) AllGroups() MapRoutesGroup {
	return r.groups
}

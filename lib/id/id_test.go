package id

import (
	"testing"

	generateuuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
)

func TestGenerate(t *testing.T) {
	var (
		id     string
		uuidV4 generateuuid.UUID
		err    error
	)

	id, err = Generate(Options{Type: "invalid"})
	assert.Empty(t, id)
	assert.Error(t, err)

	id, err = Generate(Options{Type: "invalid", Fallback: "invalid"})
	assert.Empty(t, id)
	assert.Error(t, err)

	id, err = Generate(Options{Type: "invalid", Fallback: "number"})
	assert.Empty(t, id)
	assert.Error(t, err)

	id, err = Generate(Options{Type: "number"})
	assert.NotEmpty(t, id)
	assert.Nil(t, err)

	id, err = Generate(Options{Type: "uuid"})
	assert.NotEmpty(t, id)
	assert.Nil(t, err)

	uuidV4, err = generateuuid.FromString(id)
	assert.Equal(t, id, uuidV4.String())
	assert.Nil(t, err)

	id, err = Generate(Options{Type: "uuid", Fallback: "number"})
	assert.NotEmpty(t, id)
	assert.Nil(t, err)
}

func TestMustGenerate(t *testing.T) {
	var (
		id     string
		uuidV4 generateuuid.UUID
		err    error
	)

	assert.Panics(t, func() { MustGenerate(Options{Type: "invalid"}) })
	assert.Panics(t, func() { MustGenerate(Options{Type: "invalid", Fallback: "invalid"}) })
	assert.Panics(t, func() { MustGenerate(Options{Type: "invalid", Fallback: "number"}) })

	assert.NotEmpty(t, MustGenerate(Options{Type: "number"}))

	id = MustGenerate(Options{Type: "uuid"})
	assert.NotEmpty(t, id)

	uuidV4, err = generateuuid.FromString(id)
	assert.Equal(t, id, uuidV4.String())
	assert.Nil(t, err)

	assert.NotEmpty(t, MustGenerate(Options{Type: "uuid", Fallback: "number"}))
}

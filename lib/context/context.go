// Package context é utilizado para passar parâmetros do contexto atual da execução
// de uma função.
package context

import (
	"bitbucket.org/eucatur/go-toolbox/lib/telemetry"
)

// Options são as opções de parâmetros que um contexto pode conter.
type Options struct {
	// Telemetry serviço de telemetria da aplicação.
	Telemetry telemetry.Telemetry
}

// Context responsável pelas informações do contexto.
type Context struct {
	options Options
}

// New instância um novo contexto de acordo com as opções definidas em options.
//
// Exemplo
//   // inicia o serviço de telemetria
//   t := telemetry.New()
//   // inicia o context
//   ctx := context.New(context.Options{Telemetry: t})
//
func New(options Options) *Context {
	return &Context{options}
}

// Telemetry retorna o serviço de telemetria definido no contexto.
func (c *Context) Telemetry() telemetry.Telemetry {
	return c.options.Telemetry
}

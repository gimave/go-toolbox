package debuggerconfig

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"

	"bitbucket.org/eucatur/go-toolbox/lib/debugger"
	"bitbucket.org/eucatur/go-toolbox/lib/debugger/debuggerutil"
)

type config struct {
	title  string
	tables []*table
}

type table struct {
	colList  []Col
	subtitle string
	cols     string
	contents []string
}

type Col struct {
	name  string
	width int
}

var (
	configs    = make(map[string]*config)
	RegexLines = regexp.MustCompile(`\r\n?|\n`)
)

func Set(name, title string) (c *config) {
	c, exists := configs[name]

	if !exists {
		c = create(name)
	}

	c.title = title

	configs[name] = c

	return
}

func ByName(name string) (c *config) {
	c, exists := configs[name]

	if !exists {
		c = create(name)
	}

	return
}

func mergeLineSeparator(word string) string {
	lineSeparatorBytes := []byte(debuggerutil.LineSeparator())
	wordBytes := []byte(" " + word + " ")

	lineSeparatorBytesLen := len(lineSeparatorBytes)
	wordBytesLen := len(wordBytes)

	if wordBytesLen > lineSeparatorBytesLen {
		wordBytesLen = lineSeparatorBytesLen
	}

	copy(lineSeparatorBytes[lineSeparatorBytesLen/2-wordBytesLen/2:], wordBytes)

	return string(lineSeparatorBytes)

}

func (c *config) Table(subtitle string, cols ...Col) (t *table) {
	items := []string{}
	for _, col := range cols {
		width := strconv.Itoa(col.width)
		items = append(items, fmt.Sprintf(
			"% -"+width+"s",
			col.name,
		))
	}

	t = &table{
		colList:  cols,
		subtitle: mergeLineSeparator(subtitle),
		cols:     fmt.Sprint(debuggerutil.Header(items...)),
	}

	c.tables = append(c.tables, t)

	return
}

func Column(name interface{}, width int) Col {
	return Col{fmt.Sprintf("%+v", name), width}
}

func (t *table) AddRow(contents ...interface{}) *table {
	rows := [][]string{}
	for colIndex, value := range contents {
		content := fmt.Sprintf("%+v", value)

		colCells := RegexLines.Split(content, -1)

		for cellIndex, cell := range colCells {
			if len(cell) > t.colList[colIndex].width {
				cell = cell[:Abs(t.colList[colIndex].width-4)] + "..."
			}

			cell = fmt.Sprintf(
				"% -"+strconv.Itoa(t.colList[colIndex].width)+"v",
				cell,
			)

			if cellIndex > len(rows)-1 {
				cols := make([]string, len(t.colList))

				//Preencher colunas com espaços
				for i := range cols {
					if i == colIndex {
						continue
					}

					cols[i] = fmt.Sprintf(
						"% "+strconv.Itoa(t.colList[i].width)+"v", "",
					)
				}

				cols[colIndex] = cell
				rows = append(rows, cols)
			} else {
				cols := rows[cellIndex]
				cols[colIndex] = cell
				rows[cellIndex] = cols
			}
		}
	}

	rowsContent := []string{}
	for _, row := range rows {
		content := fmt.Sprint(" ", strings.Join(row, " | "))
		rowsContent = append(rowsContent, content)
	}

	t.contents = append(t.contents, debuggerutil.Content(strings.Join(rowsContent, "\n")))

	return t
}

func (c *config) Print() {
	contents := debuggerutil.Title(c.title)

	for _, table := range c.tables {
		contents += table.subtitle + table.cols
		contents += strings.Join(table.contents, "")
	}

	debugger.Debug(contents)
}

func create(name string) *config {
	return &config{
		tables: make([]*table, 0),
	}
}

func Abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

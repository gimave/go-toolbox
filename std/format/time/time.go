// Package time é um pacote com funções específicas para formatação do tipo time(horas).
package time

import (
	"fmt"
	"time"
)

// HourMin formata um objeto time.Duration para uma string no
// formato hora e minutos(HH:II).
//
//  horMin := HourMin(75 * time.Minute)  // "01:15"
//
func HourMin(tm time.Duration) string {
	return fmt.Sprintf("%02d:%02d", int64(tm.Hours()), int64(tm.Minutes())%60)
}

// Default formata um objeto time.Time para uma string no
// formato padrão: hora, minutos e segundos(HH:II:SS).
//
//  hora := Default(data)  // "20:30:45"
//
func Default(dt time.Time) string {
	return dt.Format("15:04:05")
}

// Short formata uma string no formato horas, minutos e segundos(HH:II:SS)
// para uma string contendo apenas as horas e os minutos(HH:II).
//
//  horMin := Short("20:30:45")  // "20:30"
//
func Short(tm string) string {
	return tm[0:5]
}

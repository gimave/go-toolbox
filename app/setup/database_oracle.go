// Package setup faz todo o processo de configuração e preparação de uma app.
//
// A configuração '+build oracle' na linha abaixo é para informar ao compilador
// para incluir esse arquivo 'database_oracle.go' somente se a tag 'oracle'
// for informada no 'go run' ou 'go build'.
// Exemplos:
//     go run -tags oracle api.go -porta=9001 -debug=true
//     go build -tags oracle api.go -porta=9001 -debug=true

// +build oracle

package setup

import (
	// registra o driver para o oracle
	_ "github.com/mattn/go-oci8"
)

// Package middleware para aglomerar os middlewares padrões de que podem ser usados em qualquer API
package middleware

import (
	"encoding/json"
	"errors"
	"io/ioutil"

	"bitbucket.org/eucatur/go-toolbox/std/log"
	"github.com/labstack/echo"
)

// Usado para fazer Basic Authorization utilizando o credentials.json,
// onde o mesmo que tem os dados de usuarios e senhas para fazer a autenticação
//
// Exemplo:
// auth.Use(middleware.BasicAuth(middlewareToolbox.BasicAuthorization))
//
// Exemplo do credentials.json
//
// [
//   {"username":"site", "password":"eucatur"}
// ]

// ID, USERNAME, PASSWORD parâmetros de autenticação
var (
	ID       = "ID"
	USERNAME = "Username"
	PASSWORD = "Password"
)

// BasicAuthorization faz a autenticação utilizando o método Basic
func BasicAuthorization(username, password string, c echo.Context) (ok bool, err error) {
	source, err := ioutil.ReadFile("credentials.json")
	if err != nil {
		log.Error(err)
		panic(errors.New("Error in find .crendential file"))
	}

	type Credential struct {
		ID       string `json:"id"`
		Username string `json:"username"`
		Password string `json:"password"`
	}

	var credentials []Credential

	err = json.Unmarshal(source, &credentials)

	if err != nil {
		log.Error(err)
		panic(errors.New("Error in Unmarshal crendentials.json"))
	}

	for _, credential := range credentials {
		if username == credential.Username && password == credential.Password {
			c.Set(ID, credential.ID)
			c.Set(USERNAME, credential.Username)
			c.Set(PASSWORD, credential.Password)

			return true, nil
		}
	}

	return false, nil
}

// Package query disponibiliza métodos para a execução de queries no banco de dados.
// O objetivo desse pacote é simplificar a utilização das queries mais comuns.
// Exemplos:
//
//  // executando um select no banco slave(leitura)
//  err := query.Select(`SELECT ....`, filtro, &resultado)
//
//  // executando um select numa transação (tx)
//  err := query.Tx(tx).Select(`SELECT ....`, filtro, &resultado)
//
//  // executando um select numa conexão específica (conn)
//  err := query.Conn(conn).Select(`SELECT ....`, filtro, &resultado)
//
package query

import (
	"bitbucket.org/eucatur/go-toolbox/lib/database"

	"github.com/jmoiron/sqlx"
)

// Filter interface para um objeto que representa uma cláusula WHERE
// para ser executada no comando SQL.
type Filter interface {
	// GetQuery retorna a cláusula WHERE para ser usado em uma query.
	GetQuery() string
}

// Tx executa o comando SQL dentro da transação 'tx'.
//
//  err := query.Tx(tx).Select(`SELECT ....`, filtro, &resultado)
//
func Tx(tx *sqlx.Tx) Commands {
	return Commands{tx}
}

// Conn executa o comando SQL dentro da conexão 'conn'.
//
//  err := query.Conn(conn).Select(`SELECT ....`, filtro, &resultado)
//
func Conn(conn *sqlx.DB) Commands {
	return Commands{conn}
}

// Select executa um select na conexão slave.
//
//  err := query.Select(`SELECT ....`, filtro, &resultado)
//
func Select(query string, filter Filter, pointerToSlice interface{}) error {
	return Conn(database.Slave()).Select(query, filter, pointerToSlice)
}

// Connection interface que representa os métodos do sqlx que essa lib precisa.
type Connection interface {
	NamedQuery(query string, arg interface{}) (*sqlx.Rows, error)
}

// Commands responsável pela execução dos comandos SQL no banco de dados.
type Commands struct {
	conn Connection
}

// Select executa o SQL 'query' no banco de dados; o parâmetro 'filter' será usado
// para montar a cláusula WHERE; o resultado da consulta será gravado em 'pointerToSlice'.
//
//  err := query.Select(`SELECT ....`, filtro, &resultado)
//
func (c Commands) Select(query string, filter Filter, pointerToSlice interface{}) error {
	var (
		rows *sqlx.Rows
		err  error
	)

	queryWithFilter := query + filter.GetQuery()

	if rows, err = c.conn.NamedQuery(queryWithFilter, filter); err == nil {
		err = sqlx.StructScan(rows, pointerToSlice)
	}

	return err
}

// Package setup faz todo o processo de configuração e preparação de uma app.
//
package setup

import (
	"fmt"
	"log"
	"regexp"
	"strings"
	"time"

	"bitbucket.org/eucatur/go-toolbox/app/env"
	libdatabase "bitbucket.org/eucatur/go-toolbox/lib/database"
	"bitbucket.org/eucatur/go-toolbox/lib/debugger"
	"bitbucket.org/eucatur/go-toolbox/lib/debugger/debuggerutil"
	"bitbucket.org/eucatur/go-toolbox/lib/text"
	stdstrings "bitbucket.org/eucatur/go-toolbox/std/strings"

	"github.com/jmoiron/sqlx"

	// registra o driver para o mysql
	_ "github.com/go-sql-driver/mysql"
	// registra o driver para o postgres
	_ "github.com/lib/pq"
)

// Para realizar a conexão com o banco de dados, é necessário definir no env as chaves com as configurações.
//
// As opções de configuração são:
//   - DRIVER=mysql     (opcional, padrão: mysql)   -> driver do banco
//   - PARSETIME=false  (opcional, padrão: true)    -> cria uma segunda conexão com a opção parseTime ativa
//   - USUARIO=user     (obrigatório)               -> usuário do banco
//   - SENHA=pass       (obrigatório)               -> senha do banco
//   - HOST=127.0.0.1   (obrigatório)               -> ip ou host do banco
//   - PORTA=1234       (obrigatório)               -> porta do banco
//   - BANCO=db         (obrigatório)               -> banco padrão
//
// Primeiro, é necessário definir a conexão padrão (default), informe no env o grupo de chaves abaixo:
//              MASTER                            SLAVE
//  ----------------------------------|----------------------------------
//  API_DB_MASTER_DRIVER=mysql        |     API_DB_SLAVE_DRIVER=mysql
//  API_DB_MASTER_PARSETIME=false     |     API_DB_SLAVE_PARSETIME=false
//  API_DB_MASTER_USUARIO=user        |     API_DB_SLAVE_USUARIO=user
//  API_DB_MASTER_SENHA=pass          |     API_DB_SLAVE_SENHA=pass
//  API_DB_MASTER_HOST=127.0.0.1      |     API_DB_SLAVE_HOST=127.0.0.1
//  API_DB_MASTER_PORTA=1234          |     API_DB_SLAVE_PORTA=1234
//  API_DB_MASTER_BANCO=db            |     API_DB_SLAVE_BANCO=db
//
// Para configurar alguma conexão adicional, informe um identificador para a conexão logo após o
// prefixo API_DB_. Por exemplo, para criar uma conexão identificada por "RELATORIO", declare no env:
//                   MASTER                                        SLAVE
//  --------------------------------------------|--------------------------------------------
//  API_DB_RELATORIO_MASTER_DRIVER=mysql        |     API_DB_RELATORIO_SLAVE_DRIVER=mysql
//  API_DB_RELATORIO_MASTER_PARSETIME=false     |     API_DB_RELATORIO_SLAVE_PARSETIME=false
//  API_DB_RELATORIO_MASTER_USUARIO=user        |     API_DB_RELATORIO_SLAVE_USUARIO=user
//  API_DB_RELATORIO_MASTER_SENHA=pass          |     API_DB_RELATORIO_SLAVE_SENHA=pass
//  API_DB_RELATORIO_MASTER_HOST=127.0.0.1      |     API_DB_RELATORIO_SLAVE_HOST=127.0.0.1
//  API_DB_RELATORIO_MASTER_PORTA=1234          |     API_DB_RELATORIO_SLAVE_PORTA=1234
//  API_DB_RELATORIO_MASTER_BANCO=db            |     API_DB_RELATORIO_SLAVE_BANCO=db
//

// Regex para buscar as chaves no env no padrão: API_DB_ + "NOME_CONEXAO" + _MASTER ou SLAVE_ + "CONFIG".
// Exemplo: API_DB_RELATORIO_SLAVE_DRIVER.
var regexEnvKey = regexp.MustCompile(`^API_DB_([A-Z]+)_(MASTER|SLAVE)_[A-Z]+$`)

// Prefixo padrão para as chaves no env.
const prefixEnvKey = "API_DB_"

// dbConfig as configurações para o banco de dados.
type dbConfig struct {
	Driver    string
	ParseTime bool
	Host      string
	Usuario   string
	Senha     string
	Porta     int
	Banco     string
}

// allDBConnections todas as conexões possíveis com um banco.
type allDBConnections struct {
	master          *sqlx.DB
	slave           *sqlx.DB
	masterParseTime *sqlx.DB
	slaveParseTime  *sqlx.DB
}

// dbConnections responsável pelos procedimentos de conexão.
type dbConnections struct{}

// ConfigureDefaultConnection configura a conexão padrão (default) com o banco de dados.
func (c *dbConnections) ConfigureDefaultConnection() {
	var (
		master dbConfig
		slave  dbConfig
	)

	switch {
	// A opção abaixo é a forma nova de estabelecer uma
	// conexão padrão(default) master e/ou slave com o banco de dados.
	case env.String("API_DB_MASTER_HOST", "") != "" || env.String("API_DB_SLAVE_HOST", "") != "":
		master = c.makeConfigFromEnv("", "MASTER")
		slave = c.makeConfigFromEnv("", "SLAVE")

	// As duas opções abaixo são apenas para manter compatibilidade com
	// as APIs antigas que ainda usam essas configurações.
	//
	// Monta a configuração para o banco de dados padrão(default)
	// master se a tag API_MYSQL_MASTER_IP não estiver vazia.
	case env.String("API_MYSQL_MASTER_IP", "") != "":
		master = dbConfig{
			Driver:    "mysql",
			ParseTime: true,
			Host:      env.MustString("API_MYSQL_MASTER_IP"),
			Usuario:   env.MustString("API_MYSQL_MASTER_USUARIO"),
			Senha:     env.MustString("API_MYSQL_MASTER_SENHA"),
			Porta:     env.MustInt("API_MYSQL_MASTER_PORTA"),
			Banco:     env.MustString("API_MYSQL_MASTER_BANCO"),
		}
		fallthrough

	// Monta a configuração para o banco de dados padrão(default)
	// slave se a tag API_MYSQL_SLAVE_IP não estiver vazia.
	case env.String("API_MYSQL_SLAVE_IP", "") != "":
		slave = dbConfig{
			Driver:    "mysql",
			ParseTime: true,
			Host:      env.MustString("API_MYSQL_SLAVE_IP"),
			Usuario:   env.MustString("API_MYSQL_SLAVE_USUARIO"),
			Senha:     env.MustString("API_MYSQL_SLAVE_SENHA"),
			Porta:     env.MustInt("API_MYSQL_SLAVE_PORTA"),
			Banco:     env.MustString("API_MYSQL_SLAVE_BANCO"),
		}
	}

	conn := c.createConnections(master, slave)

	libdatabase.Configure(conn.master, conn.slave, conn.masterParseTime, conn.slaveParseTime)

	debugDatabase("", master, slave)
}

// ConfigureAdditionalConnections configura as conexões adicionais com o banco.
func (c *dbConnections) ConfigureAdditionalConnections() {
	type connections struct {
		name string
		kind string
	}

	all := make(map[string]map[string]bool, 0)

	keysParsed := []string{}

	for envKey := range env.All() {
		index := strings.LastIndex(envKey, "_")
		if index < 1 {
			continue
		}

		key := envKey[:index]

		if stdstrings.ExistsInSlice(key, keysParsed) {
			continue
		}
		keysParsed = append(keysParsed, key)

		result := regexEnvKey.FindAllStringSubmatch(envKey, -1)

		if len(result) > 0 && len(result[0]) == 3 {
			name := result[0][1]
			kind := result[0][2]

			if _, ok := all[name]; !ok {
				all[name] = map[string]bool{kind: true}
			} else {
				all[name][kind] = true
			}
		}
	}

	for name, kind := range all {
		var (
			master dbConfig
			slave  dbConfig
		)

		if _, ok := kind["MASTER"]; ok {
			master = c.makeConfigFromEnv(name, "MASTER")
		}

		if _, ok := kind["SLAVE"]; ok {
			slave = c.makeConfigFromEnv(name, "SLAVE")
		}

		conn := c.createConnections(master, slave)

		libdatabase.Add(name, conn.master, conn.slave, conn.masterParseTime, conn.slaveParseTime)

		debugDatabase(name, master, slave)
	}
}

// formatEnvKey formata a chave do env no padrão: API_DB_SLAVE_DRIVER ou API_DB_RELATORIO_SLAVE_DRIVER.
func (c *dbConnections) formatEnvKey(name, kind, option string) string {
	if name == "" {
		return fmt.Sprintf("%s%s_%s", prefixEnvKey, strings.ToUpper(kind), strings.ToUpper(option))
	}
	return fmt.Sprintf("%s%s_%s_%s", prefixEnvKey, strings.ToUpper(name), strings.ToUpper(kind), strings.ToUpper(option))
}

// makeConfigFromEnv monta um arquivo de configuração buscando os dados do env.
func (c *dbConnections) makeConfigFromEnv(name, kind string) dbConfig {
	host := env.String(c.formatEnvKey(name, kind, "HOST"), "")

	if host == "" {
		return dbConfig{}
	}

	return dbConfig{
		// Exemplos: API_DB_MASTER_DRIVER / API_DB_RELATORIO_MASTER_DRIVER
		Driver: env.String(c.formatEnvKey(name, kind, "DRIVER"), "mysql"),

		// Exemplos: API_DB_SLAVE_PARSETIME / API_DB_RELATORIO_SLAVE_PARSETIME
		ParseTime: env.Bool(c.formatEnvKey(name, kind, "PARSETIME"), true),

		// Exemplos: API_DB_MASTER_HOST / API_DB_RELATORIO_MASTER_HOST
		Host: host,

		// Exemplos: API_DB_SLAVE_USUARIO / API_DB_RELATORIO_SLAVE_USUARIO
		Usuario: env.MustString(c.formatEnvKey(name, kind, "USUARIO")),

		// Exemplos: API_DB_MASTER_SENHA / API_DB_RELATORIO_MASTER_SENHA
		Senha: env.MustString(c.formatEnvKey(name, kind, "SENHA")),

		// Exemplos: API_DB_SLAVE_PORTA / API_DB_RELATORIO_SLAVE_PORTA
		Porta: env.MustInt(c.formatEnvKey(name, kind, "PORTA")),

		// Exemplos: API_DB_MASTER_BANCO / API_DB_RELATORIO_MASTER_BANCO
		Banco: env.MustString(c.formatEnvKey(name, kind, "BANCO")),
	}
}

// createConnectionsPool cria o pool de conexão com o banco.
func (c *dbConnections) createConnectionsPool(config dbConfig, parseTime bool) *sqlx.DB {
	// Por padrão, os tipos "date", "datetime" e "timestamp" são convertidos para string.
	// Quando a opção parseTime é usada no dsn de conexão com o banco de dados, esses
	// campos serão convertidos para o tipo time.Time.
	optParseTime := ""
	if parseTime {
		optParseTime = "?parseTime=true"
	}

	var (
		dsn    string
		driver string
	)

	switch config.Driver {
	case "mysql":
		// "USUARIO:SENHA@tcp(HOST:PORTA)/BANCO"
		// Exemplo: "usuario:senha@tcp(127.0.0.1:3306)/banco?parseTime=true"
		dsn = fmt.Sprintf("%s:%s@tcp(%s:%d)/%s%s",
			config.Usuario,
			config.Senha,
			config.Host,
			config.Porta,
			config.Banco,
			optParseTime,
		)
		driver = "mysql"

	case "oracle":
		// "USUARIO/SENHA@HOST:PORTA/BANCO"
		// Exemplo: "usuario/senha@localhost:1521/ORCL"
		dsn = fmt.Sprintf("%s/%s@%s:%d/%s%s",
			config.Usuario,
			config.Senha,
			config.Host,
			config.Porta,
			config.Banco,
			optParseTime,
		)
		driver = "oci8"

	case "postgres":
		dsn = fmt.Sprintf("user=%s port=%d password=%s host=%s dbname=%s sslmode=disable",
			config.Usuario,
			config.Porta,
			config.Senha,
			config.Host,
			config.Banco,
		)
		driver = "postgres"

	default:
		log.Panicf("app:setup: o driver do banco '%s' não é suportado", config.Driver)
	}

	conn, err := sqlx.Connect(driver, dsn)

	if err != nil {
		log.Panic(err)
	}

	conn.SetMaxOpenConns(500)
	conn.SetMaxIdleConns(5)
	conn.SetConnMaxLifetime(2 * time.Minute)

	return conn
}

// createConnections cria as conexões com o banco de dados.
func (c *dbConnections) createConnections(master, slave dbConfig) allDBConnections {
	conn := allDBConnections{}

	if master.Host != "" {
		conn.master = c.createConnectionsPool(master, false)
		if master.ParseTime {
			conn.masterParseTime = c.createConnectionsPool(master, true)
		}
	}

	if slave.Host != "" {
		conn.slave = c.createConnectionsPool(slave, false)
		if slave.ParseTime {
			conn.slaveParseTime = c.createConnectionsPool(slave, true)
		}
	}

	return conn
}

// debugDatabase exibe as informações de configuração do banco de dados
// se o debugger da aplicação estiver ligado.
func debugDatabase(name string, master, slave dbConfig) {
	msg := debuggerutil.Title(fmt.Sprintf("BANCO DE DADOS: %s", name))
	msg += debuggerutil.Header("DRIVER    ", "TIPO    ", "HOST                  ", "PORTA", "USUARIO   ", "BANCO")

	format := func(kind string, config dbConfig) string {
		if config.Host == "" {
			return ""
		}

		return debuggerutil.Contentf(" %s | %s | %s | %d  | %s | %s\n",
			text.RPad(config.Driver, 10, " "),
			text.RPad(kind, 8, " "),
			text.RPad(config.Host, 22, " "),
			config.Porta,
			text.RPad(config.Usuario, 10, " "),
			config.Banco)
	}

	msg += format("master", master)
	msg += format("slave", slave)

	debugger.Debug(msg)
}

package nsq

import (
	"log"

	"bitbucket.org/eucatur/go-toolbox/app/env"

	"github.com/bitly/go-nsq"
)

var (
	producer *nsq.Producer
	nsqURL   string
)

func Configure() {
	if producer == nil {

		var (
			nsqIP, nsqPorta string
			err             error
		)

		if nsqIP, err = env.ReqString("API_NSQ_IP"); err != nil {
			log.Panic(err)
		}

		if nsqPorta, err = env.ReqString("API_NSQ_PORTA"); err != nil {
			log.Panic(err)
		}

		nsqURL = nsqIP + ":" + nsqPorta
		config := nsq.NewConfig()

		if producer, err = nsq.NewProducer(nsqURL, config); err != nil {
			log.Panic(err)
		}
	}
}

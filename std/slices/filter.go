// Package slices fornece métodos úteis para manipulação de slices.
package slices

// Filter filtra um slice de interfaces de acordo o parâmetro `filter`.
//
//  ints := []interface{}{0, 1, 2, 3, 4, 5}
//  result := slices.Filter(ints, func(i interface{}) bool { return i.(int) > 2 })
//  // []interface{}{3, 4, 5}
//
func Filter(slice []interface{}, filter func(interface{}) bool) []interface{} {
	result := make([]interface{}, 0)
	for _, v := range slice {
		if filter(v) {
			result = append(result, v)
		}
	}
	return result
}

// FilterInt filtra um slice de inteiros de acordo o parâmetro `filter`.
//
//  ints := []int{0, 1, 2, 3, 4, 5}
//  result := slices.FilterInt(ints, func(i int) bool { return i > 2 })
//  // []int{3, 4, 5}
//
func FilterInt(slice []int, filter func(int) bool) []int {
	result := make([]int, 0)
	for _, v := range slice {
		if filter(v) {
			result = append(result, v)
		}
	}
	return result
}

// FilterFloat64 filtra um slice de float64 de acordo o parâmetro `filter`.
//
//  floats := []float64{0.5, 1.5, 2.5, 3.5, 4.5, 5.5}
//  result := slices.FilterFloat64(floats, func(f float64) bool { return f > 2 })
//  // []float64{2.5, 3.5, 4.5, 5.5}
//
func FilterFloat64(slice []float64, filter func(float64) bool) []float64 {
	result := make([]float64, 0)
	for _, v := range slice {
		if filter(v) {
			result = append(result, v)
		}
	}
	return result
}

// FilterString filtra um slice de string de acordo o parâmetro `filter`.
//
//  strings := []string{"A", "B", "C", "D", "E", "F"}
//  result := slices.FilterString(strings, func(s string) bool { return s > "C" })
//  // []string{"D", "E", "F"}
//
func FilterString(slice []string, filter func(string) bool) []string {
	result := make([]string, 0)
	for _, v := range slice {
		if filter(v) {
			result = append(result, v)
		}
	}
	return result
}

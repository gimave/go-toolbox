// Package setup faz todo o processo de configuração e preparação de uma app.
//
package setup

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
	"regexp"
	"strconv"
	"time"

	"bitbucket.org/eucatur/go-toolbox/app/env"
	"bitbucket.org/eucatur/go-toolbox/lib/debugger"
	"bitbucket.org/eucatur/go-toolbox/lib/debugger/debuggerutil"
	"bitbucket.org/eucatur/go-toolbox/lib/mongo"
	libredis "bitbucket.org/eucatur/go-toolbox/lib/redis"
	"bitbucket.org/eucatur/go-toolbox/lib/text"
	mgo "gopkg.in/mgo.v2"

	"github.com/garyburd/redigo/redis"
	"github.com/joeshaw/envdecode"
	"github.com/joho/godotenv"
)

var (
	allEnvs map[string]string
	envFile string

	configured         bool
	databaseConfigured bool
	redisConfigured    bool
	mongoConfigured    bool
)

// Configured retorna se a app já está configurada.
func Configured() bool {
	return configured
}

// FromEnv faz o setup(configuração) de uma app a partir de um arquivo .env.
//
//  setup.FromEnv(".api-env")
//
func FromEnv(file string) {
	var (
		fileBytes []byte
		err       error
	)

	if configured {
		log.Panic("app:setup: o método setup.FromEnv só deve ser chamado uma única vez ao iniciar a aplicação")
	}

	// Para usar um json no arquivo env basta terminar com o
	// sufixo .json, continuando possível usar o .env normalmente.
	// Essa implementação foi necessária para adicionar no bitbucket
	// e usados no Google Cloud.
	switch regexp.MustCompile(`\.json`).MatchString(file) {
	case true:
		if fileBytes, err = ioutil.ReadFile(file); err != nil {
			panicFileNotFound(file)
		}

		if err = json.Unmarshal(fileBytes, &allEnvs); err != nil {
			log.Panic(err)
		}

		for chave, valor := range allEnvs {
			if err := os.Setenv(chave, valor); err != nil {
				log.Panic(err)
			}
		}
	case false:
		if err = godotenv.Load(file); err != nil {
			panicFileNotFound(file)
		}

		if allEnvs, err = godotenv.Read(file); err != nil {
			log.Panic(err)
		}
	}

	envFile = file

	configureEnv()
	createRedisConnPool()
	createDatabaseConnections()
	createMongoConnection()
}

// ConfigFromEnv grava as variáveis de ambiente presentes no arquivo 'file' na struct 'config'.
//
//  type Config struct {
//      CacheHabilitado bool `env:"API_SRVC_TARIFA_CACHE_HABILITADO,required"`
//  }
//
//  config := Config{}
//  setup.ConfigFromEnv(&config, ".api-env")
//
func ConfigFromEnv(config interface{}, file string) {
	if err := godotenv.Load(file); err != nil {
		log.Fatalf("app:setup: o arquivo de configuração da aplicação [%s] não foi encontrado", file)
	}

	if err := envdecode.Decode(config); err != nil {
		log.Fatal(err)
	}
}

// MakeConfig grava as variáveis de ambiente (que já foram processadas anteriormente
// pelo método setup.FromEnv) na struct 'config'.
//
//  type Config struct {
//    CacheHabilitado bool `env:"API_SRVC_TARIFA_CACHE_HABILITADO,required"`
//  }
//
//  config := Config{}
//  setup.MakeConfig(&config)
//
func MakeConfig(config interface{}) {
	if err := envdecode.Decode(config); err != nil {
		log.Fatal(err)
	}
}

// check verifica se o método setup.FromEnv já foi chamado.
func check() {
	if !configured {
		log.Fatal("app:setup: o método setup.FromEnv deve ser chamado")
	}
}

// createRedisConnPool cria um pool de conexões com o Redis.
func createRedisConnPool() {
	check()

	if !redisConfigured {
		ip := env.String("API_REDIS_IP", "")
		port := env.Int("API_REDIS_PORTA", 0)

		if ip != "" {
			redisPool := &redis.Pool{
				MaxIdle:   50,
				MaxActive: 1000,

				Dial: func() (redis.Conn, error) {
					c, err := redis.Dial("tcp", ip+":"+strconv.Itoa(port))
					if err != nil {
						log.Panic(err)
					}
					return c, err
				},
			}

			libredis.Configure(redisPool)
			redisConfigured = true

			debugRedis(ip, port)
		}
	}
}

// createDatabaseConnections cria uma conexão 'master' e 'slave' com o banco de dados.
func createDatabaseConnections() {
	check()

	if !databaseConfigured {
		conn := dbConnections{}
		conn.ConfigureDefaultConnection()
		conn.ConfigureAdditionalConnections()

		databaseConfigured = true
	}
}

// createDatabaseConnections cria uma conexão 'master' e 'slave' com o banco de dados.
func createMongoConnection() {
	check()

	if !mongoConfigured {
		dbName := env.String("API_MONGODB_DATABASE", "")

		if dbName != "" {
			ip := env.String("API_MONGODB_IP", "localhost")
			porta := env.String("API_MONGODB_PORTA", "27017")
			user := env.String("API_MONGODB_USER", "")
			pass := env.String("API_MONGODB_PASSWORD", "")

			session, err := mgo.DialWithInfo(&mgo.DialInfo{
				Addrs:    []string{ip + ":" + porta},
				Timeout:  60 * time.Second,
				Database: dbName,
				Username: user,
				Password: pass,
			})

			if err != nil {
				log.Panic("app:setup:", err)
			}

			session.SetMode(mgo.Monotonic, true)

			var db *mgo.Database
			if dbName != "" {
				db = session.DB(dbName)
			}

			mongo.Configure(session, db)
			mongoConfigured = true

			debugMongo(ip, porta, user, dbName)
		}
	}
}

// configureEnv configura as variáveis de ambiente no pacote app.env.
func configureEnv() {
	env.Configure(allEnvs)
	configured = true
}

// debugRedis salva as informações de configuração do redis se o debugger
// da aplicação estiver ligado.
func debugRedis(ip string, port int) {
	msg := debuggerutil.Title("REDIS")
	msg += debuggerutil.Header("HOST           ", "PORTA")
	msg += debuggerutil.Contentf(" %s | %d", text.RPad(ip, 15, " "), port)
	debugger.Debug(msg)
}

func debugMongo(ip, porta, user, dbName string) {
	msg := debuggerutil.Title("MONGODB")

	header := []string{"HOST           ", "PORTA"}
	format := " %s | %s"
	content := []interface{}{text.RPad(ip, 15, " "), porta}

	if user != "" {
		header = append(header, "USUARIO   ")
		format += " | %s"
		content = append(content, text.RPad(user, 10, " "))
		content = append(content, dbName)
	}

	if dbName != "" {
		header = append(header, "BANCO")
		format += " | %s"
		content = append(content, dbName)
	}

	msg += debuggerutil.Header(header...)
	msg += debuggerutil.Contentf(format+"\n", content...)

	debugger.Debug(msg)
}

func panicFileNotFound(file string) {
	log.Panicf("app:setup: o arquivo de configuração da aplicação [%s] não foi encontrado", file)
}

package http

import (
	"testing"

	"github.com/labstack/echo"
	"github.com/stretchr/testify/assert"
)

func TestRoutesGroup(t *testing.T) {
	// instanciando o objeto sem nenhum grupo de rotas
	routesGroup := NewRoutesGroup()

	assert.Equal(t, 0, len(routesGroup.AllGroups()))
	assert.Panics(t, func() { routesGroup.Group("v1") })

	e := echo.New()
	v1 := e.Group("v1")

	routesGroup.AddGroup("v1", v1)

	assert.Equal(t, 1, len(routesGroup.AllGroups()))
	assert.Equal(t, v1, routesGroup.Group("v1"))
	assert.Panics(t, func() { routesGroup.Group("v2") })

	assert.Panics(t, func() { routesGroup.AddGroup("v1", v1) })

	v2 := e.Group("v2")
	v3 := e.Group("v3")

	routesGroup.AddGroup("v2", v2).AddGroup("v3", v3)

	assert.Equal(t, 3, len(routesGroup.AllGroups()))
	assert.Equal(t, v1, routesGroup.Group("v1"))
	assert.Equal(t, v2, routesGroup.Group("v2"))
	assert.Equal(t, v3, routesGroup.Group("v3"))

	// instanciando o objeto com dois grupos de rotas
	routesGroupFull := NewRoutesGroup(MapRoutesGroup{"v1": v1, "v2": v2})

	assert.Equal(t, 2, len(routesGroupFull.AllGroups()))
	assert.Equal(t, v1, routesGroupFull.Group("v1"))
	assert.Equal(t, v2, routesGroupFull.Group("v2"))

	// instanciando o objeto via make
	routesGroupMake := MakeRoutesGroup("v3", v3)

	assert.Equal(t, 1, len(routesGroupMake.AllGroups()))
	assert.Equal(t, v3, routesGroupMake.Group("v3"))
}

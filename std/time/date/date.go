// Package date é um pacote com funções específicas para conversão do tipo date.
package date

import (
	"log"
	"time"
)

// MustParseBR faz o parse de uma string(date) no formato brasileiro (DD/MM/YYYY).
//
// Essa função irá gerar um Panic se a data for inválida.
//
//  date := MustParseBR("31/12/2017")
//
func MustParseBR(str string) time.Time {
	dt, err := parseBR(str)

	if err != nil {
		log.Panic(err)
	}

	return dt
}

// ParseBR faz o parse de uma string(date) no formato brasileiro (DD/MM/YYYY).
//
//  date, err := ParseBR("31/12/2017")
//
func ParseBR(str string) (time.Time, error) {
	return parseBR(str)
}

// parseBR faz o parse de uma string(date) no formato brasileiro (DD/MM/YYYY).
func parseBR(str string) (time.Time, error) {
	return time.Parse("02/01/2006", str)
}

// MustParseEUA faz o parse de uma string(date) no formato americano (YYYY-MM-DD).
//
// Essa função irá gerar um Panic se a data for inválida.
//
//  date := MustParseEUA("2017-12-31")
//
func MustParseEUA(str string) time.Time {
	dt, err := parseEUA(str)

	if err != nil {
		log.Panic(err)
	}

	return dt
}

// ParseEUA faz o parse de uma string(date) no formato americano (YYYY-MM-DD).
//
//  date, err := ParseEUA("2017-12-31")
//
func ParseEUA(str string) (time.Time, error) {
	return parseEUA(str)
}

// parseEUA faz o parse de uma string(date) no formato americano (YYYY-MM-DD).
func parseEUA(str string) (time.Time, error) {
	return time.Parse("2006-01-02", str)
}

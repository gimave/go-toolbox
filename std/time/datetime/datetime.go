// Package datetime é um pacote com funções específicas para conversão do tipo datetime.
package datetime

import (
	"log"
	"time"
)

// MustParseEUA faz o parse de uma string(datetime) no formato americano (YYYY-MM-DD HH:II:SS).
//
// Essa função irá gerar um Panic se a data for inválida.
//
//  datetime := MustParseEUA("2017-12-31 20:30:45")
//
func MustParseEUA(str string) time.Time {
	dt, err := parseEUA(str)

	if err != nil {
		log.Panic(err)
	}

	return dt
}

// ParseEUA faz o parse de uma string(datetime) no formato americano (YYYY-MM-DD HH:II:SS).
//
//  datetime, err := ParseEUA("2017-12-31 20:30:45")
//
func ParseEUA(str string) (time.Time, error) {
	return parseEUA(str)
}

// parseEUA faz o parse de uma string(datetime) no formato americano (YYYY-MM-DD HH:II:SS).
func parseEUA(str string) (time.Time, error) {
	return time.Parse("2006-01-02 15:04:05", str)
}

// ParseBR faz o parse de uma string(datetime) no formato brasileiro (DD/MM/YYYY HH:II:SS).
//
//  datetime, err := ParseBR("31/12/2017 20:30:45")
//
func ParseBR(str string) (time.Time, error) {
	return parseBR(str, false)
}

// ParseBRShort faz o parse de uma string(datetime) no formato brasileiro sem
// sem os segundos (DD/MM/YYYY HH:II).
//
//  datetime, err := ParseBR("31/12/2017 20:30")
//
func ParseBRShort(str string) (time.Time, error) {
	return parseBR(str, true)
}

// parseBR faz o parse de uma string(datetime) no formato brasileiro.
//
// Quando o parâmetro 'short' for informado, o parâmetro 'str' deve estar sem
// as informações dos segundos(apenas horas e minutos).
func parseBR(str string, short bool) (time.Time, error) {
	f := ""

	if short {
		f = "02/01/2006 15:04"
	} else {
		f = "02/01/2006 15:04:05"
	}

	return time.Parse(f, str)
}

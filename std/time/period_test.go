package time

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func start() time.Time {
	ref, _ := time.Parse("2006-01-02 15:04:05", "2017-01-01 00:00:00")
	return ref
}

func end() time.Time {
	ref, _ := time.Parse("2006-01-02 15:04:05", "2017-12-31 23:59:59")
	return ref
}

func Test(t *testing.T) {
	assert := assert.New(t)

	period := NewPeriod(start(), end())

	assert.Equal(period.Start(), start())
	assert.Equal(period.End(), end())
}

// Package timetracker faz a cronometragem de tempo de execução de uma tarefa.
//
// Exemplo:
//   cronometro := timetracker.Start()
//   cronometro.Stop()
//   inicio := cronometro.StartAt()
//   fim := cronometro.StoppedAt()
//   duracao := cronometro.Duration()
//
package timetracker

import (
	"time"
)

// TimeTracker é responsável pelos métodos de cronometragem do tempo.
type TimeTracker struct {
	startedAt time.Time
	stoppedAt time.Time
	duration  time.Duration
	running   bool
}

// Start inicia o cronometro.
func Start() *TimeTracker {
	return &TimeTracker{startedAt: time.Now(), running: true}
}

// Stop pára o cronometro.
func (t *TimeTracker) Stop() {
	if t.IsRunning() {
		t.stoppedAt = time.Now()
		t.duration = t.stoppedAt.Sub(t.startedAt)
		t.running = false
	}
}

// IsRunning retorna se o cronometro está rodando.
func (t *TimeTracker) IsRunning() bool {
	return t.running
}

// StartedAt tempo que o cronometro foi iniciado.
func (t *TimeTracker) StartedAt() time.Time {
	return t.startedAt
}

// StoppedAt tempo que o cronometro foi parado.
func (t *TimeTracker) StoppedAt() time.Time {
	return t.stoppedAt
}

// Duration tempo de duração do cronometro.
func (t *TimeTracker) Duration() time.Duration {
	return t.duration
}

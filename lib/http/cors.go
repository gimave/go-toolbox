package http

// CORS a configuração para o uso de CORS na API.
type CORS struct {
	origins []string
	methods []string
	headers []string
}

// NewCORS retorna uma nova instância de CORS.
func NewCORS() *CORS {
	return &CORS{
		origins: []string{"*"},
		methods: []string{"GET", "HEAD", "PUT", "PATCH", "POST", "DELETE"},
	}
}

// Origins define a lista das origens que podem acessar a API com CORS.
func (c *CORS) Origins(origins ...string) *CORS {
	c.origins = origins
	return c
}

// Methods define a lista dos métodos que podem ser acessados com CORS.
func (c *CORS) Methods(methods ...string) *CORS {
	c.methods = methods
	return c
}

// Headers define a lista dos headers que podem ser usados com CORS.
func (c *CORS) Headers(headers ...string) *CORS {
	c.headers = headers
	return c
}

// GetOrigins retorna a lista das origens que podem acessar a API com CORS.
func (c *CORS) GetOrigins() []string {
	return c.origins
}

// GetMethods retorna a lista dos métodos que podem ser acessados com CORS.
func (c *CORS) GetMethods() []string {
	return c.methods
}

// GetHeaders retorna a lista dos headers que podem ser usados com CORS.
func (c *CORS) GetHeaders() []string {
	return c.headers
}

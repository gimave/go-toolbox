package money

import (
	"math"
)

// Round faz o arredondamento das casas decimais do parâmetro 'value' de
// acordo com a quantidade de casas informado em 'precision'.
//
// Exemplos:
//
//  dinheiro := Round(150.141, 2)  // 150.14
//  dinheiro := Round(150.145, 2)  // 150.15
//  dinheiro := Round(150.146, 2)  // 150.15
//
func Round(value float64, precision int) float64 {
	var round float64

	pow := math.Pow(10, float64(precision))
	digit := pow * value
	_, div := math.Modf(digit)

	if div >= .5 {
		round = math.Ceil(digit)
	} else {
		round = math.Floor(digit)
	}

	return round / pow
}

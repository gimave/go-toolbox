package http

import (
	"reflect"

	"bitbucket.org/eucatur/go-toolbox/lib/http/handler"

	"github.com/labstack/echo"
	defaults "github.com/mcuadros/go-defaults"
)

// HandlerFunc uma interface que representa o funcionamento de um handler.
type HandlerFunc func(req *Request) handler.Response

// Handler é um construtor para montar um handler.
// Essa função recebe um HandlerFunc e converte em HandlerAdapter.
func Handler(f HandlerFunc) *HandlerAdapter {
	return &HandlerAdapter{handlerFunc: f, bindDest: nil}
}

// HandlerAdapter é responsável por adaptar nosso padrão de handler para
// o padrão de handler usado pelo framework (atualmente, é o echo).
type HandlerAdapter struct {
	handlerFunc HandlerFunc
	bindDest    interface{}
	bindOptions []string
}

// Bind configura esse handler para fazer o processo de bind
// automaticamente para 'destination'.
// Os parâmetros que estiverem no body do request serão validados e os
// dados ficarão disponíveis em request.Body.
func (h *HandlerAdapter) Bind(destination interface{}, options ...string) *HandlerAdapter {
	if reflect.TypeOf(destination).Kind() == reflect.Ptr {
		h.bindDest = reflect.ValueOf(destination).Elem().Interface()
	} else {
		h.bindDest = destination
	}

	h.bindOptions = options

	return h
}

// Adapter retorna uma função que pode ser passada por o router do echo.
// Essa função realiza toda a conversão do nosso padrão de handler para
// um padrão que o echo entenda.
//
// Nessa função também é realizada todo o pré-processamento(antes de executar
// a função do handler) e o pós-processamento(após a função do handler
// ser executada).
func (h *HandlerAdapter) Adapter() func(ctx echo.Context) error {
	return func(ctx echo.Context) error {
		request := NewRequest(ctx)
		response := NewResponse(ctx)

		if h.bindDest != nil {
			requestBody := reflect.New(reflect.TypeOf(h.bindDest)).Interface()

			r := reflect.Indirect(reflect.ValueOf(requestBody))
			r.Set(reflect.ValueOf(h.bindDest))

			if err := request.BindAndValidate(requestBody, h.bindOptions...); err != nil {
				response.FromError(err)
				return nil
			}

			defaults.SetDefaults(requestBody)

			request.Body = reflect.ValueOf(requestBody).Elem().Interface()
		}

		handlerResponse := h.handlerFunc(request)

		response.FromHandler(handlerResponse)

		return nil
	}
}

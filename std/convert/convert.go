// Package convert é utilizado para conversão de tipos(type conversion).
package convert

import (
	"encoding/json"
	"errors"
	"fmt"
	"reflect"
	"strconv"
)

// ErrTypeFrom o pacote não suporta a conversão a partir desse tipo(tipo origem).
var ErrTypeFrom = errors.New("convert: a conversão a partir desse tipo não é suportado atualmente")

// ErrTypeTo o pacote não suporta a conversão para esse tipo(tipo destino).
var ErrTypeTo = errors.New("convert: a conversão para esse tipo não é suportado atualmente")

// ErrNilValue o pacote não suporta conversão de tipos nulos(nil).
var ErrNilValue = errors.New("convert: o valor para conversão não pode ser nil")

// as flags abaixo definem como a conversão a partir de uma struct ou slice
// para string funcionará.
const (
	// UseJSON a struct(ou slice) será convertida para string utilizando JSON.
	UseJSON = 1
	// UseRawString a struct(ou slice) será convertida para string utilizando string pura.
	UseRawString = 2
)

// From define o valor que será convertido.
func From(value interface{}) Converter {
	return Converter{value}
}

// Converter objeto responsável pela conversão de tipos.
type Converter struct {
	value interface{}
}

// ToString converte para string.
func (c Converter) ToString(options ...int) (string, error) {
	codec := UseJSON

	if len(options) == 1 {
		if options[0] >= 1 && options[0] <= 2 {
			codec = options[0]
		}
	}

	kind, err := checkKindOf(c.value)
	if err != nil {
		return "", err
	}

	switch kind {
	case reflect.String:
		return c.value.(string), nil
	case reflect.Int:
		return strconv.Itoa(c.value.(int)), nil
	case reflect.Struct, reflect.Slice, reflect.Interface:
		switch codec {
		case UseRawString:
			return fmt.Sprintf("%#v", c.value), nil
		default:
			bytes, err := json.Marshal(c.value)
			if err != nil {
				return "", err
			}
			return string(bytes), nil
		}
	default:
		return "", ErrTypeFrom
	}
}

// ToInt converte para int.
func (c Converter) ToInt() (int, error) {
	kind, err := checkKindOf(c.value)
	if err != nil {
		return 0, err
	}

	switch kind {
	case reflect.String:
		return strconv.Atoi(c.value.(string))
	case reflect.Int:
		return c.value.(int), nil
	default:
		return 0, ErrTypeFrom
	}
}

// ToBool converte para bool.
func (c Converter) ToBool() (bool, error) {
	kind, err := checkKindOf(c.value)
	if err != nil {
		return false, err
	}

	switch kind {
	case reflect.Bool:
		return c.value.(bool), nil

	case reflect.String:
		val := c.value.(string)
		if val == "true" || val == "1" {
			return true, nil
		}
		if val == "false" || val == "0" {
			return false, nil
		}
		return false, ErrTypeFrom

	case reflect.Int:
		val := c.value.(int)
		if val == 1 {
			return true, nil
		}
		if val == 0 {
			return false, nil
		}
		return false, ErrTypeFrom

	default:
		return false, ErrTypeFrom
	}
}

// To converte para o tipo do objeto pointerToDest.
// Será usado reflection para descobrir o tipo de pointerToDest
// e a conversão será feita para esse tipo.
func (c Converter) To(pointerToDest interface{}) error {
	typeOf := reflect.TypeOf(pointerToDest)

	if typeOf.Kind() != reflect.Ptr {
		return errors.New("convert: o parâmetro pointerToDest deve ser um ponteiro")
	}

	switch typeOf.Elem().Kind() {
	case reflect.String:
		val, err := From(c.value).ToString()
		if err != nil {
			return err
		}
		reflect.ValueOf(pointerToDest).Elem().SetString(val)
		return nil

	case reflect.Int:
		val, err := From(c.value).ToInt()
		if err != nil {
			return err
		}
		reflect.ValueOf(pointerToDest).Elem().SetInt(int64(val))
		return nil

	case reflect.Struct, reflect.Slice, reflect.Interface:
		var data []byte

		switch c.value.(type) {
		case []byte:
			data = c.value.([]byte)
		case string:
			data = []byte(c.value.(string))
		default:
			return ErrTypeFrom
		}

		return json.Unmarshal(data, pointerToDest)

	default:
		return ErrTypeTo
	}
}

// checkKindOf verifica o tipo do elemento.
func checkKindOf(element interface{}) (reflect.Kind, error) {
	var err error

	kind := reflect.TypeOf(element).Kind()

	if kind == reflect.Ptr {
		err = ErrTypeFrom
	}

	return kind, err
}

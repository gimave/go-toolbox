// Package date é um pacote com funções específicas para formatação do tipo date.
package date

import (
	"time"
)

// EUAtoBR formata uma data do formato americano (YYYY-MM-DD) para o
// formato brasileiro (DD/MM/YYYY).
//
// Essa função retorna uma string vazia se a data for inválida.
//
//  dataStr := EUAtoBR("2017-12-31")  // "31/12/2017"
//
func EUAtoBR(str string) string {
	if dt, err := time.Parse("2006-01-02", str); err == nil {
		return dt.Format("02/01/2006")
	}
	return ""
}

// EUA formata um objeto time.Time para uma data no formato americano (YYYY-MM-DD).
//
//  dataStr := EUA(data)  // "2017-12-31
//
func EUA(dt time.Time) string {
	return dt.Format("2006-01-02")
}

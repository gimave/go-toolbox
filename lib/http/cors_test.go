package http

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCORS(t *testing.T) {
	cors := NewCORS()

	assert.Len(t, cors.GetOrigins(), 1)
	assert.Contains(t, cors.GetOrigins(), "*")

	assert.Len(t, cors.GetMethods(), 6)
	assert.Contains(t, cors.GetMethods(), "GET")
	assert.Contains(t, cors.GetMethods(), "HEAD")
	assert.Contains(t, cors.GetMethods(), "PUT")
	assert.Contains(t, cors.GetMethods(), "PATCH")
	assert.Contains(t, cors.GetMethods(), "POST")
	assert.Contains(t, cors.GetMethods(), "DELETE")

	assert.Len(t, cors.GetHeaders(), 0)

	cors.Origins("example.com", "localhost")
	assert.Len(t, cors.GetOrigins(), 2)
	assert.Contains(t, cors.GetOrigins(), "example.com")
	assert.Contains(t, cors.GetOrigins(), "localhost")
	assert.NotContains(t, cors.GetOrigins(), "*")

	cors.Headers("Authorization")
	assert.Len(t, cors.GetHeaders(), 1)
	assert.Contains(t, cors.GetHeaders(), "Authorization")
	assert.NotContains(t, cors.GetHeaders(), "Content-Type")

	cors.Methods("GET", "PUT", "DELETE")
	assert.Len(t, cors.GetMethods(), 3)
	assert.Contains(t, cors.GetMethods(), "GET")
	assert.Contains(t, cors.GetMethods(), "PUT")
	assert.Contains(t, cors.GetMethods(), "DELETE")
	assert.NotContains(t, cors.GetMethods(), "OPTIONS")
}

package phone

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestShort(t *testing.T) {
	assert.Equal(t, "12345678", Short("12345678"))
	assert.Equal(t, "912345678", Short("912345678"))

	assert.Equal(t, "1234-5678", Short("1234-5678"))
	assert.Equal(t, "91234-5678", Short("91234-5678"))

	assert.Equal(t, "69 12345678", Short("(69) 12345678"))
	assert.Equal(t, "69 1234-5678", Short("(69) 1234-5678"))

	assert.Equal(t, "69 912345678", Short("(69) 91234-5678"))
	assert.Equal(t, "69 912345678", Short("(69) 912345678"))

	assert.Equal(t, "69 12345678", Short("69 12345678"))
	assert.Equal(t, "69 1234-5678", Short("69 1234-5678"))

	assert.Equal(t, "69 912345678", Short("69 91234-5678"))
	assert.Equal(t, "69 912345678", Short("69 912345678"))

}

// Package objectid disponibiliza métodos para trabalhar com identificadores(ID) para objetos.
package objectid

import (
	"fmt"

	"bitbucket.org/eucatur/go-toolbox/std/crypt"
)

// GenerateHashFor gera um hash para o parâmetro 'object'.
//
// Exemplos:
//
//  // gerando um id para uma string
//  id := GenerateHashFor("value")
//
//  // gerando um id para uma struct
//  structTest := StructTest{Field: "value"}
//  id := GenerateHashFor(structTest)
//
func GenerateHashFor(object interface{}) string {
	return crypt.Sha256(fmt.Sprintf("%+v", object))
}

package whatsapp

import (
	"bufio"
	"errors"
	"fmt"
	"net/url"
	"os/exec"
	"regexp"
	"strings"
	"sync"
	"time"
)

var ErrNenhumDispositivo = errors.New("nenhum dispositivo disponível no momento")

type Controller struct {
	devices                                    map[string]*Device
	mux                                        sync.Mutex
	updating                                   bool
	waitingTimeForEnter, waitingTimeAfterEnter int
	err                                        error
}

// NewController instância um controladora
func NewController() (*Controller, error) {
	fmt.Printf("[LIB/WHATSAPP][CONTROLLER]: NewController...\n")
	c := &Controller{waitingTimeForEnter: 500, waitingTimeAfterEnter: 200}
	err := c.UpdateDevices()
	fmt.Printf("[LIB/WHATSAPP][CONTROLLER]: NewController...OK\n")
	return c, err
}

func (c *Controller) updatingStart() {
	fmt.Printf("[LIB/WHATSAPP] updatingStart\n")
	c.updating = true
}

func (c *Controller) Updating() bool {
	fmt.Printf("[LIB/WHATSAPP] updating? %t\n", c.updating)
	return c.updating
}

func (c *Controller) updatingDone() {
	fmt.Printf("[LIB/WHATSAPP] updatingDone\n")
	c.updating = false
}

func (c *Controller) AutoUpdate(interval int) {
	fmt.Printf("[LIB/WHATSAPP] AutoUpdate... interval: %d\n", interval)
	go func() {
		if interval > 0 {
			time.Sleep(time.Duration(interval) * time.Second)
			if c.err = c.UpdateDevices(); c.err != nil {
				fmt.Printf("[LIB/WHATSAPP][CONTROLLER]: AutoUpdate...err: %s\n", c.err.Error())
			}
			c.AutoUpdate(interval)
		}
	}()
}

func (c *Controller) SetWaitingTimeForEnterCommand(wtime int) {
	fmt.Printf("[LIB/WHATSAPP] SetWaitingTimeForEnterCommand... wtime: %d\n", wtime)
	c.waitingTimeForEnter = wtime
}

func (c *Controller) SetWaitingTimeAfterEnterCommand(wtime int) {
	fmt.Printf("[LIB/WHATSAPP] SetWaitingTimeAfterEnterCommand... wtime: %d\n", wtime)
	c.waitingTimeAfterEnter = wtime
}

func (c *Controller) areAllDevicesAvailable() bool {
	fmt.Printf("[LIB/WHATSAPP][CONTROLLER]: areAllDevicesAvailable...\n")
	for _, device := range c.Devices() {
		if !device.IsAvailable() {
			fmt.Printf("[LIB/WHATSAPP][CONTROLLER]: areAllDevicesAvailable...existe um ou mais dispositivos ocupados\n")
			return false
		}
	}
	fmt.Printf("[LIB/WHATSAPP][CONTROLLER]: areAllDevicesAvailable...OK\n")
	return true
}

func (c *Controller) UpdateDevices() error {
	c.mux.Lock()
	c.updatingStart()
	defer c.mux.Unlock()
	defer c.updatingDone()
	fmt.Printf("[LIB/WHATSAPP][CONTROLLER]: UpdateDevices...\n")

	// aguarda todos os dispositivos ficarem liberados
	if c.LenDevices() > 0 {
		var i int
		for !c.areAllDevicesAvailable() {
			if i++; i == 5 {
				return errors.New("não foi possível atualizar os dispositivos")
			}
			fmt.Printf("[LIB/WHATSAPP][CONTROLLER]: UpdateDevices... aguardando(%d)\n", i)
			time.Sleep(2 * time.Second)
		}
	}

	c.devices = map[string]*Device{}

	var (
		outPutBin []byte
		err       error
	)

	cmd := exec.Command("adb", "devices")

	if outPutBin, err = cmd.Output(); err != nil {
		return err
	}

	rgx := regexp.MustCompile(`([^\t]+)([\t\s]+)(device)`)
	scanner := bufio.NewScanner(strings.NewReader(string(outPutBin)))

	for i := 0; scanner.Scan(); i++ {
		if i > 0 {
			if groups := rgx.FindStringSubmatch(scanner.Text()); len(groups) == 4 {
				c.devices[groups[1]] = &Device{Serial: groups[1]}
			}
		}
	}

	if len(c.devices) == 0 {
		c.err = ErrNenhumDispositivo
		return c.err
	}

	fmt.Printf("[LIB/WHATSAPP][CONTROLLER]: UpdateDevices...Devices:")
	for _, device := range c.Devices() {
		fmt.Printf(" %s", device.Serial)
	}
	fmt.Printf("\n")

	fmt.Printf("[LIB/WHATSAPP][CONTROLLER]: UpdateDevices...OK\n")
	return nil
}

func (c *Controller) Devices() map[string]*Device {
	return c.devices
}

func (c *Controller) LenDevices() int {
	return len(c.Devices())
}

func (c *Controller) TakeDevice() (*Device, error) {
	fmt.Printf("[LIB/WHATSAPP][CONTROLLER]: TakeDevice...\n")
	// aguarda a atualização dos dispositivos se estiver ocorrendo...
	var i int
	for c.Updating() {
		if i++; i == 5 {
			return nil, errors.New("a atualização dos dispositivos está demorando")
		}
		fmt.Printf("[LIB/WHATSAPP][CONTROLLER]: TakeDevice...Aguardando atualização dos dispositivos...(%d)\n", i)
		time.Sleep(2000 * time.Millisecond)
	}

	// atualiza a lista de dispositivos caso esteja vazia
	if c.LenDevices() < 1 {
		if err := c.UpdateDevices(); err != nil {
			return nil, err
		}
	}

	// procura por um dispositivo disponível
	for i := 1; i < 5; i++ {
		for _, device := range c.Devices() {
			if device.IsAvailable() && device.LenErrors() == 0 {
				device.Lock()
				fmt.Printf("[LIB/WHATSAPP][CONTROLLER]: TakeDevice...OK Device: %s\n", device.Serial)
				return device, nil
			}
		}
		fmt.Printf("[LIB/WHATSAPP][CONTROLLER]: TakeDevice...Aguardando algum dispositivo desocupar...(%d)\n", i)
		time.Sleep(2000 * time.Millisecond)
	}

	return nil, errors.New("nenhum dispositivo disponível no momento")
}

func (c *Controller) SendMessage(number int, text string) error {
	fmt.Printf("[LIB/WHATSAPP][CONTROLLER]: SendMessage...Number: %d\n", number)

	if number < 11111111 {
		return errors.New("informe um numero válido")
	}

	if text == "" {
		return errors.New("informe um texto")
	}

	if c.err != nil {
		return c.err
	}

	var (
		device *Device
		t      = &url.URL{Path: text}
		args   []string
		err    error
	)

	// pega um dispositivo disponível
	if device, err = c.TakeDevice(); err != nil {
		return err
	}

	// desbloqueia o dispositivo no final da func
	defer device.Unlock()

	url := fmt.Sprintf("https://api.whatsapp.com/send?phone=55%d&text=%s", number, t.String())

	args = []string{
		"-s",
		device.Serial,
		"shell",
		"am",
		"start",
		"-a",
		"android.intent.action.VIEW",
		"-d " + "'" + url + "'",
	}

	if err = exec.Command("adb", args...).Run(); err != nil {
		device.AddError(err)
		return err
	}

	time.Sleep(time.Duration(c.waitingTimeForEnter) * time.Millisecond)

	args = []string{
		"-s",
		device.Serial,
		"shell",
		"input",
		"keyevent",
		"66",
	}

	if err = exec.Command("adb", args...).Run(); err != nil {
		device.AddError(err)
		return err
	}

	time.Sleep(time.Duration(c.waitingTimeAfterEnter) * time.Millisecond)
	fmt.Printf("[LIB/WHATSAPP][CONTROLLER]: SendMessage...OK..Number: %d\n", number)
	return nil
}

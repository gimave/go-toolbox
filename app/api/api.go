// Package api faz a configuração e inicialização de uma API.
package api

import (
	"flag"
	"fmt"
	"log"
	"os"
	"sort"
	"strconv"
	"strings"

	"bitbucket.org/eucatur/go-toolbox/app/api/handlermaker"
	"bitbucket.org/eucatur/go-toolbox/app/setup"
	"bitbucket.org/eucatur/go-toolbox/lib/debugger"
	"bitbucket.org/eucatur/go-toolbox/lib/debugger/debuggerutil"
	"bitbucket.org/eucatur/go-toolbox/lib/healthcheck"
	"bitbucket.org/eucatur/go-toolbox/lib/http"
	"bitbucket.org/eucatur/go-toolbox/lib/http/middleware/authrequest"
	"bitbucket.org/eucatur/go-toolbox/lib/text"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

var (
	porta *int
	debug *bool
)

func init() {
	porta = flag.Int("porta", 0, "porta onde a API irá rodar")
	debug = flag.Bool("debug", false, "habilita/desabilita o modo debug")
}

// API estrutura responsável pela configuração e inicialização de uma API.
type API struct {
	port    int
	debug   bool
	cors    *http.CORS
	service *echo.Echo
}

// MakeDefault faz uma API do tipo padrão. O envFile(arquivo env de configuração)
// será usado para configurar a API.
func MakeDefault(envFile string) *API {
	// faz o parse das flags.
	flag.Parse()

	// é obrigatório informar a porta onde a API vai rodar.
	if *porta < 1 {
		log.Panic("informe a flag '-porta' para iniciar a API")
	}

	// se a flag debug for true, ligamos o debugger e configuramos para
	// escrever as mensagens em 'os.Stdout'.
	if *debug {
		debugger.On(os.Stdout)
	}

	// configura a aplicação de acordo com as opções definidas no arquivo .env
	setup.FromEnv(envFile)

	// instância o echo
	e := echo.New()

	// adiciona os middlewares
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	// remove o logo exibido pelo echo
	e.HideBanner = true

	return &API{port: *porta, debug: *debug, service: e}
}

// Service disponibiliza acesso ao service (serviço com os métodos para trabalhar
// com o protocolo HTTP -- middleware, routes). Atualmente, esse serviço é
// fornecido pelo framework echo.
func (a *API) Service(task func(service *echo.Echo)) {
	task(a.service)
}

// EnableCORS habilita o uso de CORS na API.
func (a *API) EnableCORS() *http.CORS {
	a.cors = http.NewCORS()
	return a.cors
}

// EnableAuthByKey habilita o uso de autenticação por chaves na API.
func (a *API) EnableAuthByKey(file string) *authrequest.ByKeyService {
	auth := authrequest.ByKey()
	auth.LoadKeysFromFile(file)
	a.debugAuthKeys(auth.GetKeys())
	return auth
}

// EnableAuthByFunc habilita o uso de autenticação por chaves na API.
func (a *API) EnableAuthByFunc(getKeyFunc authrequest.GetKeysFunc) *authrequest.ByKeyService {
	auth := authrequest.ByKey()
	auth.LoadKeysFromFunc(getKeyFunc)
	a.debugAuthKeys(auth.GetKeys())
	return auth
}

// RegisterURLVersion registra a URL administrativa que retorna a
// versão da API.
func (a *API) RegisterURLVersion(apiVersion string) {
	a.service.GET("/adm/versao", handlermaker.Version(apiVersion).Adapter())
}

// RegisterURLHealthCheck registra a URL administrativa que faz o
// healthcheck dos componentes da API.
func (a *API) RegisterURLHealthCheck(checker *healthcheck.HealthChecker) {
	a.service.GET("/adm/check", handlermaker.HealthCheck(checker).Adapter())
}

// Run inicia o serviço HTTP do servidor da API.
func (a *API) Run() {
	if a.cors != nil {
		a.service.Use(middleware.CORSWithConfig(middleware.CORSConfig{
			AllowOrigins: a.cors.GetOrigins(),
			AllowMethods: a.cors.GetMethods(),
			AllowHeaders: a.cors.GetHeaders(),
		}))
	}

	a.debugRoutes()
	a.startServer()
}

// debugRoutes se o debug estiver ativo, as rotas atuais da API serão exibidas.
func (a *API) debugRoutes() {
	if a.debug {
		type route struct {
			method string
			path   string
		}

		apiRoutes := make([]route, 0)
		for _, r := range a.service.Routes() {
			if !strings.HasSuffix(r.Path, "/*") {
				apiRoutes = append(apiRoutes, route{r.Method, r.Path})
			}
		}

		sort.SliceStable(apiRoutes, func(i, j int) bool { return apiRoutes[i].path < apiRoutes[j].path })

		allRoutes := debuggerutil.Title("ROTAS DA API")
		allRoutes += debuggerutil.Header(text.RPad("METODO", 10, " "), "URL")

		content := ""
		for _, r := range apiRoutes {
			content += fmt.Sprintf(" %s | %s\n", text.RPad(r.method, 10, " "), r.path)
		}

		allRoutes += debuggerutil.Content(content)
		debugger.Debug(allRoutes)
	}
}

// debugAuthKeys se o debug estiver ativo, as chaves de acesso da API serão exibidas.
func (a *API) debugAuthKeys(apiKeys authrequest.MapKeysPerGroup) {
	if a.debug {
		allKeys := debuggerutil.Title("CHAVES DE ACESSO DA API")
		allKeys += debuggerutil.Header(text.RPad("GRUPO", 18, " "), "CLIENTE:CHAVE")

		content := ""
		for group, clients := range apiKeys {
			for client, key := range clients {
				content += fmt.Sprintf(" %s | %s:%s\n", text.RPad(group, 18, " "), client, key)
			}
		}

		allKeys += debuggerutil.Content(content)
		debugger.Debug(allKeys)
	}
}

// startServer inicia o servidor.
func (a *API) startServer() {
	a.service.Logger.Fatal(a.service.Start(":" + strconv.Itoa(a.port)))
}

package telemetry

import (
	"runtime"
	"strings"
)

// metadata armazena os metadados sobre uma função.
type metadata struct {
	funcName string
	fileName string
	fileLine int
}

// extractMetadataFrom extrai os metadados do caller.
// Os callers são todas as funções que foram executadas
// durante o stack atual de funções.
func extractMetadataFrom(caller int) metadata {
	pc := make([]uintptr, 10)
	runtime.Callers(caller, pc)
	function := runtime.FuncForPC(pc[0])
	file, line := function.FileLine(pc[0])

	funcNameParts := strings.Split(function.Name(), "/")
	funcName := funcNameParts[len(funcNameParts)-1]

	return metadata{funcName: funcName, fileName: file, fileLine: line}
}

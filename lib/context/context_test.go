package context

import (
	"testing"

	"bitbucket.org/eucatur/go-toolbox/lib/telemetry"

	"github.com/stretchr/testify/assert"
)

func TestContext(t *testing.T) {
	ctx := New(Options{})
	assert.Nil(t, ctx.Telemetry())

	tlm := telemetry.New()
	ctx = New(Options{Telemetry: tlm})
	assert.Equal(t, ctx.Telemetry(), tlm)
}

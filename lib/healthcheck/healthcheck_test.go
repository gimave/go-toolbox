package healthcheck

import (
	"testing"

	"bitbucket.org/eucatur/go-toolbox/lib/healthcheck/checkers"
	"bitbucket.org/eucatur/go-toolbox/lib/test"

	"github.com/dimiro1/health/db"
	"github.com/stretchr/testify/assert"
)

func init() {
	test.SetupEnv()
}

func TestUsingMake(t *testing.T) {
	health := Make(Map{
		"mysql-master": checkers.NewMysqlMaster(),
		"mysql-slave":  checkers.NewMysqlSlave(),
		"redis":        checkers.NewRedis(),
	})

	result := health.Check()

	assert.True(t, result.IsUp())
	assert.False(t, result.IsDown())

	_, ok := result.GetInfo("mysql-master").(db.Checker)
	assert.False(t, ok)

	_, ok = result.GetInfo("mysql-slave").(db.Checker)
	assert.False(t, ok)

	_, ok = result.GetInfo("redis").(checkers.RedisChecker)
	assert.False(t, ok)
}

func TestUsingNew(t *testing.T) {
	health := New()

	health.AddChecker("mysql-master", checkers.NewMysqlMaster()).
		AddChecker("redis", checkers.NewRedis())

	result := health.Check()

	assert.True(t, result.IsUp())
	assert.False(t, result.IsDown())

	_, ok := result.GetInfo("mysql-master").(db.Checker)
	assert.False(t, ok)

	_, ok = result.GetInfo("redis").(checkers.RedisChecker)
	assert.False(t, ok)
}

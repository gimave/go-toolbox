# LEIA-ME #

**go-toolbox** é uma *caixa de ferramentas* escritas em golang.
O objetivo desse projeto é criar uma série de ferramentas genéricas que possam ser utilizadas na construção e padronização de novas APIs.


*Para acessar a documentação das funcionalidades atuais:* 

*1) entre na raiz do projeto e execute:*
```
#!bash
godoc -http=:6060
```
*2) abra o seu navegador de internet e acesse:*

* na máquina local: http://localhost:6060/pkg/bitbucket.org/eucatur/go-toolbox/
* com vagrant: http://192.168.33.10:6060/pkg/bitbucket.org/eucatur/go-toolbox/


*Rodar os testes:*
```
#!bash
go test ./...
```


*Verificar a cobertura de testes:*
```
#!bash
go-carpet --summary
```


### Ambiente de desenvolvimento ###

* golang 1.8.3: https://golang.org/
* go-carpet: https://github.com/msoap/go-carpet
* govendor: https://github.com/kardianos/govendor 
* redis: http://redis.io/
* sistema operacional linux: testado em Debian e Ubuntu

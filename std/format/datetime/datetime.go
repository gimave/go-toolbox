// Package datetime é um pacote com funções específicas para formatação do tipo datetime.
package datetime

import (
	"time"
)

// EUAtoBR formata uma datahora do formato americano (YYYY-MM-DD HH:II:SS)
// para o formato brasileiro (DD/MM/YYYY HH:II:SS).
//
// Essa função retorna uma string vazia se a datahora for inválida.
//
//  datahoraStr := EUAtoBR("2017-12-31 20:30:45")  // "31/12/2017 20:30:45"
//
func EUAtoBR(str string) string {
	if dt, err := time.Parse("2006-01-02 15:04:05", str); err == nil {
		return dt.Format("02/01/2006 15:04:05")
	}
	return ""
}

// EUAtoBRShort formata uma datahora do formato americano (YYYY-MM-DD HH:II:SS)
// para o formato brasileiro sem os segundos (DD/MM/YYYY HH:II).
//
// Essa função retorna uma string vazia se a datahora for inválida.
//
//  datahoraStr := EUAtoBRShort("2017-12-31 20:30:45")  // "31/12/2017 20:30"
//
func EUAtoBRShort(str string) string {
	if dt, err := time.Parse("2006-01-02 15:04:05", str); err == nil {
		return dt.Format("02/01/2006 15:04")
	}
	return ""
}

// BRShort formata um objeto time.Time para uma data no formato brasileiro
// sem os segundos (DD/MM/YYYY HH:II).
//
//  datahoraStr := BRShort(datahora)  // "31/12/2017 20:30"
//
func BRShort(dt time.Time) string {
	return dt.Format("02/01/2006 15:04")
}

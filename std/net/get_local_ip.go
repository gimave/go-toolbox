// Package net é um pacote utilizado para funções envolvendo rede(networking).
package net

import (
	libnet "net"
)

// GetLocalIP obtém o IP local da máquina onde a aplicação está rodando.
//
//  ip, err := GetLocalIP()
//
func GetLocalIP() (string, error) {
	var (
		addrs []libnet.Addr
		ip    string
		err   error
	)

	if addrs, err = libnet.InterfaceAddrs(); err != nil {
		return ip, err
	}

	for _, a := range addrs {
		if ipnet, ok := a.(*libnet.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				ip = ipnet.IP.String()
				break
			}
		}
	}

	return ip, nil
}

// Package handlermaker é um pacote auxiliar usado para facilitar a geração de
// handlers comuns usados pelas APIs.
package handlermaker

import (
	"bitbucket.org/eucatur/go-toolbox/lib/healthcheck"
	"bitbucket.org/eucatur/go-toolbox/lib/http"
	"bitbucket.org/eucatur/go-toolbox/lib/http/handler"
	"bitbucket.org/eucatur/go-toolbox/lib/serverid"
	"bitbucket.org/eucatur/go-toolbox/std/conditions/check"
)

// Version monta um handler que retorna a versão da API.
func Version(apiVersion string) *http.HandlerAdapter {
	return http.Handler(func(req *http.Request) handler.Response {
		server, _ := serverid.GenerateFromLocalIP(2, ".")

		return handler.Success(200, http.Body{
			"versao":   apiVersion,
			"servidor": server,
		})
	})
}

// HealthCheck monta um handler que retorna a situação("saúde") dos componentes
// e serviços usados pela API.
func HealthCheck(checker *healthcheck.HealthChecker) *http.HandlerAdapter {
	return http.Handler(func(req *http.Request) handler.Response {
		health := checker.Check()
		status := check.If(health.IsDown(), 503, 200).(int)

		if status == 200 {
			return handler.Success(status, health)
		}

		return handler.Error(status, health)
	})
}

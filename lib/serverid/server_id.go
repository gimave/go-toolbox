// Package serverid disponibiliza métodos para trabalhar com identificadores(ID) para servidores.
package serverid

import (
	"strings"

	"bitbucket.org/eucatur/go-toolbox/std/net"
)

// GenerateFromLocalIP gera uma identificação(ID) para o servidor a partir
// do endereço de IP da máquina onde a aplicação está rodando. O parâmetro 'parts'
// define quantas partes do IP serão usadas para gerar o ID.
//
// Exemplos usando o IP '10.0.2.15'.
//
//  id, err := GenerateFromLocalIP(1, "")   // 15
//  id, err := GenerateFromLocalIP(2, "")   // 215
//  id, err := GenerateFromLocalIP(2, ".")  // 2.15
//  id, err := GenerateFromLocalIP(3, "")   // 0215
//  id, err := GenerateFromLocalIP(3, ".")  // 0.2.15
//
func GenerateFromLocalIP(parts int, sep string) (string, error) {
	var (
		ip  string
		err error
	)

	if ip, err = net.GetLocalIP(); err != nil {
		return "", err
	}

	str := strings.Split(ip, ".")

	if parts < 1 || len(str) <= parts {
		return strings.Join(str, sep), nil
	}

	return strings.Join(str[len(str)-parts:], sep), nil
}

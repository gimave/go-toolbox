package soap

import (
	"bytes"
	"errors"
	"os/exec"
)

func execCommand(cmd *exec.Cmd, body []byte) (result []byte, err error) {
	cmd.Stdin = bytes.NewReader(body)

	var output bytes.Buffer
	cmd.Stdout = &output
	var cmdError bytes.Buffer
	cmd.Stderr = &cmdError
	if err := cmd.Run(); err != nil {
		return []byte{}, errors.New(cmdError.String())
	}

	return output.Bytes(), nil
}

func Curl(url string, body []byte) (result []byte, err error) {
	cmd := exec.Command(
		"curl",
		"-X", "POST",
		"-H", "content-type: application/soap+xml",
		"-d", "@-",
		url,
	)

	return execCommand(cmd, body)
}

//Gerar arquivo PEM
//cat client.crt client.key > client.includesprivatekey.pem
func CurlWithCert(url string, body []byte, certPath string) (result []byte, err error) {
	cmd := exec.Command(
		"curl",
		"-X", "POST",
		"-H", "content-type: application/soap+xml",
		"--cert", certPath,
		"--cert-type", "PEM",
		"-d", "@-",
		"--insecure",
		url,
	)

	return execCommand(cmd, body)
}

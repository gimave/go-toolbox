// Package redis é uma lib para comunicação com o banco de dados redis.
package redis

import (
	"log"

	"github.com/garyburd/redigo/redis"
)

// armazena o pool de conexões com o redis
var redisPool *redis.Pool

// Configure usado para configurar o pacote.
// Esse método deve ser chamado somente uma vez na inicialização da API.
func Configure(pool *redis.Pool) {
	if redisPool != nil {
		log.Panic("redis: o método 'Configure' já foi chamado")
	}

	redisPool = pool
}

// Pool retorna o pool de conexões com o redis.
func Pool() *redis.Pool {
	if redisPool == nil {
		log.Panic("redis: o método 'Configure' deve ser chamado para configurar esse pacote")
	}
	return redisPool
}

// Exec executa a tarefa 'task' dentro do redis.
// Esse método pega uma conexão do pool e passa como parâmetro para 'task'.
// Após executar 'task', a conexão com redis é fechada e o resultado de 'task',
// juntamente com o erro, é retornado.
func Exec(task func(redis.Conn) (interface{}, error)) (interface{}, error) {
	redisConn := Pool().Get()
	defer redisConn.Close()
	return task(redisConn)
}

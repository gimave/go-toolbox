package types

import (
	"fmt"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNullBytes(t *testing.T) {

	jsonString := `{
		"exemplo": "valor"
	}`

	jsonBytes := []byte(jsonString)
	jsonBytesString := strings.Replace(fmt.Sprint(jsonBytes), " ", ", ", -1)
	jsonBytesStringBytes := []byte(jsonBytesString)

	nullbytes := NullBytes{Bytes: jsonBytes, Valid: true}
	nullbytesBackup := NullBytes{Bytes: jsonBytes, Valid: true}

	// nullbytes -> json
	bytes, err := nullbytes.MarshalJSON()
	assert.Equal(t, nil, err)
	assert.Equal(t, jsonBytesStringBytes, bytes)

	// json -> nullbytes
	err = nullbytes.UnmarshalJSON(jsonBytes)
	assert.Equal(t, nil, err)
	assert.Equal(t, nullbytesBackup, nullbytes)

	// nullbytes -> db
	value, err := nullbytes.Value()
	assert.Equal(t, nil, err)
	assert.Equal(t, jsonBytes, value)

	// db -> nullbytes
	err = nullbytes.Scan(jsonBytes)
	assert.Equal(t, nil, err)
	assert.Equal(t, nullbytesBackup, nullbytes)
}

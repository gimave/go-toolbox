// Package tasker é um pacote com funções para execuções de tarefas(comandos).
package tasker

import (
	"sync"
	"time"
)

var mut sync.RWMutex

// RunSync executa o processo 'process' de forma síncrona repetidamente conforme
// o intervalo definido em 'duration'.
//
//  // define a tarefa que será executada
//  task := func() {
//      fmt.Println("executando ...")
//  }
//
//  // configura o tasker para rodar a tarefa a cada 2 minutos
//  RunSync(task, 2 * time.Minute)
//
func RunSync(process func(), duration time.Duration) {
	ticker := time.NewTicker(duration)

	go func() {
		for t := range ticker.C {
			mut.Lock()
			process()
			mut.Unlock()
			_ = t
		}
	}()
}

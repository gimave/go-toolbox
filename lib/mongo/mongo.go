package mongo

import (
	"log"

	mgo "gopkg.in/mgo.v2"
)

var (
	session *mgo.Session
	db      *mgo.Database
)

func Configure(s *mgo.Session, d *mgo.Database) {
	if session != nil {
		log.Panic("mongo: o método 'Configure' já foi chamado")
	}

	session = s
	db = d
}

func Session() *mgo.Session {
	if session == nil {
		log.Panic("mongo: o método 'Configure' deve ser chamado para configurar esse pacote")
	}
	return session
}

func DB() *mgo.Database {
	if db == nil {
		log.Panic("mongo: banco não definido")
	}
	return db
}

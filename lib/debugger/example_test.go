package debugger_test

import (
	"errors"
	"os"

	"bitbucket.org/eucatur/go-toolbox/lib/debugger"
)

func Example() {
	// Liga o debugger e configura para escrever em 'os.Stdout'.
	// Geralmente, esse processo será feito na inicialização da app.
	debugger.On(os.Stdout)

	// Para gravar uma mensagem para debug, importe o pacote 'debugger'
	// e chame um dos métodos abaixo ...
	debugger.Debug("mensagem ...")
	debugger.Debug(errors.New("mensagem de erro"))
	debugger.Debugf("mensagem: %d", 1)
	debugger.Debugf("mensagem: %d | %s", 2, "OK")

	// O método `FileLine` imprime o arquivo e a linha que gerou a
	// mensagem de debug.
	debugger.FileLine(errors.New("mensagem de erro"))

	// O método `Stack` imprime todo o stack de execução até o local
	// onde a mensagem de debug foi gerada.
	debugger.Stack(errors.New("mensagem de erro"))
}

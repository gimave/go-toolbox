// Package test contém métodos para facilitar a criação de testes automatizados.
package test

import (
	"log"
	"os"
	"path"
	"strings"

	"bitbucket.org/eucatur/go-toolbox/app/setup"
)

// SetupEnv prepara o ambiente para rodar os testes automatizados.
func SetupEnv() {
	if !setup.Configured() {
		pwd, err := os.Getwd()
		if err != nil {
			log.Panic(err)
		}

		allDirs := make([]string, 0)
		for _, dir := range strings.Split(pwd, "/") {
			allDirs = append(allDirs, dir)
			if dir == "lib" {
				allDirs = append(allDirs, "test")
				break
			}
		}

		dir := "/" + path.Join(allDirs...)

		setup.FromEnv(dir + "/.test-env")
	}
}

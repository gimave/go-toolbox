package http

import (
	"fmt"
	"time"

	"bitbucket.org/eucatur/go-toolbox/lib/debugger/debuggerutil"
	"bitbucket.org/eucatur/go-toolbox/lib/http/handler"
	"bitbucket.org/eucatur/go-toolbox/lib/validator"
	"bitbucket.org/eucatur/go-toolbox/std/errors"

	"github.com/labstack/echo"
)

// NewResponse retorna uma nova instância de Response.
func NewResponse(c echo.Context) *Response {
	return &Response{c}
}

// Response disponibiliza métodos para lidar com as responses
// (respostas) no protocolo http.
type Response struct {
	context echo.Context
}

// Ctx retorna o echo.Context para qual essa resposta foi criada.
func (r Response) Ctx() echo.Context {
	return r.context
}

// FromHandler converte a resposta gerada por um handler para uma resposta em
// formato JSON.
func (r Response) FromHandler(resp handler.Response) {
	codeError := resp.CodeError()
	bodyError := resp.BodyError()

	if bodyError != nil {
		e, ok := bodyError.(error)
		if ok {
			r.FromError(e)
			return
		}

		if codeError < 400 || codeError > 599 {
			codeError = 422
		}

		r.JSON(codeError, bodyError)
		return
	}

	codeSuccess := resp.CodeSuccess()

	if codeSuccess < 100 || codeSuccess > 399 {
		codeSuccess = 200
	}

	if resp.IsBlob() {

		if resp.BlobFileName() != "" {
			r.Ctx().Response().Header().Set("Content-Disposition", "attachment; filename="+resp.BlobFileName())
		}

		r.Ctx().Blob(codeSuccess, resp.ContentType(), resp.BodySuccessBlob())
		return
	}

	if resp.IsRedirect() {
		r.Ctx().Redirect(codeSuccess, resp.BodyRedirect())
	}

	r.JSON(codeSuccess, resp.BodySuccess())
}

// FromError converte um erro gerado por um handler para uma resposta em
// formato JSON.
func (r Response) FromError(err error) {
	// Tramento de erros:
	// -- se 'err' for uma instância de um erro conhecido, será feito o tratamento específico
	//    para cada erro antes de retornar a resposta.
	// -- se 'err' for outro tipo de erro, será gerado um erro genérico como resposta.
	switch e := err.(type) {
	case errors.APPError:
		r.Err(err.Error())
		return

	case errors.APIError:
		r.JSON(e.Status(), BodyError(e.Message()))
		return

	case errors.DescriptiveError:
		body := BodyError(e.Message())

		body["codigo"] = e.Code()

		details := e.Details()

		if len(details) > 0 {
			body["detalhes"] = e.Details()
		}

		r.JSON(422, body)
		return

	case *validator.VError:
		r.JSON(400, e)
		return

	case *echo.HTTPError:
		r.JSON(e.Code, BodyError(e.Message))
		return
	}

	switch {
	case err == errors.ErrNoResult:
		r.Err("Não foram encontrados resultados para a consulta informada")
		return

	case err == errors.ErrUnauthorized:
		r.JSON(401, BodyError("Acesso não autorizado"))
		return

	case err == errors.ErrForbidden:
		r.JSON(403, BodyError("Você não possui permissão para realizar essa operação"))
		return

	default:
		r.JSON(500, BodyError("Ocorreu um erro interno"))

		fmt.Print(debuggerutil.Title(fmt.Sprintf("%s [%v]", "ERRO 500", time.Now().Format(time.RFC3339Nano))))
		fmt.Print(debuggerutil.Header("CAUSA DO ERRO"))

		if e, ok := err.(errors.Error); ok {
			e.PrintStack()
		} else {
			fmt.Printf("err: %v | tipo: %#v\n", err, err)
		}

		fmt.Print(debuggerutil.LineSeparator() + "\n")
	}
}

// Msg é um atalho para gerar uma resposta em formato JSON, com código de
// status 200 e body da resposta será {"mensagem": parâmetro 'content'}.
func (r Response) Msg(content interface{}) error {
	r.context.JSON(200, BodyMessage(content))
	return nil
}

// Err é um atalho para gerar uma resposta em formato JSON, com código de
// status 422 e body da resposta será {"erro": parâmetro 'content'}.
func (r Response) Err(content interface{}) error {
	r.context.JSON(422, BodyError(content))
	return nil
}

// JSON é um atalho para gerar uma resposta em formato JSON, com código de
// status definido no parâmetro 'code' e o body da resposta será
// definido no parâmetro 'content'.
func (r Response) JSON(code int, content interface{}) {
	r.context.JSON(code, content)
}

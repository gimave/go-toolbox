// Package authrequest agrupa as estratégias de autenticação para uma requisição HTTP.
package authrequest

import (
	"github.com/labstack/echo"
)

const (
	// defaultSchema - schema padrão de autenticação.
	defaultSchema = "ECTR"

	// AuthorizedClientCtx é a chave usada para gravar o cliente da API autenticado no context do request atual.
	AuthorizedClientCtx = "auth.client"
)

// http401 gera uma resposta HTTP padrão para acesso não autorizado.
func http401(ctx echo.Context) error {
	return ctx.JSON(401, echo.Map{"erro": "acesso não autorizado"})
}

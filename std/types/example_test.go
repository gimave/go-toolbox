package types_test

import (
	"fmt"

	"bitbucket.org/eucatur/go-toolbox/std/types"
)

func Example() {
	// exemplo de utilização do tipo bool.
	var b types.Bool = -1

	fmt.Printf("resultado: %#v\n", b.IsFalse()) // false
	fmt.Printf("resultado: %#v\n", b.IsTrue())  // false
	fmt.Printf("resultado: %#v\n", b.Value())   // false
	fmt.Printf("resultado: %#v\n", b.Empty())   // true

	b = 0
	fmt.Printf("resultado: %#v\n", b.IsFalse()) // true
	fmt.Printf("resultado: %#v\n", b.IsTrue())  // false
	fmt.Printf("resultado: %#v\n", b.Value())   // false
	fmt.Printf("resultado: %#v\n", b.Empty())   // false

	b = 1
	fmt.Printf("resultado: %#v\n", b.IsFalse()) // false
	fmt.Printf("resultado: %#v\n", b.IsTrue())  // true
	fmt.Printf("resultado: %#v\n", b.Value())   // true
	fmt.Printf("resultado: %#v\n", b.Empty())   // false

	// Output:
	// resultado: false
	// resultado: false
	// resultado: false
	// resultado: true
	// resultado: true
	// resultado: false
	// resultado: false
	// resultado: false
	// resultado: false
	// resultado: true
	// resultado: true
	// resultado: false
}

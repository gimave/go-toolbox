package authrequest

import (
	"bytes"
	"fmt"
	"log"
	"regexp"
	"strings"
	"time"

	"bitbucket.org/eucatur/go-toolbox/std/conditions/check"
	stdstrings "bitbucket.org/eucatur/go-toolbox/std/strings"

	"github.com/labstack/echo"
	"github.com/spf13/afero"
	"github.com/spf13/viper"
)

// MapKeysPerGroup conjunto de chaves de acesso agrupadas por grupo:
//  keys["nome-do-grupo"]["nome-do-cliente"] = "chave-do-cliente"
// Exemplo:
//  keys["api"]["sge"]  = "F1F58D68-7E98-11E7-8321-40F2E90C459C"
//  keys["api"]["site"] = "0E8E0634-7E99-11E7-8321-40F2E90C459C"
//
type MapKeysPerGroup map[string]map[string]string

// GetKeysFunc é usada como parâmetro para inserir keys
type GetKeysFunc func() MapKeysPerGroup

// ByKey retorna uma nova instância de ByKeyService.
func ByKey() *ByKeyService {
	return &ByKeyService{allKeys: make(MapKeysPerGroup)}
}

// ByKeyService serviço que faz a autenticação de um request usando uma chave.
type ByKeyService struct {
	allKeys     MapKeysPerGroup
	filename    string
	getKeysFunc *GetKeysFunc
}

// GetKeys retorna todas as chaves que tem permissão para acessar a API.
func (b *ByKeyService) GetKeys() MapKeysPerGroup {
	return b.allKeys
}

// LoadKeys carrega as chaves que tem permissão para acessar a API.
func (b *ByKeyService) LoadKeys(allKeys MapKeysPerGroup) {
	b.allKeys = allKeys
}

// LoadKeysFromFile carrega as chaves de acesso de um arquivo no formato YAML.
func (b *ByKeyService) LoadKeysFromFile(filename string) {
	b.filename = filename
	fileType := check.If(regexp.MustCompile(`\.json`).MatchString(filename), "json", "yaml").(string)

	file, err := afero.ReadFile(afero.NewOsFs(), filename)
	if err != nil {
		log.Panicf("middleware/authrequest: erro ao ler o arquivo de configuração das chaves [%s] | err: %#v", filename, err)
	}

	config := viper.New()
	config.SetConfigType(fileType)

	if err = config.ReadConfig(bytes.NewReader(file)); err != nil {
		log.Panicf("middleware/authrequest: erro ao fazer o parse do arquivo de configuração das chaves [%s] | err: %#v", filename, err)
	}

	var (
		groups  map[string]interface{}
		clients map[string]interface{}
		ok      bool
	)

	if groups, ok = config.Get("grupos").(map[string]interface{}); !ok {
		log.Panicf("middleware/authrequest: a chave `grupos` deve estar configurada no arquivo [%s]", filename)
	}

	allKeys := make(MapKeysPerGroup)

	for group, iClients := range groups {
		if clients, ok = iClients.(map[string]interface{}); !ok {
			log.Panicf("middleware/authrequest: a estrutura das informações dentro do grupo [%s] é inválida no arquivo [%s]", group, filename)
		}

		allKeys[group] = make(map[string]string)

		for client, key := range clients {
			allKeys[group][client] = key.(string)
		}
	}

	b.LoadKeys(allKeys)
}

// LoadKeysFromFunc carrega as chaves de acesso passando uma funcão como parâmetro
// portanto o que ocorre dentro da função é de responsabilidade de
// quem a envia
func (b *ByKeyService) LoadKeysFromFunc(getKeysFunc GetKeysFunc) {
	b.getKeysFunc = &getKeysFunc
	b.LoadKeys(getKeysFunc())
}

// ReloadKeys recarrega as chaves de acesso com os parâmetros
// previamento recebidos
func (b *ByKeyService) ReloadKeys() {
	log.Println("recarregando as keys...")
	if b.filename != "" {
		b.LoadKeysFromFile(b.filename)
		log.Println("keys recarregadas via arquivo.")
		return
	}
	if b.getKeysFunc != nil {
		getKeysFunc := *b.getKeysFunc
		b.LoadKeys(getKeysFunc())
		log.Println("keys recarregadas via func.")
		return
	}
	log.Println("não foi possível recarregar as keys")
}

// AutoReloadKeysMinutes recarrega as chaves de acesso repetidamente conforme
// o parâmetro informado em minutos, seja via arquivo ou função
func (b *ByKeyService) AutoReloadKeysMinutes(minutes int) {
	if minutes > 0 {
		if duration, err := time.ParseDuration(fmt.Sprintf("%dm", minutes)); err == nil {
			go func() {
				for {
					time.Sleep(duration)
					b.ReloadKeys()
				}
			}()
		}
	}
}

// AllowAccessFor cria um middleware que faz a autenticação de um request.
// O parâmetro `groups` define quais os grupos que tem permissão de acesso.
// O middleware irá extrair as informações de acesso presentes no header
// `Authorization` e fará a comparação se o cliente e a chave são
// validos e se o cliente está presente num dos grupos com permissão.
func (b *ByKeyService) AllowAccessFor(groups ...string) echo.MiddlewareFunc {

	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(ctx echo.Context) error {

			auth := strings.TrimSpace(ctx.Request().Header.Get("Authorization"))

			if strings.HasPrefix(auth, defaultSchema) && len(groups) > 0 {
				auth = strings.TrimSpace(strings.TrimPrefix(auth, defaultSchema))

				for group, clients := range b.allKeys {
					if stdstrings.ExistsInSlice(group, groups) {
						for client, key := range clients {
							if auth == client+":"+key {
								ctx.Set(AuthorizedClientCtx, client)
								return next(ctx)
							}
						}
					}
				}
			}

			go b.ReloadKeys()
			return http401(ctx)
		}
	}
}

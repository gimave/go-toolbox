package timetracker

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestTracker(t *testing.T) {
	startedAt := time.Now()

	tracker := Start()
	isRunning := tracker.IsRunning()

	time.Sleep(2 * time.Second)

	stoppedAt := time.Now()
	tracker.Stop()

	assert.True(t, isRunning)
	assert.False(t, tracker.IsRunning())

	assert.True(t, tracker.StartedAt().Sub(startedAt) < 100*time.Millisecond)
	assert.True(t, tracker.StoppedAt().Sub(stoppedAt) < 100*time.Millisecond)

	assert.True(t, tracker.Duration() < (2*time.Second)+(100*time.Millisecond))
}

package soap

import (
	"crypto/tls"
	"crypto/x509"
	"io"
	"io/ioutil"
	"net"
	"net/http"
	"time"

	"bitbucket.org/eucatur/api-bpe-v1/lib/bpe/log"
)

func HTTPPostTLS(cert tls.Certificate, url string, body io.Reader) ([]byte, error) {
	var (
		resposta []byte
		err      error
	)
	/*
		carregar o certificado cliente. Para gerar o crt e key a partir de um PFX use o openssl

		Exemplo:
		# exporta o key com seguranca
		openssl pkcs12 -in certificado.pfx -nocerts -out client_seguro.key

		# exporta o key sem seguranca
		openssl rsa -in client_seguro.key -out client.key

		# exporta o crt
		openssl pkcs12 -in certificado.pfx -clcerts -nokeys -out client.crt
		openssl pkcs12 -in certificado.pfx -clcerts -nokeys -out client.crt -passin pass:1234

	*/
	/*cert, err := tls.LoadX509KeyPair("/root/bpe/certificados/privados/05921606/client.crt", "/root/bpe/certificados/privados/05921606/client.key")

	if err != nil {
		log.Error(err)
		return resposta, err
	}
	*/

	/*
		Carregar as cadeias do certificado, baixar as cadeias a partir da seguinte
		URI http://www.bpe.ms.gov.br/wp-content/uploads/sites/167/2017/06/cadeia_certificados-BPE.zip
	*/
	caCertPool := x509.NewCertPool()

	certificatePool := [][]string{
		{
			"/root/bpe/certificados/MS/ICP-Brasilv5.crt",
			"/root/bpe/certificados/MS/AC_Certisign_G7.crt",
			"/root/bpe/certificados/MS/AC_Certisign_Multipla_SSL.crt",
		},
		{
			"/root/bpe/certificados/RS/ICP-Brasilv5.crt",
			"/root/bpe/certificados/RS/Autoridade_Certificadora_Serpro_v4.crt",
			"/root/bpe/certificados/RSRS/Autoridade_Certificadora_do_Serpro_Final_SSL.crt",
		},
	}

	for _, certPath := range certificatePool[0] {
		caCert, err := ioutil.ReadFile(certPath)

		if err != nil {
			log.Error(err)
			return resposta, err
		}

		caCertPool.AppendCertsFromPEM(caCert)
	}

	// Configura HTTP Client
	tlsConfig := &tls.Config{
		Certificates: []tls.Certificate{cert},
		RootCAs:      caCertPool,
		ClientAuth:   tls.RequireAnyClientCert,
		MinVersion:   tls.VersionTLS12,
		// CurvePreferences:         []tls.CurveID{tls.CurveP521, tls.CurveP384, tls.CurveP256},
		// PreferServerCipherSuites: true,
	}

	// configura as cifras
	tlsConfig.CipherSuites = []uint16{
		tls.TLS_RSA_WITH_AES_256_CBC_SHA,
		tls.TLS_RSA_WITH_AES_128_CBC_SHA,
		tls.TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA,
		tls.TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA,
		tls.TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA,
		tls.TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA,
		tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
		tls.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
		tls.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
	}

	// configura a versao do TLS suportada
	tlsConfig.MinVersion = tls.VersionTLS10
	tlsConfig.MaxVersion = tls.VersionTLS12
	tlsConfig.PreferServerCipherSuites = true

	tlsConfig.BuildNameToCertificate()

	// configura o transporte. Ajuste o valor 5s de acordo com sua necessidade
	transport := &http.Transport{
		TLSClientConfig: tlsConfig,
		Dial: (&net.Dialer{
			Timeout: 20 * time.Second,
		}).Dial,
		TLSHandshakeTimeout: 20 * time.Second,
	}

	// configra 10 segundos para o timeout da requisicao. Ajuste de acordo com sua necessidade.
	client := &http.Client{
		Timeout:   time.Second * 20,
		Transport: transport,
	}

	resp, err := client.Post(url, "application/soap+xml", body)

	if err != nil {
		log.Error(err)
		return []byte{}, err
	}

	return ioutil.ReadAll(resp.Body)
}

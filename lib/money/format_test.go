package money

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFormat(t *testing.T) {
	assert.Equal(t, "0,00", Format(0))
	assert.Equal(t, "0,01", Format(1))
	assert.Equal(t, "0,12", Format(12))
	assert.Equal(t, "1,23", Format(123))
	assert.Equal(t, "12,34", Format(1234))
	assert.Equal(t, "123,45", Format(12345))
	assert.Equal(t, "1.234,56", Format(123456))
	assert.Equal(t, "12.345,67", Format(1234567))
	assert.Equal(t, "123.456,78", Format(12345678))

	assert.Equal(t, "-0,01", Format(-1))
	assert.Equal(t, "-0,12", Format(-12))
	assert.Equal(t, "-1,23", Format(-123))
	assert.Equal(t, "-12,34", Format(-1234))
	assert.Equal(t, "-123,45", Format(-12345))
	assert.Equal(t, "-1.234,56", Format(-123456))
	assert.Equal(t, "-12.345,67", Format(-1234567))
	assert.Equal(t, "-123.456,78", Format(-12345678))

	assert.Equal(t, "0,00", Format(int64(0)))
	assert.Equal(t, "0,01", Format(int64(1)))
	assert.Equal(t, "0,12", Format(int64(12)))
	assert.Equal(t, "1,23", Format(int64(123)))
	assert.Equal(t, "12,34", Format(int64(1234)))
	assert.Equal(t, "123,45", Format(int64(12345)))
	assert.Equal(t, "1.234,56", Format(int64(123456)))
	assert.Equal(t, "12.345,67", Format(int64(1234567)))
	assert.Equal(t, "123.456,78", Format(int64(12345678)))

	assert.Equal(t, "-0,12", Format(int64(-12)))
	assert.Equal(t, "-1,23", Format(int64(-123)))
	assert.Equal(t, "-12,34", Format(int64(-1234)))
	assert.Equal(t, "-123,45", Format(int64(-12345)))
	assert.Equal(t, "-1.234,56", Format(int64(-123456)))
	assert.Equal(t, "-12.345,67", Format(int64(-1234567)))
	assert.Equal(t, "-123.456,78", Format(int64(-12345678)))

	assert.Equal(t, "0,00", Format(0.0))
	assert.Equal(t, "0,01", Format(0.01))
	assert.Equal(t, "0,12", Format(0.12))
	assert.Equal(t, "1,23", Format(1.23))
	assert.Equal(t, "12,34", Format(12.34))
	assert.Equal(t, "123,45", Format(123.45))
	assert.Equal(t, "1.234,56", Format(1234.56))
	assert.Equal(t, "12.345,67", Format(12345.67))
	assert.Equal(t, "123.456,78", Format(123456.78))

	assert.Equal(t, "-0,01", Format(-0.01))
	assert.Equal(t, "-0,12", Format(-0.12))
	assert.Equal(t, "-1,23", Format(-1.23))
	assert.Equal(t, "-12,34", Format(-12.34))
	assert.Equal(t, "-123,45", Format(-123.45))
	assert.Equal(t, "-1.234,56", Format(-1234.56))
	assert.Equal(t, "-12.345,67", Format(-12345.67))
	assert.Equal(t, "-123.456,78", Format(-123456.78))
}

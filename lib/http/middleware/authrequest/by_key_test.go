package authrequest

import (
	"io/ioutil"
	"net/http/httptest"
	"os"
	"path"
	"testing"

	"github.com/labstack/echo"
	"github.com/stretchr/testify/assert"
)

func TestAuthByKeyUsingRawKeys(t *testing.T) {
	allKeys := make(MapKeysPerGroup)

	allKeys["admin"] = make(map[string]string)
	allKeys["api"] = make(map[string]string)

	allKeys["admin"]["admin-client"] = "admin-client-key"
	allKeys["api"]["api-client1"] = "api-client-key1"
	allKeys["api"]["api-client2"] = "api-client-key2"

	auth := ByKey()
	auth.LoadKeys(allKeys)

	testAuthByKey(t, auth)
}

func TestAuthByKeyUsingYAMLFile(t *testing.T) {
	dir, err := ioutil.TempDir("", "")
	assert.Nil(t, err)

	yamlFileContent := `# arquivo gerado automaticamente para testes 
grupos: 
  admin: 
    admin-client: admin-client-key
  api: 
    api-client1: api-client-key1
    api-client2: api-client-key2
`

	filename := path.Join(dir, ".auth-keys")

	err = ioutil.WriteFile(filename, []byte(yamlFileContent), 0660)
	assert.Nil(t, err)

	auth := ByKey()
	auth.LoadKeysFromFile(filename)

	testAuthByKey(t, auth)

	os.RemoveAll(dir)
}

func TestAuthByKeyUsingJSONFile(t *testing.T) {
	dir, err := ioutil.TempDir("", "")
	assert.Nil(t, err)

	jsonFileContent := `
		{
			"grupos": { 
  				"admin": { 
					"admin-client": "admin-client-key"
				},
				"api": { 
    				"api-client1": "api-client-key1",
					"api-client2": "api-client-key2"
				}
			}
		}`

	filename := path.Join(dir, "auth-keys.json")

	err = ioutil.WriteFile(filename, []byte(jsonFileContent), 0660)
	assert.Nil(t, err)

	auth := ByKey()
	auth.LoadKeysFromFile(filename)

	testAuthByKey(t, auth)

	os.RemoveAll(dir)
}

func testAuthByKey(t *testing.T, auth *ByKeyService) {
	e := echo.New()

	admin := e.Group("/admin")
	admin.Use(auth.AllowAccessFor("admin"))

	api := e.Group("/api")
	api.Use(auth.AllowAccessFor("admin", "api"))

	e.GET("/public", func(ctx echo.Context) error {
		return ctx.String(200, "<public>")
	})

	admin.GET("/test", func(ctx echo.Context) error {
		return ctx.String(200, "<admin-test>")
	})

	api.GET("/test", func(ctx echo.Context) error {
		return ctx.String(200, "<api-test>")
	})

	body401 := "{\"erro\":\"acesso não autorizado\"}"

	status, body := byKeyRequest(e, echo.GET, "/public", "", "")
	assert.Equal(t, 200, status)
	assert.Equal(t, "<public>", body)

	status, body = byKeyRequest(e, echo.GET, "/admin/test", "", "")
	assert.Equal(t, 401, status)
	assert.Equal(t, body401, body)

	status, body = byKeyRequest(e, echo.GET, "/admin/test", "invalid-client", "invalid-key")
	assert.Equal(t, 401, status)
	assert.Equal(t, body401, body)

	status, body = byKeyRequest(e, echo.GET, "/admin/test", "admin-client", "invalid-key")
	assert.Equal(t, 401, status)
	assert.Equal(t, body401, body)

	status, body = byKeyRequest(e, echo.GET, "/admin/test", "invalid-client", "admin-client-key")
	assert.Equal(t, 401, status)
	assert.Equal(t, body401, body)

	status, body = byKeyRequest(e, echo.GET, "/admin/test", "admin-client", "admin-client-key")
	assert.Equal(t, 200, status)
	assert.Equal(t, "<admin-test>", body)

	status, body = byKeyRequest(e, echo.GET, "/admin/test", "api-client1", "api-client-key1")
	assert.Equal(t, 401, status)
	assert.Equal(t, body401, body)

	status, body = byKeyRequest(e, echo.GET, "/api/test", "admin-client", "admin-client-key")
	assert.Equal(t, 200, status)
	assert.Equal(t, "<api-test>", body)

	status, body = byKeyRequest(e, echo.GET, "/api/test", "api-client1", "api-client-key1")
	assert.Equal(t, 200, status)
	assert.Equal(t, "<api-test>", body)

	status, body = byKeyRequest(e, echo.GET, "/api/test", "api-client2", "api-client-key2")
	assert.Equal(t, 200, status)
	assert.Equal(t, "<api-test>", body)
}

func byKeyRequest(service *echo.Echo, method, path, client, key string) (int, string) {
	req := httptest.NewRequest(method, path, nil)
	req.Header.Set("Authorization", "ECTR"+" "+client+":"+key)
	resp := httptest.NewRecorder()
	service.ServeHTTP(resp, req)
	return resp.Code, resp.Body.String()
}

package types

import (
	"database/sql/driver"
	"fmt"
	"strings"
)

type NullBytes struct {
	Bytes []byte
	Valid bool
}

func (t NullBytes) MarshalJSON() ([]byte, error) {
	return []byte(strings.Replace(fmt.Sprint(t.Bytes), " ", ", ", -1)), nil
}

func (t *NullBytes) UnmarshalJSON(bytes []byte) error {
	t.Bytes = bytes
	t.Valid = len(t.Bytes) > 0
	return nil
}

func (t NullBytes) Value() (driver.Value, error) {
	return t.Bytes, nil
}

func (t *NullBytes) Scan(value interface{}) error {

	t.Bytes = []byte{}

	if value != nil {
		switch v := value.(type) {
		case []uint8:
			t.Bytes = []byte(v)
		case string:
			t.Bytes = []byte(v)
		default:
			return fmt.Errorf("o tipo %T não pode ser scaneado para NullBytes", value)
		}
	}

	t.Valid = len(t.Bytes) > 0
	return nil
}

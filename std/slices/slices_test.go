package slices

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestExistsValue(t *testing.T) {

	values := []string{"A", "B"}

	assert.Equal(t, true, ExistsValue("A", values))
	assert.Equal(t, true, ExistsValue("B", values))
	assert.Equal(t, false, ExistsValue("C", values))
	assert.Equal(t, false, ExistsValue("", values))
}

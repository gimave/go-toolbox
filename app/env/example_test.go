package env_test

import (
	"fmt"

	"bitbucket.org/eucatur/go-toolbox/app/env"
)

func Example() {
	// Configura o pacote com as variáveis de ambiente.
	// ATENÇÃO: esse processo já é feito automaticamente ao chamar o método
	// setup.FromEnv, portanto não é necessário faze-lo novamente.
	env.Configure(env.MapEnv{
		"STRING": "text",
		"INT":    "123",
	})

	// Busca pela env "STRING"
	stringEnv := env.String("STRING", "default")
	// Exibirá "text"
	fmt.Println(stringEnv)

	// Busca pela env "ENV_TEST"
	stringEnv = env.String("ENV_TEST", "default")
	// Exibirá "default", a env "ENV_TEST" não existe.
	fmt.Println(stringEnv)

	// Busca pela env "INT"
	intEnv := env.Int("INT", 0)
	// Exibirá "123"
	fmt.Println(intEnv)

	// Busca pela env "ENV_TEST"
	intEnv = env.Int("ENV_TEST", 0)
	// Exibirá "0", a env "ENV_TEST" não existe.
	fmt.Println(intEnv)

	// Output:
	// text
	// default
	// 123
	// 0
}

// Package to read json file end set into enviroment variables
package json2env

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
)

func LoadFile(filePath string) (err error) {
	file, err := ioutil.ReadFile(filePath)
	if err != nil {
		return errors.New(fmt.Sprintf("Error in find file: %s", filePath))
	}

	var data map[string]string
	json.Unmarshal(file, &data)

	for k, v := range data {
		os.Setenv(k, v)
	}

	return
}

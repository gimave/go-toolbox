package query

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestEncode(t *testing.T) {

	var p = struct {
		ID        int    `query:"id"`
		Nome      string `query:"nome"`
		Telefone  string
		TemFilhos bool `query:"tem_filhos"`
	}{
		ID:        10,
		Nome:      "João da Silva",
		Telefone:  "11222223333",
		TemFilhos: true,
	}

	str, err := Encode(p)
	assert.Contains(t, str, "id=10")
	assert.Contains(t, str, "nome=Jo%C3%A3o+da+Silva")
	assert.Contains(t, str, "Telefone=11222223333")
	assert.Contains(t, str, "tem_filhos=true")
	assert.Nil(t, err)
}

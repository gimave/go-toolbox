package date

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestEUAtoBR(t *testing.T) {
	assert.Equal(t, "31/12/2017", EUAtoBR("2017-12-31"))
	assert.Equal(t, "", EUAtoBR("2017-31-17"))
}

func TestEUA(t *testing.T) {
	ref, _ := time.Parse("2006-01-02", "2017-12-31")
	assert.Equal(t, "2017-12-31", EUA(ref))
}

// Package env faz o parse e o tratamento das variáveis de ambiente.
// Esse pacote facilita o uso de variáveis de ambiente dentro da API.
// Obs: variáveis de ambiente são usadas principalmente para configurações.
package env

import (
	"fmt"
	"log"

	"bitbucket.org/eucatur/go-toolbox/std/convert"
)

// MapEnv mapa das variáveis de ambiente no formato map[chave]valor.
type MapEnv map[string]string

var (
	allEnvs    MapEnv
	configured bool
)

// Configure usado para configurar o pacote.
// Esse método deve ser chamado somente uma vez na inicialização da API.
func Configure(envs MapEnv) {
	if configured {
		log.Panic("app:env: o método env.Configure já foi chamado")
	}

	allEnvs = envs
}

// All retorna todas as variáveis de ambiente.
func All() MapEnv {
	return allEnvs
}

// Bool retorna uma variável de ambiente do tipo bool.
// Se a variável não for encontrada ou não for possível converte-la para bool,
// o parâmetro 'def' (default) será retornado.
//
//  // Busca pela variável 'API_CACHE_HABILITADO'
//  val := env.Bool("API_CACHE_HABILITADO", false)
//
func Bool(env string, def bool) bool {
	result, err := ReqBool(env)

	if err != nil {
		return def
	}

	return result
}

// String retorna uma variável de ambiente do tipo string.
// Se a variável não for encontrada ou não for possível converte-la para string,
// o parâmetro 'def' (default) será retornado.
//
//  // Busca pela variável 'API_REDIS_HOST'
//  val := env.String("API_REDIS_HOST", "localhost")
//
func String(env string, def string) string {
	result, err := ReqString(env)

	if err != nil {
		return def
	}

	return result
}

// Int retorna uma variável de ambiente do tipo int.
// Se a variável não for encontrada ou não for possível converte-la para int,
// o parâmetro 'def' (default) será retornado.
//
//   // Busca pela variável 'API_REDIS_PORTA'
//   val := env.Int("API_REDIS_PORTA", 6379)
//
func Int(env string, def int) int {
	result, err := ReqInt(env)

	if err != nil {
		return def
	}

	return result
}

// MustBool retorna uma variável de ambiente do tipo bool.
// Se a variável não for encontrada ou não for possível converte-la para bool,
// um Panic será lançado.
//
//  // Busca pela variável 'API_CACHE_HABILITADO'
//  val := env.MustBool("API_CACHE_HABILITADO")
//
func MustBool(env string) bool {
	result, err := ReqBool(env)

	if err != nil {
		log.Panic(err)
	}

	return result
}

// MustString retorna uma variável de ambiente do tipo string.
// Se a variável não for encontrada ou não for possível converte-la para string,
// um Panic será lançado.
//
//  // Busca pela variável 'API_REDIS_HOST'
//  val := env.MustString("API_REDIS_HOST")
//
func MustString(env string) string {
	result, err := ReqString(env)

	if err != nil {
		log.Panic(err)
	}

	return result
}

// MustInt retorna uma variável de ambiente do tipo int.
// Se a variável não for encontrada ou não for possível converte-la para int,
// um Panic será lançado.
//
//   // Busca pela variável 'API_REDIS_PORTA'
//   val := env.MustInt("API_REDIS_PORTA")
//
func MustInt(env string) int {
	result, err := ReqInt(env)

	if err != nil {
		log.Panic(err)
	}

	return result
}

// ReqBool retorna uma variável de ambiente requerida do tipo bool.
// Se a variável não for encontrada ou não for possível converte-la para bool,
// um erro será retornado.
//
//   // Busca pela variável 'API_CACHE_HABILITADO'
//   val, err := env.Bool("API_CACHE_HABILITADO")
//
func ReqBool(env string) (bool, error) {
	var (
		result interface{}
		err    error
	)

	if result, err = reqEnv(env, "bool"); err != nil {
		return false, err
	}

	return result.(bool), nil
}

// ReqString retorna uma variável de ambiente requerida do tipo string.
// Se a variável não for encontrada ou não for possível converte-la para string,
// um erro será retornado.
//
//   // Busca pela variável 'API_REDIS_HOST'
//   val, err := env.ReqString("API_REDIS_HOST")
//
func ReqString(env string) (string, error) {
	var (
		result interface{}
		err    error
	)

	if result, err = reqEnv(env, "string"); err != nil {
		return "", err
	}

	return result.(string), nil
}

// ReqInt retorna uma variável de ambiente requerida do tipo int.
// Se a variável não for encontrada ou não for possível converte-la para int,
// um erro será retornado.
//
//   // Busca pela variável 'API_REDIS_PORTA'
//   val, err := env.ReqInt("API_REDIS_PORTA")
//
func ReqInt(env string) (int, error) {
	var (
		result interface{}
		err    error
	)

	if result, err = reqEnv(env, "int"); err != nil {
		return 0, err
	}

	return result.(int), nil
}

func reqEnv(env string, kind string) (interface{}, error) {
	val, err := get(env)
	if err != nil {
		return nil, err
	}

	var result interface{}

	switch kind {
	case "bool":
		result, err = convert.From(val).ToBool()
	case "string":
		result, err = convert.From(val).ToString()
	case "int":
		result, err = convert.From(val).ToInt()
	}

	if err != nil {
		err = fmt.Errorf("app:env: o valor da variável de ambiente [%s] não pôde ser convertida para %s: ERR [%+v]", env, kind, err)
	}

	return result, err
}

func get(env string) (string, error) {
	val, ok := allEnvs[env]

	if !ok || val == "" {
		return "", fmt.Errorf("app:env: a variável de ambiente [%s] não está definida", env)
	}

	return val, nil
}

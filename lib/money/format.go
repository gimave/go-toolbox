package money

import (
	"bytes"
	"fmt"
	"reflect"
	"strconv"

	"bitbucket.org/eucatur/go-toolbox/lib/text"
)

// Format formata um valor numérico como dinheiro (em reais).
//
// Exemplos:
//
//  dinheiro := Format(150.13)   // 150,13
//  dinheiro := Format(1500.13)  // 1.500,13
//
func Format(valor interface{}) string {
	v := reflect.ValueOf(valor)

	switch v.Kind() {
	case reflect.Int:
		return formatInt(int64(v.Int()))
	case reflect.Int64:
		return formatInt(v.Int())
	case reflect.Float64:
		return formatFloat(v.Float())
	}

	return ""
}

func formatFloat(valor float64) string {
	str := text.OnlyNumbers(fmt.Sprintf("%0.2f", Round(valor, 2)))

	i, err := strconv.Atoi(str)
	if err != nil {
		return ""
	}

	valorInteiro := int64(i)
	if valor < 0 {
		valorInteiro *= -1
	}

	return formatInt(int64(valorInteiro))
}

func formatInt(valor int64) string {
	var buff bytes.Buffer
	valorAbsoluto := absoluteValue(valor)

	reais := valorAbsoluto / 100
	centavos := valorAbsoluto % 100

	if reais <= 999 {
		buff.WriteString(fmt.Sprintf("%d", reais))
	} else {
		var total, milhares = reais, reais

		for {
			milhares = total / 1000
			reais = total % 1000

			if milhares <= 0 {
				buff.WriteString(fmt.Sprintf("%03d", reais))
				break
			}

			buff.WriteString(fmt.Sprintf("%d", milhares))
			buff.WriteString(".")

			total = reais
		}
	}

	buff.WriteString(",")
	buff.WriteString(fmt.Sprintf("%02d", centavos))

	valorFormatado := buff.String()
	if valor < 0 {
		valorFormatado = "-" + valorFormatado
	}

	return valorFormatado
}

func absoluteValue(valor int64) int64 {
	if valor < 0 {
		return -valor
	}

	return valor
}

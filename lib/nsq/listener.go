package nsq

import (
	"encoding/json"
	"errors"
	"reflect"

	"github.com/bitly/go-nsq"
)

// Listener é a estrutura do ouvinte
type Listener struct {
	topic, channel string
	body           interface{}
	handler        func(body interface{}) error
	err            error
}

// NewListener instância um novo ouvinte de publicações
func NewListener(topic string) *Listener {

	l := &Listener{}

	if topic == "" {
		l.err = errors.New("informe um topic válido")
		return l
	}

	l.topic = topic
	return l
}

// Channel define o canal se será ouvido
func (l *Listener) Channel(channel string) *Listener {

	if l.err != nil {
		return l
	}

	if channel == "" {
		l.err = errors.New("informe um channel válido")
		return l
	}

	l.channel = channel
	return l
}

// Body define o modelo corpo da publicações
func (l *Listener) Body(body interface{}) *Listener {

	if l.err != nil {
		return l
	}

	if body == nil {
		l.err = errors.New("informe um body válido")
		return l
	}

	if reflect.TypeOf(body).Kind() == reflect.Ptr {
		l.body = reflect.ValueOf(body).Elem().Interface()
	} else {
		l.body = body
	}

	return l
}

// Handler define o processo que será realizado para cada publicação recebida
func (l *Listener) Handler(handler func(body interface{}) error) *Listener {

	if l.err != nil {
		return l
	}

	if handler == nil {
		l.err = errors.New("informe um handler válido")
		return l
	}

	l.handler = handler
	return l
}

// Start inicia a escuta das publicações
func (l *Listener) Start() error {

	if l.err != nil {
		return l.err
	}

	var (
		consumer *nsq.Consumer
		config   = nsq.NewConfig()
		err      error
	)

	if consumer, err = nsq.NewConsumer(l.topic, l.channel, config); err != nil {
		return err
	}

	consumer.AddHandler(nsq.HandlerFunc(func(message *nsq.Message) error {

		body := reflect.New(reflect.TypeOf(l.body)).Interface()

		if err := json.Unmarshal(message.Body, body); err != nil {
			return err
		}

		return l.handler(reflect.ValueOf(body).Elem().Interface())
	}))

	if err = consumer.ConnectToNSQD(nsqURL); err != nil {
		return err
	}

	return nil
}

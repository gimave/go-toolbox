package convert

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

type StructTest struct {
	Field string
}

func TestToString(t *testing.T) {
	var (
		result string
		err    error
	)

	str := "TEST"
	_, err = From(&str).ToString()
	assert.NotNil(t, err)

	result, err = From("123").ToString()
	assert.Equal(t, "123", result)
	assert.Nil(t, err)

	result, err = From(123).ToString()
	assert.Equal(t, "123", result)
	assert.Nil(t, err)

	structTest := StructTest{Field: "value"}

	result, err = From(structTest).ToString()
	assert.Equal(t, "{\"Field\":\"value\"}", result)
	assert.Nil(t, err)

	result, err = From(structTest).ToString(UseJSON)
	assert.Equal(t, "{\"Field\":\"value\"}", result)
	assert.Nil(t, err)

	result, err = From(structTest).ToString(UseRawString)
	assert.Equal(t, "convert.StructTest{Field:\"value\"}", result)
	assert.Nil(t, err)

	sliceStruct := []StructTest{structTest}
	result, err = From(sliceStruct).ToString()
	assert.Equal(t, "[{\"Field\":\"value\"}]", result)
	assert.Nil(t, err)

	result, err = From(sliceStruct).ToString(UseJSON)
	assert.Equal(t, "[{\"Field\":\"value\"}]", result)
	assert.Nil(t, err)

	result, err = From(sliceStruct).ToString(UseRawString)
	assert.Equal(t, "[]convert.StructTest{convert.StructTest{Field:\"value\"}}", result)
	assert.Nil(t, err)
}

func TestToInt(t *testing.T) {
	var (
		result int
		err    error
	)

	result, err = From("123").ToInt()
	assert.Equal(t, 123, result)
	assert.Nil(t, err)

	result, err = From(123).ToInt()
	assert.Equal(t, 123, result)
	assert.Nil(t, err)

	structTest := StructTest{Field: "value"}

	_, err = From(structTest).ToInt()
	assert.NotNil(t, err)
	assert.Equal(t, ErrTypeFrom, err)
}

func TestToBool(t *testing.T) {
	var (
		result bool
		err    error
	)

	result, err = From(true).ToBool()
	assert.Equal(t, true, result)
	assert.Nil(t, err)

	result, err = From(false).ToBool()
	assert.Equal(t, false, result)
	assert.Nil(t, err)

	result, err = From("true").ToBool()
	assert.Equal(t, true, result)
	assert.Nil(t, err)

	result, err = From("false").ToBool()
	assert.Equal(t, false, result)
	assert.Nil(t, err)

	result, err = From(1).ToBool()
	assert.Equal(t, true, result)
	assert.Nil(t, err)

	result, err = From(0).ToBool()
	assert.Equal(t, false, result)
	assert.Nil(t, err)

	_, err = From("test").ToBool()
	assert.NotNil(t, err)
	assert.Equal(t, ErrTypeFrom, err)

	_, err = From(2).ToBool()
	assert.NotNil(t, err)
	assert.Equal(t, ErrTypeFrom, err)
}

func TestTo(t *testing.T) {
	var (
		err error
	)

	toStr := ""
	err = From("123").To(&toStr)
	assert.Equal(t, "123", toStr)
	assert.Nil(t, err)

	toInt := 0
	err = From(123).To(&toInt)
	assert.Equal(t, 123, toInt)
	assert.Nil(t, err)

	structTest := StructTest{Field: "value"}
	structDest := StructTest{}

	err = From("{\"Field\":\"value\"}").To(&structDest)
	assert.Equal(t, structTest, structDest)
	assert.Nil(t, err)

	testWithInterface := func(value interface{}) error {
		return From("{\"Field\":\"value\"}").To(&value)
	}

	structDestInterface := StructTest{}
	err = testWithInterface(&structDestInterface)
	assert.Equal(t, structTest, structDestInterface)
	assert.Nil(t, err)

	sliceTest := []StructTest{structTest}
	sliceDest := []StructTest{}

	err = From("[{\"Field\":\"value\"}]").To(&sliceDest)
	assert.Equal(t, sliceTest, sliceDest)
	assert.Nil(t, err)

	err = From("{\"Field\":\"value\"}").To(structDest)
	assert.NotNil(t, err)
}

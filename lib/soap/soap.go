package soap

import (
	"errors"
	"net/http"
	"strings"

	"bitbucket.org/eucatur/go-toolbox/std/log"
)

// GetContentType retorna um mime-type a partir do conteúdo da mensagem
func GetContentType(data []byte) (contentType string, ok bool) {
	if contentTypeParts := strings.Split(http.DetectContentType(data), ";"); len(contentTypeParts) > 0 {
		contentType = contentTypeParts[0]
		ok = len(contentType) > 0
	}

	return
}

// Verify if result content is equal to "text/xml" or "text/plain"
func verifyContentType(result []byte) (stringResult string, err error) {
	contentType, ok := GetContentType(result)

	if ok && (contentType != "text/xml") && (contentType != "text/plain") {
		err = errors.New("The result is an invalid type: " + contentType)
	}

	stringResult = string(result)

	return
}

// For use this method is necessary the curl library is installed in the host
func Post(url string, content []byte) (response string, err error) {
	respBytes, err := Curl(url, content)

	if err != nil {
		log.Error(err)
		return
	}

	return verifyContentType(respBytes)
}

// For use this method is necessary the curl library is installed in the host
func PostWithTLS(url string, content []byte, PEMPath string) (response string, err error) {
	respBytes, err := CurlWithCert(url, content, PEMPath)

	if err != nil {
		return
	}

	return verifyContentType(respBytes)
}

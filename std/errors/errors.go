// Package errors é um pacote para simplificar o tratamento de erros nas apps.
// O objetivo desse pacote é agrupar os erros comuns e os métodos utilizados para
// tratamento/conversão de erros.
package errors

import (
	liberrors "errors"
	"fmt"
	"runtime"
)

var (
	// ErrNoResult usado para representar uma busca que não teve resultados.
	ErrNoResult = liberrors.New("nenhum resultado encontrado")

	// ErrUnauthorized o acesso ao recurso não foi autorizado.
	ErrUnauthorized = liberrors.New("acesso não autorizado")

	// ErrForbidden o cliente não tem permissão para acessar o recurso.
	ErrForbidden = liberrors.New("sem permissão para acessar o recurso")
)

// New retorna uma instância de error.
//
//  // gerando um erro a partir de uma string
//  err := errors.New("mensagem de erro")
//
//  // gerando um erro a partir de um outro erro já existente
//  err := errors.New(errAnterior)
//
//  // gerando um erro, mas guardando o erro anterior como histórico
//  err := errors.New("nova mensagem de erro", errAnterior)
//
func New(err interface{}, previousError ...error) error {
	stackErr := newStackError(err, previousError...)
	return Error{stackErr}
}

// APP retorna uma instância de APPError.
//
//  // gerando um erro a partir de uma string
//  apperr := errors.APP("mensagem de erro")
//
//  // gerando um erro a partir de um outro erro já existente
//  apperr := errors.APP(errAnterior)
//
//  // gerando um erro, mas guardando o erro anterior como histórico
//  apperr := errors.APP("nova mensagem de erro", errAnterior)
//
func APP(message interface{}, previousError ...error) error {
	err := fmt.Sprintf("%v", message)
	stackErr := newStackError(err, previousError...)
	return APPError{err, stackErr}
}

// API retorna uma instância de APIError.
//
//  // gerando um erro a partir de uma string
//  apierr := errors.API(499, "mensagem de erro")
//
//  // gerando um erro a partir de um outro erro já existente
//  apierr := errors.API(499, errAnterior)
//
//  // gerando um erro, mas guardando o erro anterior como histórico
//  apierr := errors.API(499, "nova mensagem de erro", errAnterior)
//
func API(status int, message interface{}, previousError ...error) error {
	err := fmt.Sprintf("%v", message)
	stackErr := newStackError(err, previousError...)
	return APIError{status, err, stackErr}
}

// Descriptive retorna uma instância de DescriptiveError.
//
func Descriptive(code DescriptiveErrorCode, err error, details map[string]interface{}, previousError ...error) error {
	message := err.Error()
	stackErr := newStackError(err, previousError...)
	return DescriptiveError{code, message, details, stackErr}
}

// Error um erro genérico.
type Error struct {
	StackError
}

// APPError um erro gerado por uma aplicação.
type APPError struct {
	message string
	StackError
}

// Error retorna a mensagem de erro.
func (a APPError) Error() string {
	return a.message
}

// APIError um erro gerado por uma api.
type APIError struct {
	status  int
	message string
	StackError
}

// Status retorna o código de status do erro.
func (a APIError) Status() int {
	return a.status
}

// Message retorna a mensagem de erro.
func (a APIError) Message() string {
	return a.message
}

// Error retorna o código de status e a mensagem de erro.
func (a APIError) Error() string {
	return fmt.Sprintf("status: %d | message: %s", a.status, a.message)
}

// DescriptiveErrorCode `interface` para o código do erro
type DescriptiveErrorCode interface {
	ErrorCode() string
}

// DescriptiveError um erro detalhado com código.
type DescriptiveError struct {
	code    DescriptiveErrorCode
	message string
	details map[string]interface{}
	StackError
}

// Details retorna os detalhes do erro.
func (de DescriptiveError) Details() map[string]interface{} {
	if de.details == nil {
		de.details = make(map[string]interface{})
	}
	return de.details
}

// Error retorna o código, a mensagem e os detalhes do erro.
func (de DescriptiveError) Code() string {
	if de.code == nil {
		return ""
	}

	return de.code.ErrorCode()
}

// Error retorna o código, a mensagem e os detalhes do erro.
func (de DescriptiveError) Error() string {
	return fmt.Sprintf("err: %v %v %+v", de.code, de.message, de.details)
}

// Message retorna a mensagem de erro.
func (de DescriptiveError) Message() string {
	return de.message
}

// StackItem representa um item da pilha de erros.
type StackItem struct {
	File string
	Line int
	Err  string
}

// Error retorna a mensagem de erro.
func (s StackItem) Error() string {
	return s.Err
}

// String representação em string do `StackItem`.
func (s StackItem) String() string {
	return fmt.Sprintf("err: %s | file: %s:%d", s.Err, s.File, s.Line)
}

// StackError representa uma pilha de erros.
type StackError struct {
	stack []StackItem
}

// Stack retorna a pilha de errors.
func (e StackError) Stack() []StackItem {
	return e.stack
}

// PrintStack imprime a pilha de errors em formato string.
func (e StackError) PrintStack() {
	fmt.Println(e.SprintStack())
}

// SprintStack formata a pilha de errors em formato string.
func (e StackError) SprintStack() string {
	out := ""
	for i, v := range e.stack {
		out += fmt.Sprintf("[%d] %s\n", i, v.String())
	}
	return out
}

// Error retorna a mensagem de erro do último item da pilha de erros.
func (e StackError) Error() string {
	if len(e.stack) < 1 {
		return "errors.Error: empty stack"
	}

	return e.stack[len(e.stack)-1].Error()
}

func newStackError(err interface{}, previousError ...error) StackError {
	stack := make([]StackItem, 0)

	if len(previousError) > 0 {
		first := previousError[0]

		if e, ok := first.(Error); ok {
			stack = e.Stack()
		} else {
			stack = append(stack, StackItem{"", 0, fmt.Sprintf("%v", first)})
		}
	}

	_, file, line, _ := runtime.Caller(2)
	msg := fmt.Sprintf("%v", err)

	stack = append(stack, StackItem{file, line, msg})
	return StackError{stack}
}

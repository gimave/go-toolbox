package query

import (
	"fmt"
	"net/url"
	"reflect"
)

const TagDefault = "query"

// var tagDefault = "query"

// func SetTagDefault(newTagDefault string) {
// 	tagDefault = newTagDefault
// }

func Encode(a interface{}) (string, error) {

	var values = url.Values{}

	t := reflect.TypeOf(a)
	v := reflect.ValueOf(a)

	// remove o ponteiro
	if t.Kind() == reflect.Ptr {
		t = t.Elem()
	}

	// remove o ponteiro
	if v.Kind() == reflect.Ptr {
		v = v.Elem()
	}

	// trata structs
	if t.Kind() == reflect.Struct {
		for i := 0; i < t.NumField(); i++ {

			// ignora os campos de tipos não suportados
			// Func
			// Interface
			// Map
			// Ptr
			// Slice
			// Struct
			// UnsafePointer
			if v.Field(i).Kind() == reflect.Invalid ||
				v.Field(i).Kind() == reflect.Uintptr ||
				v.Field(i).Kind() == reflect.Uintptr ||
				v.Field(i).Kind() == reflect.Complex64 ||
				v.Field(i).Kind() == reflect.Complex128 ||
				v.Field(i).Kind() == reflect.Array ||
				v.Field(i).Kind() == reflect.Chan ||
				v.Field(i).Kind() == reflect.Func ||
				v.Field(i).Kind() == reflect.Interface ||
				v.Field(i).Kind() == reflect.Map ||
				v.Field(i).Kind() == reflect.Ptr ||
				v.Field(i).Kind() == reflect.Slice ||
				v.Field(i).Kind() == reflect.Struct ||
				v.Field(i).Kind() == reflect.UnsafePointer {
				continue
			}

			// ignora os campos onde não podemos obter a interface
			if !v.Field(i).CanInterface() {
				continue
			}

			var (
				key   = t.Field(i).Tag.Get(TagDefault)
				value = fmt.Sprint(v.Field(i).Interface())
			)

			if key == "" {
				key = t.Field(i).Name
			}

			values.Add(key, value)
		}
	}

	return values.Encode(), nil
}

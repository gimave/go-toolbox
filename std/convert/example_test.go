package convert_test

import (
	"fmt"

	"bitbucket.org/eucatur/go-toolbox/std/convert"
)

func Example() {

	// conversão de int para string
	resStr, err := convert.From(123).ToString()
	fmt.Printf("resultado: %#v | err: %+v\n", resStr, err)

	// conversão de string para int
	resInt, err := convert.From("123").ToInt()
	fmt.Printf("resultado: %#v | err: %+v\n", resInt, err)

	// conversão de string para bool
	resBool, err := convert.From("true").ToBool()
	fmt.Printf("resultado: %#v | err: %+v\n", resBool, err)

	// conversão de int para bool
	resBool, err = convert.From(0).ToBool()
	fmt.Printf("resultado: %#v | err: %+v\n", resBool, err)

	// conversão de string para struct
	type StructTest struct {
		Field string
	}

	resStruct := StructTest{}
	err = convert.From("{\"Field\":\"value\"}").To(&resStruct)
	fmt.Printf("resultado: %#v | err: %+v\n", resStruct, err)

	// Output:
	// resultado: "123" | err: <nil>
	// resultado: 123 | err: <nil>
	// resultado: true | err: <nil>
	// resultado: false | err: <nil>
	// resultado: convert_test.StructTest{Field:"value"} | err: <nil>
}

package telemetry

import (
	"bytes"
	"sync"
)

// SyncBuffer é um thread-safe buffer usado para armazenar os
// dados coletados antes de serem armazenados no storage.
type SyncBuffer struct {
	b bytes.Buffer
	m sync.Mutex
}

// Write escreve os dados no buffer.
func (s *SyncBuffer) Write(p []byte) (n int, err error) {
	s.m.Lock()
	defer s.m.Unlock()
	return s.b.Write(p)
}

// Bytes retorna os dados armazenados no buffer.
func (s *SyncBuffer) Bytes() []byte {
	s.m.Lock()
	defer s.m.Unlock()
	return s.b.Bytes()
}

// Reset reseta os dados do buffer.
func (s *SyncBuffer) Reset() {
	s.m.Lock()
	defer s.m.Unlock()
	s.b.Reset()
}

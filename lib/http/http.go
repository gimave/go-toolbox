// Package http é um pacote com funções para simplificar procedimentos que
// envolvam o protocolo HTTP.
package http

// Body o body para ser enviado com a Response.
type Body map[string]interface{}

// BodyMessage um atalho para gerar um JSON padrão em caso de sucesso.
//
//  body := http.BodyMessage("executado com sucesso")
//
func BodyMessage(content interface{}) Body {
	return Body{"mensagem": content}
}

// BodyError um atalho para gerar um JSON padrão em caso de erro.
//
//  body := http.BodyError("ocorreu um erro durante a execução")
//
func BodyError(content interface{}) Body {
	return Body{"erro": content}
}

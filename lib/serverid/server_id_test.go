package serverid

import (
	"strings"
	"testing"

	"bitbucket.org/eucatur/go-toolbox/std/net"

	"github.com/stretchr/testify/assert"
)

func TestGenerateFromLocalIP(t *testing.T) {
	ip, err := net.GetLocalIP()
	assert.NotEmpty(t, ip)
	assert.Nil(t, err)

	id, err := GenerateFromLocalIP(1, "")
	assert.Equal(t, ip[strings.LastIndex(ip, ".")+1:], id)
	assert.Nil(t, err)

	var ipEnd string
	ip, ipEnd = ip[:strings.LastIndex(ip, ".")], ip[strings.LastIndex(ip, ".")+1:]

	id, err = GenerateFromLocalIP(2, ".")
	assert.Equal(t, ip[strings.LastIndex(ip, ".")+1:]+"."+ipEnd, id)
	assert.Nil(t, err)

	ip, ipEnd = ip[:strings.LastIndex(ip, ".")], ip[strings.LastIndex(ip, ".")+1:]+"."+ipEnd

	id, err = GenerateFromLocalIP(3, ".")
	assert.Equal(t, ip[strings.LastIndex(ip, ".")+1:]+"."+ipEnd, id)
	assert.Nil(t, err)
}

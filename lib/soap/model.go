package soap

type Envelope struct {
	XMLName struct{} `xml:"http://www.w3.org/2003/05/soap-envelope Envelope"`
	Body    EnvelopeBody
}

type EnvelopeBody struct {
	XMLName struct{} `xml:"http://www.w3.org/2003/05/soap-envelope Body"`
	Content []byte   `xml:",innerxml"`
}

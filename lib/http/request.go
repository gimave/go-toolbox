package http

import (
	"strconv"

	"bitbucket.org/eucatur/go-toolbox/lib/http/middleware/authrequest"
	"bitbucket.org/eucatur/go-toolbox/lib/validator"
	"bitbucket.org/eucatur/go-toolbox/std/errors"

	"github.com/labstack/echo"
	uuid "github.com/satori/go.uuid"
)

func init() {
	validator.SetFuncMarshalJSON(func(vError *validator.VError) interface{} {
		return map[string]interface{}{
			"erro": map[string]interface{}{
				"mensagem": vError.Message,
				"detalhes": vError.Details,
			},
		}
	})
}

// NewRequest retorna uma nova instância de Request.
func NewRequest(c echo.Context) *Request {
	return &Request{c, nil}
}

// Request disponibiliza métodos para lidar com as requests
// (requisições) no protocolo http.
type Request struct {
	context echo.Context
	Body    interface{}
}

// Ctx retorna o echo.Context para qual essa requisição foi criada.
func (r Request) Ctx() echo.Context {
	return r.context
}

// Bind faz o bind do body do request para o parâmetro 'object'.
//
//  filtro := seccionamento.NewFiltro()
//
//  if err = req.Bind(&filtro); err != nil {
//      return handler.Fail(err)
//  }
//
func (r Request) Bind(object interface{}) error {
	return r.context.Bind(object)
}

// Validate faz a validação das informações presentes no parâmetro 'object'.
//
//  if err = req.Validate(&filtro); err != nil {
//      return handler.Fail(err)
//  }
//
func (r Request) Validate(object interface{}, options ...string) error {
	vError := validator.Validate(object, options...)

	if vError != nil {
		return vError
	}

	return nil
}

// BindAndValidate faz o bind do body do request e também a validação das informações.
//
//  filtro := seccionamento.NewFiltro()
//
//  if err = req.BindAndValidate(&filtro); err != nil {
//      return handler.Fail(err)
//  }
//
func (r Request) BindAndValidate(object interface{}, options ...string) error {
	var err error

	if err = r.Bind(object); err == nil {
		err = r.Validate(object, options...)
	}

	return err
}

// GetParamInt busca o 'param' nos parâmetros que vieram via URL param.
//
//  // url -> "/seccionamentos/:seccionamento_id"
//  if id, err = req.GetParamInt("seccionamento_id"); err != nil {
//      return handler.Fail(err)
//  }
//
func (r Request) GetParamInt(param string) (int, error) {
	return strconv.Atoi(r.context.Param(param))
}

// GetParamUUID busca o 'param' nos parâmetros que vieram via URL param.
//
//  // url -> "/pagamentos/:pagamento_id"
//  if id, err = req.GetParamUUID("pagamento_id"); err != nil {
//      return handler.Fail(err)
//  }
//
func (r Request) GetParamUUID(param string) (uuid.UUID, error) {
	return uuid.FromString(r.context.Param(param))
}

// GetAuthorizedClient busca o cliente da api autenticado que está gravado no context.
//
//  if cliente, err = req.GetAuthorizedClient(); err != nil {
//		return handler.Fail(err)
//	}
//
func (r Request) GetAuthorizedClient() (client string, err error) {
	client = r.context.Get(authrequest.AuthorizedClientCtx).(string)
	if client == "" {
		err = errors.ErrUnauthorized
	}
	return
}

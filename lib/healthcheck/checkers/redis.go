package checkers

import (
	"errors"
	"strings"

	libredis "bitbucket.org/eucatur/go-toolbox/lib/redis"

	healthredis "github.com/dimiro1/health/redis"
	"github.com/garyburd/redigo/redis"
)

// NewRedis retorna uma nova instância de Checker.
func NewRedis() healthredis.Checker {
	return healthredis.NewCheckerWithRedis(RedisChecker{})
}

// RedisChecker serviço responsável pela "checagem da saúde" do redis.
type RedisChecker struct{}

// GetVersion checa se o redis está funcionando.
func (r RedisChecker) GetVersion() (string, error) {
	version, err := libredis.Exec(func(conn redis.Conn) (interface{}, error) {
		var (
			data string
			err  error
		)

		if data, err = redis.String(conn.Do("INFO")); err != nil {
			return "", err
		}

		for _, line := range strings.Split(data, "\r\n") {
			if strings.Contains(line, "redis_version") {
				values := strings.Split(line, ":")
				if len(values) > 1 {
					return values[1], nil
				}
			}
		}

		return "", errors.New("healthcheck/checkers: erro ao buscar informações no redis")
	})

	return version.(string), err
}

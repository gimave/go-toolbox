package whatsapp

import (
	"fmt"
)

type Device struct {
	Serial string
	busy   bool
	errors []error
}

func (d *Device) Lock() {
	fmt.Printf("[LIB/WHATSAPP][DEVICE:%s]: lock\n", d.Serial)
	d.busy = true
}

func (d *Device) Unlock() {
	fmt.Printf("[LIB/WHATSAPP][DEVICE:%s]: unlock\n", d.Serial)
	d.busy = false
}

func (d *Device) AddError(err error) {
	fmt.Printf("[LIB/WHATSAPP][DEVICE:%s]: AddError\n", d.Serial)
	if err != nil {
		d.errors = append(d.errors, err)
	}
}

func (d *Device) Errors() []error {
	fmt.Printf("[LIB/WHATSAPP][DEVICE:%s]: Errors\n", d.Serial)
	return d.errors
}

func (d *Device) LenErrors() int {
	fmt.Printf("[LIB/WHATSAPP][DEVICE:%s]: LenErrors\n", d.Serial)
	return len(d.errors)
}

func (d *Device) IsAvailable() bool {
	fmt.Printf("[LIB/WHATSAPP][DEVICE:%s]: available? %t, errors: %+v\n", d.Serial, !d.busy, d.errors)
	return !d.busy && len(d.errors) == 0
}

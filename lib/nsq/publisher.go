package nsq

import (
	"encoding/json"
	"errors"
)

// Publisher é a estrutura do publicador
type Publisher struct {
	topic string
	body  interface{}
	err   error
}

// NewPublisher instância um publicador
func NewPublisher(topic string) *Publisher {

	p := &Publisher{}

	if topic == "" {
		p.err = errors.New("informe um topic válido")
		return p
	}

	p.topic = topic
	return p
}

// Body define o body da publicação
func (p *Publisher) Body(body interface{}) *Publisher {

	if p.err != nil {
		return p
	}

	if body == nil {
		p.err = errors.New("informe um body válido")
		return p
	}

	p.body = body
	return p
}

// Send envia a publicação ao topic
func (p *Publisher) Send() error {

	if p.err != nil {
		return p.err
	}

	var (
		body []byte
		err  error
	)

	if body, err = json.Marshal(p.body); err != nil {
		return err
	}

	return producer.Publish(p.topic, body)
}

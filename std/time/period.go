package time

import (
	"time"
)

// Period representa um período específico de tempo com data inicial e final.
type Period struct {
	start time.Time
	end   time.Time
}

// NewPeriod retorna uma nova instância de Period.
//
//  periodo := NewPeriod(dataInicial, dataFinal)
//
func NewPeriod(start, end time.Time) Period {
	return Period{start, end}
}

// Start retorna a data inicial do período.
//
//  inicio := periodo.Start()
//
func (p Period) Start() time.Time {
	return p.start
}

// End retorna a data final do período.
//
//  fim := periodo.End()
//
func (p Period) End() time.Time {
	return p.end
}

// Package search realiza buscas por uma informação usando um filtro como parâmetro.
// Esse pacote pode utilizar um serviço de cache para agilizar as consultas.
// Normalmente, as buscas serão feitas num banco de dados e armazenadas no redis
// como serviço de cache.
//
// Exemplos de configuração desse pacote:
//
//  //Exemplo 1: configurando o serviço de busca sem cache.
//  // procedimento que retorna as informações
//  proc := func(filter query.Filter) (interface{}, error) {
//      return database.buscarPorFiltro(filter.(Filtro))
//  }
//
//  // inicia um service de busca, que retornará sempre um slice de
//  // seccionamento ([]Seccionamento{}) usando a função proc acima.
//  service := search.For([]Seccionamento{}).Use(proc)
//
//  // Exemplo 2: configurando o serviço de busca com cache.
//  // procedimento que retorna as informações
//  proc := func(filter query.Filter) (interface{}, error) {
//      return database.buscarPorFiltro(filter.(Filtro))
//  }
//
//  // configura o cache que será usado pelo service.
//  cache := search.Cache{
//      Service:    libcache.NewRedis("api-services-v2.busca.seccionamentos"),
//      Expiration: time.Duration(2) * time.Minute,
//  }
//
//  // inicia um service de busca, que retornará sempre um slice de
//  // seccionamento ([]Seccionamento{}) usando a função proc acima e armazenando
//  // os dados no redis com expiração de 2 minutos.
//  service := search.For([]Seccionamento{}).Use(proc).WithCache(cache)
//
package search

import (
	"fmt"
	"reflect"
	"time"

	"bitbucket.org/eucatur/go-toolbox/app/env"
	"bitbucket.org/eucatur/go-toolbox/lib/cache"
	"bitbucket.org/eucatur/go-toolbox/lib/database/query"
	"bitbucket.org/eucatur/go-toolbox/lib/debugger"
	"bitbucket.org/eucatur/go-toolbox/lib/objectid"
	"bitbucket.org/eucatur/go-toolbox/std/errors"
)

// Proc procedimento que será executado para buscar os dados.
// Normalmente, essa função será implementada por um DB.
//
// ATENÇÃO: o retorno dessa função (interface{}) deve ser um slice.
type Proc func(filter query.Filter) (interface{}, error)

// Cache configuração das opções de cache para o service de busca.
type Cache struct {
	// Service repositório onde os dados de cache serão armazenados.
	Service cache.Service
	// Expiration tempo para expiração do cache.
	Expiration time.Duration
}

// Service responsável pela execução de buscas usando um filtro como parâmetro.
// Esse service também pode armazenar e buscar os dados usando um sistema de cache.
type Service struct {
	// emptySlice é um slice vazio, esse é o tipo de slice que será usado
	// para os retornos dos métodos Select e Get.
	emptySlice interface{}
	// proc é o procedimento que será executado para buscar os dados.
	proc Proc
	// cache é um objeto de configuração para definir o comportamento do service
	// em relação ao armazenamento de cache.
	cache Cache
}

// For é utilizado para instanciar o service.
func For(emptySlice interface{}) *Service {
	return &Service{emptySlice: emptySlice}
}

// Use configura o procedimento que será executado para buscar os dados.
func (s *Service) Use(proc Proc) *Service {
	s.proc = proc
	return s
}

// WithCache configura as informações do serviço de cache.
func (s *Service) WithCache(cache Cache) *Service {
	s.cache = cache
	return s
}

// Get faz uma busca por um único item.
//
//  // usando o service para buscar por Seccionamento{}
//  var resultado interface{}
//  filtro := Filtro{ID: id}
//  if resultado, err = busca.service.Get(filtro); err == nil {
//      seccionamento = resultado.(Seccionamento)
//  }
//  return seccionamento, err
//
func (s *Service) Get(filter query.Filter) (interface{}, error) {
	var (
		result interface{}
		err    error
	)

	if err = s.checkConfig(); err != nil {
		return result, err
	}

	if result, err = s.Select(filter); err != nil {
		return result, err
	}

	elements := reflect.ValueOf(result)

	if elements.Len() < 1 {
		return result, errors.ErrNoResult
	}

	return elements.Index(0).Interface(), nil
}

// Select faz uma busca por um slice de itens.
//
//  // usando o service para buscar por []Seccionamento{}
//  var resultado interface{}
//  if resultado, err = busca.service.Select(filtro); err == nil {
//      seccionamentos = resultado.([]Seccionamento)
//  }
//  return seccionamentos, err
//
func (s *Service) Select(filter query.Filter) (interface{}, error) {
	var (
		result interface{}
		err    error
	)

	if err = s.checkConfig(); err != nil {
		return result, err
	}

	pointerToStruct := reflect.New(reflect.TypeOf(s.emptySlice)).Interface()

	if ok := s.cacheGet(filter, &pointerToStruct); ok {
		return reflect.ValueOf(pointerToStruct).Elem().Interface(), nil
	}

	if result, err = s.proc(filter); err != nil {
		return result, err
	}

	if !isSlice(result) {
		return result, errors.New("database:search: o retorno do 'proc' deve ser um slice")
	}

	elements := reflect.ValueOf(result)

	if elements.Len() < 1 {
		return s.emptySlice, nil
	}

	s.cacheSet(filter, result)

	return result, nil
}

func (s *Service) cacheSet(filter query.Filter, result interface{}) {
	if s.cache.Service != nil {
		key := objectid.GenerateHashFor(filter)

		err := s.cache.Service.Set(key, result, s.cache.Expiration)

		if err != nil && err != cache.ErrServiceDisabled {
			debugger.FileLine(fmt.Sprintf("%v", err))
		}
	}
}

func (s *Service) cacheGet(filter query.Filter, pointerToStruct interface{}) bool {
	if s.cache.Service == nil {
		return false
	}

	key := objectid.GenerateHashFor(filter)

	err := s.cache.Service.Get(key, pointerToStruct)

	if err != nil {
		if err != cache.ErrServiceDisabled && err != cache.ErrNotCached {
			debugger.FileLine(fmt.Sprintf("%v", err))
		}

		return false
	}

	return true
}

func (s *Service) checkConfig() error {
	if !isSlice(s.emptySlice) {
		return errors.New("database:search: o método 'For(emptySlice interface{})' deve ser chamado para configurar esse service")
	}

	if s.proc == nil {
		return errors.New("database:search: o método 'Use(proc Proc)' deve ser chamado para configurar esse service")
	}

	if s.cache.Service != nil {
		// go armazena o time.Duration em nanosegundos internamente, então:
		// cache.Expiration == 1000 significa 1 segundo, esse é o tempo mínimo
		// de cache que nós consideramos como válido.
		if s.cache.Expiration < 1000 {
			expiration, err := env.ReqInt("API_CACHE_EXPIRACAO")
			if err != nil {
				return errors.New("database:search: a variável de ambiente `API_CACHE_EXPIRACAO` não está definida")
			}

			s.cache.Expiration = time.Duration(expiration) * time.Minute

			if s.cache.Expiration < 1000 {
				return errors.New("database:search: a variável de ambiente `API_CACHE_EXPIRACAO` ou o parâmetro " +
					"`Cache.Expiration` deve estar configurado para usar a lib de cache")
			}
		}
	}

	return nil
}

func isSlice(object interface{}) bool {
	return reflect.TypeOf(object).Kind() == reflect.Slice
}

package env

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

var all = MapEnv{
	"STRING":      "text",
	"SMALL_INT_1": "0",
	"SMALL_INT_2": "1",
	"BIG_INT":     "123456",
	"BOOL_1":      "true",
	"BOOL_2":      "1",
	"BOOL_3":      "false",
	"BOOL_4":      "0",
}

func init() {
	Configure(all)
}

func TestAll(t *testing.T) {
	assert.Equal(t, all, All())
}

func TestBool(t *testing.T) {
	assert.Equal(t, true, Bool("STRING", true))

	assert.Equal(t, false, Bool("SMALL_INT_1", true))
	assert.Equal(t, true, Bool("SMALL_INT_2", false))
	assert.Equal(t, false, Bool("BIG_INT", false))

	assert.Equal(t, true, Bool("BOOL_1", false))
	assert.Equal(t, true, Bool("BOOL_2", false))
	assert.Equal(t, false, Bool("BOOL_3", true))
	assert.Equal(t, false, Bool("BOOL_4", true))

	assert.Equal(t, true, Bool("UNDEFINED", true))
}

func TestString(t *testing.T) {
	assert.Equal(t, "text", String("STRING", "default"))

	assert.Equal(t, "0", String("SMALL_INT_1", "default"))
	assert.Equal(t, "1", String("SMALL_INT_2", "default"))
	assert.Equal(t, "123456", String("BIG_INT", "default"))

	assert.Equal(t, "true", String("BOOL_1", "default"))
	assert.Equal(t, "1", String("BOOL_2", "default"))
	assert.Equal(t, "false", String("BOOL_3", "default"))
	assert.Equal(t, "0", String("BOOL_4", "default"))

	assert.Equal(t, "default", String("UNDEFINED", "default"))
}

func TestInt(t *testing.T) {
	assert.Equal(t, -1, Int("STRING", -1))

	assert.Equal(t, 0, Int("SMALL_INT_1", -1))
	assert.Equal(t, 1, Int("SMALL_INT_2", -1))
	assert.Equal(t, 123456, Int("BIG_INT", -1))

	assert.Equal(t, -1, Int("BOOL_1", -1))
	assert.Equal(t, 1, Int("BOOL_2", -1))
	assert.Equal(t, -1, Int("BOOL_3", -1))
	assert.Equal(t, 0, Int("BOOL_4", -1))

	assert.Equal(t, -1, Int("UNDEFINED", -1))
}

func TestMustBool(t *testing.T) {
	assert.Panics(t, func() { MustBool("STRING") })
	assert.Panics(t, func() { MustBool("BIG_INT") })
	assert.Panics(t, func() { MustBool("UNDEFINED") })

	assert.Equal(t, false, MustBool("SMALL_INT_1"))
	assert.Equal(t, true, MustBool("SMALL_INT_2"))

	assert.Equal(t, true, MustBool("BOOL_1"))
	assert.Equal(t, true, MustBool("BOOL_2"))
	assert.Equal(t, false, MustBool("BOOL_3"))
	assert.Equal(t, false, MustBool("BOOL_4"))
}

func TestMustString(t *testing.T) {
	assert.Panics(t, func() { MustString("UNDEFINED") })

	assert.Equal(t, "text", MustString("STRING"))

	assert.Equal(t, "0", MustString("SMALL_INT_1"))
	assert.Equal(t, "1", MustString("SMALL_INT_2"))
	assert.Equal(t, "123456", MustString("BIG_INT"))

	assert.Equal(t, "true", MustString("BOOL_1"))
	assert.Equal(t, "1", MustString("BOOL_2"))
	assert.Equal(t, "false", MustString("BOOL_3"))
	assert.Equal(t, "0", MustString("BOOL_4"))
}

func TestMustInt(t *testing.T) {
	assert.Panics(t, func() { MustInt("STRING") })
	assert.Panics(t, func() { MustInt("BOOL_1") })
	assert.Panics(t, func() { MustInt("BOOL_3") })
	assert.Panics(t, func() { MustInt("UNDEFINED") })

	assert.Equal(t, 0, MustInt("SMALL_INT_1"))
	assert.Equal(t, 1, MustInt("SMALL_INT_2"))
	assert.Equal(t, 123456, MustInt("BIG_INT"))

	assert.Equal(t, 1, MustInt("BOOL_2"))
	assert.Equal(t, 0, MustInt("BOOL_4"))
}

func TestReqBool(t *testing.T) {
	var (
		result bool
		err    error
	)

	result, err = ReqBool("STRING")
	assert.Equal(t, false, result)
	assert.NotNil(t, err)

	result, err = ReqBool("SMALL_INT_1")
	assert.Equal(t, false, result)
	assert.Nil(t, err)

	result, err = ReqBool("SMALL_INT_2")
	assert.Equal(t, true, result)
	assert.Nil(t, err)

	result, err = ReqBool("BIG_INT")
	assert.Equal(t, false, result)
	assert.NotNil(t, err)

	result, err = ReqBool("BOOL_1")
	assert.Equal(t, true, result)
	assert.Nil(t, err)

	result, err = ReqBool("BOOL_2")
	assert.Equal(t, true, result)
	assert.Nil(t, err)

	result, err = ReqBool("BOOL_3")
	assert.Equal(t, false, result)
	assert.Nil(t, err)

	result, err = ReqBool("BOOL_4")
	assert.Equal(t, false, result)
	assert.Nil(t, err)

	result, err = ReqBool("UNDEFINED")
	assert.Equal(t, false, result)
	assert.NotNil(t, err)
}

func TestReqString(t *testing.T) {
	var (
		result string
		err    error
	)

	result, err = ReqString("STRING")
	assert.Equal(t, "text", result)
	assert.Nil(t, err)

	result, err = ReqString("SMALL_INT_1")
	assert.Equal(t, "0", result)
	assert.Nil(t, err)

	result, err = ReqString("SMALL_INT_2")
	assert.Equal(t, "1", result)
	assert.Nil(t, err)

	result, err = ReqString("BIG_INT")
	assert.Equal(t, "123456", result)
	assert.Nil(t, err)

	result, err = ReqString("BOOL_1")
	assert.Equal(t, "true", result)
	assert.Nil(t, err)

	result, err = ReqString("BOOL_2")
	assert.Equal(t, "1", result)
	assert.Nil(t, err)

	result, err = ReqString("BOOL_3")
	assert.Equal(t, "false", result)
	assert.Nil(t, err)

	result, err = ReqString("BOOL_4")
	assert.Equal(t, "0", result)
	assert.Nil(t, err)

	result, err = ReqString("UNDEFINED")
	assert.Equal(t, "", result)
	assert.NotNil(t, err)
}

func TestReqInt(t *testing.T) {
	var (
		result int
		err    error
	)

	result, err = ReqInt("STRING")
	assert.Equal(t, 0, result)
	assert.NotNil(t, err)

	result, err = ReqInt("SMALL_INT_1")
	assert.Equal(t, 0, result)
	assert.Nil(t, err)

	result, err = ReqInt("SMALL_INT_2")
	assert.Equal(t, 1, result)
	assert.Nil(t, err)

	result, err = ReqInt("BIG_INT")
	assert.Equal(t, 123456, result)
	assert.Nil(t, err)

	result, err = ReqInt("BOOL_1")
	assert.Equal(t, 0, result)
	assert.NotNil(t, err)

	result, err = ReqInt("BOOL_2")
	assert.Equal(t, 1, result)
	assert.Nil(t, err)

	result, err = ReqInt("BOOL_3")
	assert.Equal(t, 0, result)
	assert.NotNil(t, err)

	result, err = ReqInt("BOOL_4")
	assert.Equal(t, 0, result)
	assert.Nil(t, err)

	result, err = ReqInt("UNDEFINED")
	assert.Equal(t, 0, result)
	assert.NotNil(t, err)
}

package slices

// ExistsValue verifica se o valor existe no slice
func ExistsValue(value string, values []string) bool {
	for _, v := range values {
		if value == v {
			return true
		}
	}
	return false
}

// ExistsValueInt verifica se o valor existe no slice de inteiros
func ExistsValueInt(value int, values []int) bool {
	for _, v := range values {
		if value == v {
			return true
		}
	}
	return false
}

// ExistsValueFloat64 verifica se o valor existe no slice de float64
func ExistsValueFloat64(value float64, values []float64) bool {
	for _, v := range values {
		if value == v {
			return true
		}
	}
	return false
}

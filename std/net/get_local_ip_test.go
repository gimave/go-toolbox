package net

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetLocalIP(t *testing.T) {
	ip, err := GetLocalIP()

	// Colocar um IP estático aqui para comparar com o resultado da função
	// vai causar um erro quando esse teste rodar numa máquina com IP diferente.
	// Só testar se o valor da variável "ip" não está vazia é o suficiente no momento.
	assert.NotEmpty(t, ip)
	assert.Nil(t, err)
}

package types

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestBool(t *testing.T) {
	var b Bool = -1
	assert.Equal(t, false, b.IsFalse())
	assert.Equal(t, false, b.IsTrue())
	assert.Equal(t, false, b.Value())
	assert.Equal(t, true, b.Empty())

	b = 0
	assert.Equal(t, true, b.IsFalse())
	assert.Equal(t, false, b.IsTrue())
	assert.Equal(t, false, b.Value())
	assert.Equal(t, false, b.Empty())

	b = 1
	assert.Equal(t, false, b.IsFalse())
	assert.Equal(t, true, b.IsTrue())
	assert.Equal(t, true, b.Value())
	assert.Equal(t, false, b.Empty())
}

package datetime

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestEUAtoBR(t *testing.T) {
	assert.Equal(t, "31/12/2017 20:30:45", EUAtoBR("2017-12-31 20:30:45"))
	assert.Equal(t, "", EUAtoBR("2017-31-17 20:30:45"))
}

func TestEUAtoBRShort(t *testing.T) {
	assert.Equal(t, "31/12/2017 20:30", EUAtoBRShort("2017-12-31 20:30:45"))
	assert.Equal(t, "", EUAtoBRShort("2017-31-17 20:30:45"))
}

func TestBRShort(t *testing.T) {
	ref, _ := time.Parse("2006-01-02 15:04:05", "2017-12-31 20:30:45")
	assert.Equal(t, "31/12/2017 20:30", BRShort(ref))
}

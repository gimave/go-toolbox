package middleware

import (
	"log"
	"regexp"
	"strings"

	"github.com/labstack/echo"

	"github.com/gin-gonic/gin"
)

// Platform, Mobile, Aplicativo, Desktop constantes usadas pra pegar a plataforma
const (
	Platform   = "platform"
	Mobile     = "Mobile"
	Aplicativo = "Aplicativo"
	Desktop    = "Desktop"
)

func getPlataformByUserAgent(userAgent string) (string, error) {
	userAgent = strings.ToLower(userAgent)

	regexAplicativo := regexp.MustCompile(`(; wv\)|aplicativo)`)
	if regexAplicativo.MatchString(userAgent) {
		return Aplicativo, nil
	}

	rgxMobile := regexp.MustCompile(`(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|up\.browser|up\.link|webos|wos)`)
	if rgxMobile.MatchString(userAgent) {
		return Mobile, nil
	}

	return Desktop, nil
}

// SetPlatformGin set os dados da plafaforma usando o GIN
func SetPlatformGin() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			platformType string
			err          error
		)

		userAgent := c.Request.Header.Get("User-Agent")

		if platformType, err = getPlataformByUserAgent(userAgent); err != nil {
			log.Println(err)
		} else {
			c.Set(Platform, platformType)
		}
	}
}

// SetPlatformEcho set os dados da plafaforma usando o ECHO
func SetPlatformEcho() echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) (err error) {
			var platformType string

			userAgent := c.Request().Header.Get("User-Agent")

			if platformType, err = getPlataformByUserAgent(userAgent); err != nil {
				log.Println(err)
			} else {
				c.Set(Platform, platformType)
			}

			return next(c)
		}
	}
}

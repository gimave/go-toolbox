// Package cache é uma lib com funções para realização de cache de dados.
package cache

import (
	"errors"
	"time"
)

var (
	// ErrServiceDisabled o serviço de cache está desabilitado.
	ErrServiceDisabled = errors.New("cache: o serviço de cache está desabilitado")

	// ErrNotCached a informação não está armazenada em cache.
	ErrNotCached = errors.New("cache: a informação não está armazenada em cache")
)

// Service interface que representa os métodos que devem estar disponíveis em um
// serviço de cache.
type Service interface {
	// IsEnabled retorna se o serviço de cache está habilitado.
	IsEnabled() bool
	// Set salva uma informação no cache.
	Set(key string, value interface{}, expiration time.Duration) error
	// Get busca uma informação no cache.
	Get(key string, value interface{}) error
	// Del remove uma informação do cache.
	Del(key string) error
}

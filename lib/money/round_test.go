package money

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestRound(t *testing.T) {
	assert.Equal(t, 1.49, Round(1.490, 2))
	assert.Equal(t, 1.49, Round(1.491, 2))
	assert.Equal(t, 1.49, Round(1.492, 2))
	assert.Equal(t, 1.49, Round(1.493, 2))
	assert.Equal(t, 1.49, Round(1.494, 2))
	assert.Equal(t, 1.50, Round(1.495, 2))
	assert.Equal(t, 1.50, Round(1.496, 2))
	assert.Equal(t, 1.50, Round(1.497, 2))
	assert.Equal(t, 1.50, Round(1.498, 2))
	assert.Equal(t, 1.50, Round(1.499, 2))

	assert.Equal(t, 1.499, Round(1.4990, 3))
	assert.Equal(t, 1.498, Round(1.4981, 3))
	assert.Equal(t, 1.497, Round(1.4972, 3))
	assert.Equal(t, 1.496, Round(1.4963, 3))
	assert.Equal(t, 1.495, Round(1.4954, 3))
	assert.Equal(t, 1.495, Round(1.4945, 3))
	assert.Equal(t, 1.494, Round(1.4936, 3))
	assert.Equal(t, 1.493, Round(1.4927, 3))
	assert.Equal(t, 1.492, Round(1.4918, 3))
	assert.Equal(t, 1.491, Round(1.4909, 3))

	assert.Equal(t, 1.50, Round(1.4990, 2))
	assert.Equal(t, 1.50, Round(1.4981, 2))
	assert.Equal(t, 1.50, Round(1.4972, 2))
	assert.Equal(t, 1.50, Round(1.4963, 2))
	assert.Equal(t, 1.50, Round(1.4954, 2))
	assert.Equal(t, 1.49, Round(1.4945, 2))
	assert.Equal(t, 1.49, Round(1.4936, 2))
	assert.Equal(t, 1.49, Round(1.4927, 2))
	assert.Equal(t, 1.49, Round(1.4918, 2))
	assert.Equal(t, 1.49, Round(1.4909, 2))
}

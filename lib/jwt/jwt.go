package jwt

import (
	"errors"

	jwt_go "github.com/dgrijalva/jwt-go"
)

func CreateTokenWithClaims(claims jwt_go.MapClaims, secret string) (token string, err error) {
	t := jwt_go.NewWithClaims(jwt_go.SigningMethodHS384, claims)

	token, err = t.SignedString([]byte(secret))

	return
}

func VerifyTokenAndGetClaims(tokenString, secret string) (map[string]interface{}, error) {
	token, err := jwt_go.Parse(tokenString, func(token *jwt_go.Token) (interface{}, error) {
		return []byte(secret), nil
	})

	if token == nil {
		return nil, errors.New("Invalid Token")
	}

	if claims, ok := token.Claims.(jwt_go.MapClaims); ok && token.Valid {
		return claims, nil
	} else {
		return nil, err
	}
}

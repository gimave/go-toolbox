package errors

import (
	"errors"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

var currentFile = "bitbucket.org/eucatur/go-toolbox/std/errors/errors_test.go"

func TestNew(t *testing.T) {
	err := New("error message")
	assert.Equal(t, "error message", err.Error())
	assert.Error(t, err)

	err = New(errors.New("error [0]"))
	testErrorStack(t, err, "error [0]", currentFile, 18, 0)

	err = New("error [1]", err)
	testErrorStack(t, err, "error [1]", currentFile, 21, 1)

	err = New(New("error [2]"), err)
	testErrorStack(t, err, "error [2]", currentFile, 24, 2)

	err = New(errors.New("error [A]"), errors.New("previous error"))
	testErrorStack(t, err, "previous error", "", 0, 0)
	testErrorStack(t, err, "error [A]", currentFile, 27, 1)
}

func TestAPP(t *testing.T) {
	err := APP("error message")
	assert.Equal(t, "error message", err.Error())
	assert.Error(t, err)

	err = APP(errors.New("error message"))
	assert.Equal(t, "error message", err.Error())
	assert.Error(t, err)

	err = APP(99)
	assert.Equal(t, "99", err.Error())
	assert.Error(t, err)

	err = APP(99.5)
	assert.Equal(t, "99.5", err.Error())
	assert.Error(t, err)

	err = APP("error APP", errors.New("previous error"))
	testAPPStack(t, err, "previous error", "", 0, 0)
	testAPPStack(t, err, "error APP", currentFile, 49, 1)
}

func TestAPI(t *testing.T) {
	status := 499
	message := "error message"

	err := API(status, message)
	testAPIError(t, err, status, message)

	err = API(status, errors.New(message))
	testAPIError(t, err, status, message)

	err = API(status, 99)
	testAPIError(t, err, status, "99")

	err = API(status, 99.5)
	testAPIError(t, err, status, "99.5")

	err = API(499, "error API", errors.New("previous error"))
	testAPIStack(t, err, "previous error", "", 0, 0)
	testAPIStack(t, err, "error API", currentFile, 70, 1)
}

func testAPIError(t *testing.T, err error, expectedStatus int, expectedMessage string) {
	expectedError := fmt.Sprintf("status: %d | message: %s", expectedStatus, expectedMessage)

	assert.Equal(t, expectedError, err.Error())
	assert.Error(t, err)

	apiError, ok := err.(APIError)
	assert.True(t, ok)
	assert.Equal(t, expectedStatus, apiError.Status())
	assert.Equal(t, expectedMessage, apiError.Message())
}

func testErrorStack(t *testing.T, err error, message, file string, line, stackIndex int) {
	stderror, ok := err.(Error)
	assert.True(t, ok)

	stack := stderror.Stack()

	if !t.Failed() {
		testStack(t, stack, message, file, line, stderror.SprintStack(), stackIndex)
	}
}

func testAPPStack(t *testing.T, err error, message, file string, line, stackIndex int) {
	stderror, ok := err.(APPError)
	assert.True(t, ok)

	stack := stderror.Stack()

	if !t.Failed() {
		testStack(t, stack, message, file, line, stderror.SprintStack(), stackIndex)
	}
}

func testAPIStack(t *testing.T, err error, message, file string, line, stackIndex int) {
	stderror, ok := err.(APIError)
	assert.True(t, ok)

	stack := stderror.Stack()

	if !t.Failed() {
		testStack(t, stack, message, file, line, stderror.SprintStack(), stackIndex)
	}
}

func testStack(t *testing.T, stack []StackItem, message, file string, line int, printStack string, stackIndex int) {
	item := stack[stackIndex]

	assert.Equal(t, message, item.Error())
	assert.Contains(t, item.File, file)
	assert.Equal(t, line, item.Line)
	assert.Contains(t, item.String(), message)
	assert.Contains(t, item.String(), file)
	assert.Contains(t, item.String(), fmt.Sprintf("%v", line))

	assert.Contains(t, printStack, fmt.Sprintf("[%d]", stackIndex))
	assert.Contains(t, printStack, message)
	assert.Contains(t, printStack, file)
	assert.Contains(t, printStack, fmt.Sprintf("%v", line))
}

// Package phone é um pacote com funções específicas para formatação de telefones.
package phone

import (
	"regexp"
	"strings"
)

// regex para encontrar tudo, EXCETO números, hífens e espaços.
var regexPhone = regexp.MustCompile(`[^0-9\-\s]`)

// Short formata um telefone para um formato mais compacto.
//
//  tel := Short("(69) 1234-5678")   // "69 1234-5678"
//  tel := Short("(69) 91234-5678")  // "69 912345678"
//
func Short(phone string) string {
	phone = regexPhone.ReplaceAllString(phone, "")

	if len(phone) > 12 {
		phone = strings.Replace(phone, "-", "", -1)
	}

	return phone
}

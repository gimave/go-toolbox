// Package database disponibiliza métodos para acessar as conexões com o banco de dados.
//
// Exemplos:
//
//  // executando um select no banco slave padrão (leitura) - opção parseTime desativada
//  database.Slave().Select(...)
//
//  // executando um select no banco slave identificado por "relatorio" (leitura) - opção parseTime desativada
//  database.Use("relatorio").Slave().Select(...)
//
//  // executando um select no banco slave identificado por "relatorio" (leitura) - opção parseTime ativada
//  database.Use("relatorio").ParseTime(true).Slave().Select(...)
//
//  // executando um get no banco slave padrão (leitura) - opção parseTime ativada
//  database.ParseTime(true).Slave().Get(...)
//
//  // executando um insert/update/delete no banco master default(escrita)
//  database.Master().Exec(...)
//
//  // executando um insert/update/delete num banco master identificado por "relatorio" (escrita)
//  database.Use("relatorio").Master().Exec(...)
//
// Para maiores informações sobre os métodos, leia a documentação do sqlx:
//  http://jmoiron.github.io/sqlx/
//
package database

import (
	"log"
	"strings"
	"sync"

	"github.com/jmoiron/sqlx"
)

const (
	// Default - conexão padrão com o banco de dados.
	Default = "default"
)

var (
	// armazena todas as connections(pool de conexões) disponíveis.
	allConnections = make(map[string]connections, 0)
	configured     bool
	once           sync.Once
)

type connections map[string]map[bool]*sqlx.DB

// Configure usado para configurar o pacote.
// Esse método deve ser chamado somente uma vez na inicialização da API.
func Configure(master, slave, masterParseTime, slaveParseTime *sqlx.DB) {
	once.Do(func() {
		configured = true
		Add(Default, master, slave, masterParseTime, slaveParseTime)
	})
}

// Add adiciona uma conexão com o banco de dados. O parâmetro 'name' define
// o identificador da conexão; se esse parâmetro for vazio, o package irá
// entender que é a conexão 'default' (padrão).
func Add(name string, master, slave, masterParseTime, slaveParseTime *sqlx.DB) {
	var mu sync.Mutex

	mu.Lock()
	defer mu.Unlock()

	// é obrigatório chamar o método 'Configure' para configurar as conexões 'default' antes
	// adicionar outros tipos de conexão.
	if !configured {
		log.Panic("database: é necessário chamar o método 'Configure' antes de executar o método 'Add'")
	}

	name = strings.ToLower(name)
	if Exists(name) {
		log.Panicf("database: as conexões '%s' já foram configuradas", name)
	}

	allConnections[name] = connections{
		"master": map[bool]*sqlx.DB{
			false: master,
			true:  masterParseTime,
		},
		"slave": map[bool]*sqlx.DB{
			false: slave,
			true:  slaveParseTime,
		},
	}
}

// Exists retorna se existe uma conexão identificada pelo parâmetro 'name'.
func Exists(name string) bool {
	_, ok := allConnections[strings.ToLower(name)]
	return ok
}

// Use usa a conexão identicada pelo parâmetro 'name'.
func Use(name string) *Connection {
	return &Connection{name: strings.ToLower(name)}
}

// ParseTime configura se a conexão será com a opção parseTime ativa ou não.
// Quando a opção parseTime estiver ativa, os valores timestamp serão convertidos
// para o objeto time.Time; caso contrário timestamp serão convertidos para string.
func ParseTime(option bool) *Connection {
	return Use(Default).ParseTime(option)
}

// Master retorna o pool de conexões com o banco de dados master(escrita).
func Master() *sqlx.DB {
	return Use(Default).ParseTime(false).Master()
}

// Slave retorna o pool de conexões com o banco de dados slave(leitura).
func Slave() *sqlx.DB {
	return Use(Default).ParseTime(false).Slave()
}

// Connection um pool de conexões com o banco de dados.
type Connection struct {
	name      string
	parseTime bool
}

// ParseTime configura se a conexão será com a opção parseTime ativa ou não.
// Quando a opção parseTime estiver ativa, os valores timestamp serão convertidos
// para o objeto time.Time; caso contrário timestamp serão convertidos para string.
func (c *Connection) ParseTime(option bool) *Connection {
	var mu sync.Mutex

	mu.Lock()
	defer mu.Unlock()

	c.parseTime = option
	return c
}

// Master retorna o pool de conexões com o banco de dados master(escrita).
func (c *Connection) Master() *sqlx.DB {
	return c.get("master")
}

// Slave retorna o pool de conexões com o banco de dados slave(leitura).
func (c *Connection) Slave() *sqlx.DB {
	return c.get("slave")
}

// get obtém um pool de conexões de acordo com o tipo informado no parâmetro 'kind'.
func (c *Connection) get(kind string) *sqlx.DB {
	// filtra as conexões pelo nome ('name')
	if connByName, ok := allConnections[c.name]; ok {
		// filtra as conexões pelo tipo ('kind')
		if connByKind, ok := connByName[kind]; ok {
			// filtra a conexão pelo opção parseTime
			if connByParseTime, ok := connByKind[c.parseTime]; ok {
				return connByParseTime
			}
		}
	}

	log.Panicf("database: não foi encontrada uma conexão com o banco de dados {name: %s, kind: %v, parseTime: %v}",
		c.name, kind, c.parseTime)

	return nil
}

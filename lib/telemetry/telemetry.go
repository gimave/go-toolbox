// Package telemetry coleta várias informações da app. As informações são usadas para fazer request-tracing,
// encontrar erros, rastrear o tempo de processamento de uma funcão, etc.
//
// Exemplo:
//    telemetry.Configure("api-exemplo-v1", telemetry.ModeDebug, true, telemetry.NewFileStorage("/var/log/telemetry/"))
//
//    t := telemetry.New()
//    in := telemetry.Args{
//        "x": 0,
//        "y": 1,
//    }
//    out := telemetry.Args{
//        "z": true,
//        "err": &err,
//    }
//    defer t.TracingFunc().Input(in).Output(out)
//
package telemetry

import (
	"encoding/json"
	"io"
	"log"
	"os"
	"strings"
	"sync"
	"time"

	"bitbucket.org/eucatur/go-toolbox/lib/id"
)

const (
	// ModeDebug - todas as informações serão coletadas.
	ModeDebug = "debug"
	// ModeError - as informações só serão coletadas em caso de erro.
	ModeError = "error"
)

var (
	config                = packageConfig{storage: os.Stdout}
	telemetryNullInstance = new(telemetryNull)
)

// packageConfig as opções de configurações do pacote.
type packageConfig struct {
	app        string
	mode       string
	enabled    bool
	storage    io.Writer
	configured bool
}

// Args argumentos genéricos usados pelo pacote.
type Args map[string]interface{}

// Configure configura como o pacote irá se comportar.
//   app: define a app que está gerando as informações de telemetria.
//   mode: define o modo como o pacote irá funcionar (debug ou error).
//   enabled: define se o pacote está habilitado ou não.
//   storage: serviço responsável pelo armazenamento das informações.
func Configure(app string, mode string, enabled bool, storage io.Writer) {
	var mu sync.Mutex

	mu.Lock()
	defer mu.Unlock()

	if config.configured {
		log.Panic("telemetry: o método 'Configure' já foi chamado")
	}

	mode = strings.ToLower(mode)
	switch mode {
	case ModeDebug, ModeError:
	default:
		log.Panic("telemetry: o parâmetro 'mode' deve ser 'ModeDebug' ou 'ModeError'")
	}

	// Se o pacote está desabilitado, define que os.Stdout será o storage.
	// Dessa forma, nós evitamos inicializar algum serviço de storage que não será usado.
	if !enabled {
		storage = os.Stdout
	}

	config = packageConfig{
		app:     app,
		mode:    mode,
		enabled: enabled,
		storage: storage,

		configured: true,
	}
}

// New retorna uma nova instância de um objeto de telemetria.
// Se o pacote estiver habilitado, retorna a telemetria padrão.
// Caso o pacote esteja desabilitado, retorna uma instância nula de
// telemetria (essa instância é otimizada para não consumir nenhum recurso).
func New() Telemetry {
	if config.enabled {
		return newTelemetryDefault()
	}

	return newTelemetryNull()
}

// Telemetry a interface que define um serviço de telemetria.
type Telemetry interface {
	// SetID sobreescreve o id padrão da telemetria.
	SetID(id string) Telemetry
	// SetSession define as informações da sessão.
	SetSession(session Args) Telemetry
	// SetTags define as tags para a telemetria.
	SetTags(tags Args) Telemetry
	// TracingFunc retorna um serviço para rastreamento de funções.
	TracingFunc() Tracing
	// SendDataToStorage envia os dados coletados para o storage.
	SendDataToStorage(telemetryType, dataType string, dataValue Args)
}

// telemetryNull é um serviço nulo de telemetria.
// Esse serviço é usado quando o package está desabilitado; o serviço
// foi otimizado para não consumir nenhum recurso de processamento.
type telemetryNull int

func newTelemetryNull() Telemetry {
	return telemetryNullInstance
}

func (t *telemetryNull) SetID(id string) Telemetry {
	return t
}

func (t *telemetryNull) SetSession(session Args) Telemetry {
	return t
}

func (t *telemetryNull) SetTags(tags Args) Telemetry {
	return t
}

func (t *telemetryNull) TracingFunc() Tracing {
	return tracingNullInstance
}

func (t *telemetryNull) SendDataToStorage(telemetryType, dataType string, dataValue Args) {
	return
}

// telemetryDefault é um serviço padrão de telemetria.
// Esse serviço é usado quando o package está habilitado.
type telemetryDefault struct {
	data Args
}

func newTelemetryDefault() Telemetry {
	data := Args{
		"id":      id.MustGenerate(id.Options{Type: "uuid", Fallback: "number"}),
		"session": Args{},
		"tags":    Args{},
	}

	return &telemetryDefault{data: data}
}

func (t *telemetryDefault) SetID(id string) Telemetry {
	t.data["id"] = id
	return t
}

func (t *telemetryDefault) SetSession(session Args) Telemetry {
	t.data["session"] = session
	return t
}

func (t *telemetryDefault) SetTags(tags Args) Telemetry {
	t.data["tags"] = tags
	return t
}

func (t *telemetryDefault) TracingFunc() Tracing {
	return newTracingFunc(t)
}

func (t *telemetryDefault) SendDataToStorage(telemetryType, dataType string, dataValue Args) {
	go func(app string, global Args, telemetryType, dataType string, dataValue Args) {
		data := Args{
			"app":    app,
			"type":   telemetryType,
			"time":   time.Now(),
			"global": global,
			dataType: dataValue,
		}

		jsonData, err := marshalJSON(data)
		if err == nil {
			config.storage.Write(jsonData)
		}
	}(config.app, t.data, telemetryType, dataType, dataValue)
}

func marshalJSON(data Args) ([]byte, error) {
	return json.Marshal(Args{"telemetry": data})
}

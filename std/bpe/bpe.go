package bpe

// DeTipoBilheteParaDescontoBPe recebe TipoBilhete (TipoBilh) e retorno o tipo
// e a descrição desconto conforme manual
func DeTipoBilheteParaDescontoBPe(tipoBilhete string) (string, string) {

	switch tipoBilhete {
	case "01" /*normal*/, "02" /*requisitada*/, "03" /*alp*/, "07" /*confirmação*/, "12" /*televenda*/, "15" /*internet*/ :
		return "01", "Tarifa Promocional"
	case "11":
		return "02", "Idoso"
	case "08":
		return "03", "Criança"
	case "06":
		return "04", "Deficiente"
	case "14":
		return "05", "Estudante"
	case "09" /*passe auditor fiscal*/, "16" /*militar*/ :
		return "08", "Profissional em Deslocamento"
	case "05" /*passe livre*/ :
		return "09", "Profissional da Empresa"
	case "10":
		return "10", "Jovem"
	default:
		return "99", "Outros"
	}
	// "06" - animal doméstico, "07" - acordo coletivo
}

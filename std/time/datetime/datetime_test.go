package datetime

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func datetimeref() time.Time {
	ref, _ := time.Parse("2006-01-02 15:04:05", "2017-12-31 20:30:45")
	return ref
}

func TestMustParseEUA(t *testing.T) {
	assert := assert.New(t)

	assert.Panics(func() { MustParseEUA("datetime to parse") })
	assert.Panics(func() { MustParseEUA("2017-31-17 20:30:35") })

	dt := MustParseEUA("2017-12-31 20:30:45")
	assert.Equal(datetimeref(), dt)
}

func TestParseEUA(t *testing.T) {
	dt, err := ParseEUA("2017-12-31 20:30:45")
	assert.Equal(t, datetimeref(), dt)
	assert.Nil(t, err)

	dt, err = ParseEUA("2017-31-17 20:30:45")
	assert.True(t, dt.IsZero())
	assert.NotNil(t, err)
}

func TestParseBR(t *testing.T) {
	dt, err := ParseBR("31/12/2017 20:30:45")
	assert.Equal(t, datetimeref(), dt)
	assert.Nil(t, err)

	dt, err = ParseBR("17/31/2017 20:30:45")
	assert.True(t, dt.IsZero())
	assert.NotNil(t, err)
}

func TestParseBRShort(t *testing.T) {
	ref, _ := time.Parse("2006-01-02 15:04", "2017-12-31 20:30")

	dt, err := ParseBRShort("31/12/2017 20:30")
	assert.Equal(t, ref, dt)
	assert.Nil(t, err)

	dt, err = ParseBRShort("17/31/2017 20:30")
	assert.True(t, dt.IsZero())
	assert.NotNil(t, err)
}

package redis

import (
	"errors"
	"time"

	"bitbucket.org/eucatur/go-toolbox/std/convert"

	"github.com/garyburd/redigo/redis"
)

var (
	// ErrNilValue usado quando a informação buscada não existe no redis.
	ErrNilValue = errors.New("redis: nenhum valor encontrado")
)

// NewStorage retorna uma nova instância de Storage.
func NewStorage(prefix string) *Storage {
	return &Storage{
		prefix: prefix,
	}
}

// Storage é uma abstração criada em cima do redis; esse service implementa as
// funções mais básicas de um serviço de armazenamento(salvar, buscar e excluir).
// Obs: Esse serviço converte todos os valores para string antes de salvar.
type Storage struct {
	prefix string
}

// key faz a composição da chave final: prefixo + chave.
func (s Storage) key(key string) string {
	return s.prefix + "." + key
}

// Set salva uma informação no redis.
//
//  // armazena o valor "INFO" no redis com a chave "key" por 5 minutos.
//  err := Set("key", "INFO", time.Duration(5) * time.Minute)
//
func (s Storage) Set(key string, value interface{}, expiration time.Duration) error {
	var (
		valueString string
		err         error
	)

	if err = convert.From(value).To(&valueString); err != nil {
		return err
	}

	_, err = Exec(func(redisConn redis.Conn) (interface{}, error) {
		if _, err = redisConn.Do("SET", s.key(key), valueString); err != nil {
			return nil, err
		}

		expirationInSeconds := int64(expiration / time.Second)

		_, err = redisConn.Do("EXPIRE", s.key(key), expirationInSeconds)
		return nil, err
	})

	return err
}

// Get busca uma informação no redis.
//
//  // busca no redis uma informação pela chave "key", se existir a informação será
//  // gravada na variável resultado. Se não existir, um erro será retornado.
//  err := Get("key", &resultado)
//
func (s Storage) Get(key string, value interface{}) error {
	reply, err := Exec(func(redisConn redis.Conn) (interface{}, error) {
		return redisConn.Do("GET", s.key(key))
	})

	if err != nil {
		return err
	}

	if reply == nil {
		return ErrNilValue
	}

	return convert.From(reply).To(value)
}

// Del remove uma informação do redis.
//
//  err := Del("key")
//
func (s Storage) Del(key string) error {
	var err error

	_, err = Exec(func(redisConn redis.Conn) (interface{}, error) {
		return redisConn.Do("DEL", s.key(key))
	})

	return err
}

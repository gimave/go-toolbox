// Package debugger um pacote simples para fazer o debug via mensagens dentro da app.
package debugger

import (
	"fmt"
	"io"
	"runtime"
	"runtime/debug"
	"strings"
	"time"

	"bitbucket.org/eucatur/go-toolbox/std/net"
)

// a instância global de Debugger.
var instance *Debugger

// New retorna uma nova instância de Debugger.
func New(writer io.Writer) *Debugger {
	return &Debugger{true, writer}
}

// On liga o debugger e configura para escrever as mensagens em 'writer'.
// Se o debugger já estiver habilitado, o 'writer' não será alterado.
func On(writer io.Writer) {
	if instance == nil {
		instance = New(writer)
		return
	}

	if !IsEnabled() {
		instance.writer = writer
		instance.enabled = true
	}
}

// Off desliga o debugger, a partir desse momento o debugger deixa de escrever
// as próximas mensagens.
func Off() {
	if instance != nil {
		instance.enabled = false
	}
}

// IsEnabled retorna se o debugger está ligado ou desligado.
func IsEnabled() bool {
	return instance != nil && instance.enabled
}

// FileLine escreve uma mensagem para debug e inclue o arquivo e a
// linha que gerou a mensagem.
func FileLine(content interface{}) {
	if instance != nil {
		instance.FileLine(content)
	}
}

// Stack escreve uma mensagem para debug e inclue todo o stack de execução
// até o momento da geração da mensagem.
func Stack(content interface{}) {
	if instance != nil {
		instance.Stack(content)
	}
}

// Debug escreve uma mensagem para debug.
func Debug(content interface{}) {
	if instance != nil {
		instance.Debug(content)
	}
}

// Debugf escreve uma mensagem para debug usando formatação.
func Debugf(format string, content ...interface{}) {
	if instance != nil {
		instance.Debugf(format, content...)
	}
}

// Debugger responsável por escrever as mensagens de debug.
type Debugger struct {
	enabled bool
	writer  io.Writer
}

// FileLine escreve uma mensagem para debug e inclue o arquivo e a
// linha que geraram a mensagem.
func (d *Debugger) FileLine(content interface{}) {
	if d.enabled {
		_, file, line, _ := runtime.Caller(1)

		if strings.Contains(file, "go-toolbox/lib/debugger/debugger.go") {
			_, file, line, _ = runtime.Caller(2)
		}

		d.print(fmt.Sprintf("%v\nfile: %s:%d\n\n", content, file, line))
	}
}

// Stack escreve uma mensagem para debug e inclue todo o stack de execução
// até o momento da geração da mensagem.
func (d *Debugger) Stack(content interface{}) {
	if d.enabled {
		d.print(fmt.Sprintf("%v\nSTACK:%s\n\n", content, debug.Stack()))
	}
}

// Debug escreve uma mensagem para debug.
func (d *Debugger) Debug(content interface{}) {
	if d.enabled {
		d.print(fmt.Sprintf("%v\n\n", content))
	}
}

// Debugf escreve uma mensagem para debug usando formatação.
func (d *Debugger) Debugf(format string, content ...interface{}) {
	if d.enabled {
		msg := fmt.Sprintf(format, content...)
		d.Debug(msg)
	}
}

func (d *Debugger) print(message string) {
	if d.enabled {
		ip, _ := net.GetLocalIP()
		fmt.Fprintf(d.writer, "[DEBUG %s %s]: %s", ip, time.Now().Format(time.RFC3339), message)
	}
}

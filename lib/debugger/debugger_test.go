package debugger

import (
	"bytes"
	"errors"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

// Essa função testa o uso do debugger usando a instância global
// criada dentro do package.
func TestUsingPackage(t *testing.T) {
	// onde as mensagens de debugger serão gravadas durante os testes
	writer := new(bytes.Buffer)

	// liga o debugger e configura para escrever no writer
	On(writer)
	assert.True(t, IsEnabled())

	// grava uma mensagem para debug
	Debug("message")
	assert.True(t, strings.Contains(writer.String(), "message"))
	writer.Reset()

	// grava várias mensagens de debug
	Debug("message: 1")
	Debug(99)
	Debug(99.5)
	Debug(errors.New("message: err"))
	Debugf("message: %d", 2)
	Debugf("message: %d | %s", 3, "OK")
	assert.True(t, strings.Contains(writer.String(), "message: 1"))
	assert.True(t, strings.Contains(writer.String(), "99"))
	assert.True(t, strings.Contains(writer.String(), "99.5"))
	assert.True(t, strings.Contains(writer.String(), "message: err"))
	assert.True(t, strings.Contains(writer.String(), "message: 2"))
	assert.True(t, strings.Contains(writer.String(), "message: 3 | OK"))
	writer.Reset()

	FileLine("message: 1")
	msg := "bitbucket.org/eucatur/go-toolbox/lib/debugger/debugger_test.go:42"
	if !strings.Contains(writer.String(), msg) {
		msg = "bitbucket.org/eucatur/go-toolbox/lib/debugger/_test/_obj_test/debugger.go:54"
	}

	assert.Contains(t, writer.String(), msg)
	writer.Reset()

	Stack("message: 1")
	assert.Contains(t, writer.String(), "bitbucket.org/eucatur/go-toolbox/lib/debugger.(*Debugger).Stack")
	assert.Contains(t, writer.String(), "bitbucket.org/eucatur/go-toolbox/lib/debugger/debugger_test.go:51")
	writer.Reset()

	// desliga o debugger
	Off()
	assert.False(t, IsEnabled())

	// como o debugger está desligado, nada será gravado
	Debug("message")
	assert.Empty(t, writer.String())

	Debug("message: 1")
	Debugf("message: %d", 2)
	assert.Empty(t, writer.String())
	writer.Reset()

	// liga novamente o debugger
	On(writer)
	assert.True(t, IsEnabled())

	// grava um erro
	Debug(errors.New("message"))
	assert.True(t, strings.Contains(writer.String(), "message"))
}

// Essa função testa o uso do debugger usando uma instância
// criada dinamicamente (sem fazer uso da instância global).
func TestUsingInstance(t *testing.T) {
	// onde as mensagens de debugger serão gravadas durante os testes
	writer := new(bytes.Buffer)

	// instância o debugger
	obj := New(writer)

	// grava uma mensagem para debug
	obj.Debug("message")
	assert.True(t, strings.Contains(writer.String(), "message"))
	writer.Reset()

	// grava várias mensagens de debug
	obj.Debug("message: 1")
	obj.Debug(99)
	obj.Debug(99.5)
	obj.Debug(errors.New("message: err"))
	obj.Debugf("message: %d", 2)
	obj.Debugf("message: %d | %s", 3, "OK")
	assert.True(t, strings.Contains(writer.String(), "message: 1"))
	assert.True(t, strings.Contains(writer.String(), "99"))
	assert.True(t, strings.Contains(writer.String(), "99.5"))
	assert.True(t, strings.Contains(writer.String(), "message: err"))
	assert.True(t, strings.Contains(writer.String(), "message: 2"))
	assert.True(t, strings.Contains(writer.String(), "message: 3 | OK"))
	writer.Reset()

	obj.FileLine("message: 1")
	assert.Contains(t, writer.String(), "bitbucket.org/eucatur/go-toolbox/lib/debugger/debugger_test.go:107")
	writer.Reset()

	obj.Stack("message: 1")
	assert.Contains(t, writer.String(), "bitbucket.org/eucatur/go-toolbox/lib/debugger.(*Debugger).Stack")
	assert.Contains(t, writer.String(), "bitbucket.org/eucatur/go-toolbox/lib/debugger/debugger_test.go:111")
	writer.Reset()
}

// Package debuggerutil fornece métodos úteis para geração de mensagens para debug.
package debuggerutil

import (
	"fmt"
	"strings"

	"github.com/fatih/color"
)

var (
	green  = color.New(color.FgGreen).SprintFunc()
	cyan   = color.New(color.FgCyan).SprintFunc()
	yellow = color.New(color.FgYellow).SprintFunc()

	lineSeparator = strings.Repeat("-", 90) + "\n"
)

// Title formata o título da mensagem.
func Title(title string) string {
	return fmt.Sprintf("\n %s\n", green(">> "+title))
}

// Header formata o cabeçalho da mensagem.
func Header(items ...string) string {
	header := lineSeparator + " "
	for i, v := range items {
		header += cyan(v)
		if len(items) > i+1 {
			header += " | "
		}
	}
	header += "\n" + lineSeparator
	return header
}

// Subtitle formata o subtítulo da mensagem.
func Subtitle(subtitle string) string {
	return lineSeparator + breakLine(subtitle) + lineSeparator
}

// Content formata o conteúdo da mensagem.
func Content(content string) string {
	return breakLine(content) + lineSeparator
}

// Contentf formata o conteúdo da mensagem usando sprintf.
func Contentf(format string, content ...interface{}) string {
	return Content(fmt.Sprintf(format, content...))
}

// Warning formata a mensagem como um aviso.
func Warning(message string) string {
	return lineSeparator + breakLine(yellow(message)) + lineSeparator
}

// LineSeparator retorna o separador de linhas padrão.
func LineSeparator() string {
	return lineSeparator
}

func breakLine(content string) string {
	if !strings.HasSuffix(content, "\n") {
		content += "\n"
	}
	return content
}

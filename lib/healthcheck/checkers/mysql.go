package checkers

import (
	"bitbucket.org/eucatur/go-toolbox/lib/database"

	"github.com/dimiro1/health"
	"github.com/dimiro1/health/db"
)

// NewMysqlMaster retorna um novo checker para o banco de dados master.
func NewMysqlMaster() health.Checker {
	return db.NewMySQLChecker(database.Master().DB)
}

// NewMysqlSlave retorna um novo checker para o banco de dados slave.
func NewMysqlSlave() health.Checker {
	return db.NewMySQLChecker(database.Slave().DB)
}

// Package to read json file end set into enviroment variables
package json2env

import (
	"testing"
)

func TestLoadFile(t *testing.T) {
	err := LoadFile("config.json")
	if err != nil {
		t.Errorf(err.Error())
	}
}

package cache

import (
	"time"

	"bitbucket.org/eucatur/go-toolbox/app/env"
	libredis "bitbucket.org/eucatur/go-toolbox/lib/redis"
)

// Redis é o serviço de cache que armazena as informações no redis.
type Redis struct {
	// se a flag 'API_CACHE_HABILITADO' está habilitada ou não.
	flagEnabled bool
	// conexão com o redis para armazenamento dos dados.
	storage *libredis.Storage
}

// NewRedis cria uma nova instância de Redis.
//
//  // inicia o cache usando o prefix "api-services.cache.seccionamentos".
//  service := cache.NewRedis("api-services.cache.seccionamentos")
//
func NewRedis(prefix string) *Redis {
	return &Redis{
		flagEnabled: env.Bool("API_CACHE_HABILITADO", false),
		storage:     libredis.NewStorage(prefix),
	}
}

// IsEnabled retorna se o serviço de cache está habilitado.
//
//  habilitado := service.IsEnabled()
//
func (r Redis) IsEnabled() bool {
	return r.flagEnabled
}

// Set salva uma informação no cache.
//
//  // armazena o valor "INFO" no redis com a chave "key" por 2 minutos.
//  err := service.Set("key", "INFO", time.Duration(2) * time.Minute)
//
func (r Redis) Set(key string, value interface{}, expiration time.Duration) error {
	if r.IsEnabled() {
		return r.storage.Set(key, value, expiration)
	}

	return ErrServiceDisabled
}

// Get busca uma informação no cache.
//
//  // busca no cache uma informação pela chave "key", se existir a informação será
//  // gravada na variável resultado. Se não existir, um erro será retornado.
//  err := service.Get("key", &resultado)
//
func (r Redis) Get(key string, value interface{}) error {
	if r.IsEnabled() {
		err := r.storage.Get(key, value)
		if err == libredis.ErrNilValue {
			err = ErrNotCached
		}
		return err
	}

	return ErrServiceDisabled
}

// Del remove uma informação do cache.
//
//  err := service.Del("key")
//
func (r Redis) Del(key string) (err error) {
	if r.IsEnabled() {
		return r.storage.Del(key)
	}

	return ErrServiceDisabled
}

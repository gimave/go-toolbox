package types

// Bool representação de um tipo boolean que permite usar
// um valor de inicialização vazio(-1).
type Bool int

// IsFalse retorna se o valor do boolean é falso.
func (b Bool) IsFalse() bool {
	return b == 0
}

// IsTrue retorna se o valor do boolean é verdadeiro.
func (b Bool) IsTrue() bool {
	return b == 1
}

// Value retorna se o valor do boolean é falso ou verdadeiro.
func (b Bool) Value() bool {
	return b == 1
}

// Empty retorna se o valor do boolean é vazio.
func (b Bool) Empty() bool {
	return b != 0 && b != 1
}

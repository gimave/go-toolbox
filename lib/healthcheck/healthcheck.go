// Package healthcheck um pacote para "checar a saúde" dos componentes
// e serviços usados por uma app.
package healthcheck

import (
	"github.com/dimiro1/health"
)

// Map um mapa dos checkers.
type Map map[string]health.Checker

// New retorna uma nova instância de HealthChecker.
func New() *HealthChecker {
	return &HealthChecker{health.NewCompositeChecker()}
}

// Make monta uma instância de HealthChecker com os checkers para alguns
// componentes já configurados.
func Make(checkers Map) *HealthChecker {
	health := New()
	for description, checker := range checkers {
		health.AddChecker(description, checker)
	}
	return health
}

// HealthChecker serviço responsável pela "checagem da saúde" da app.
type HealthChecker struct {
	checker health.CompositeChecker
}

// AddChecker adiciona uma checagem de um novo componente da app.
func (h *HealthChecker) AddChecker(description string, checker health.Checker) *HealthChecker {
	h.checker.AddChecker(description, checker)
	return h
}

// Check realiza o processo de checagem em todos os componentes configurados.
func (h *HealthChecker) Check() health.Health {
	return h.checker.Check()
}

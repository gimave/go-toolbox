package healthcheck_test

import (
	"fmt"

	"bitbucket.org/eucatur/go-toolbox/lib/healthcheck"
	"github.com/dimiro1/health"
)

// A struct abaixo é um exemplo de implementação de um checker.
// Um checker é responsável por testar "a saúde" de um componente
// ou de um serviço utilizado pela app.

// MemoryChecker simulação de um checker que testa o consumo
// de memória de um serviço.
type MemoryChecker struct {
	Used float64
}

// Check método que retorna "a saúde" do serviço.
func (m *MemoryChecker) Check() health.Health {
	result := health.NewHealth()

	// nessa simulação, se a memória usada for maior que 90%, nós
	// simulamos que o serviço "caiu".
	if m.Used > 90 {
		result.Down().AddInfo("memory", "consumo excessivo de memória")

	} else { // caso contrário, informamos que está tudo OK.
		result.Up().AddInfo("memory", "OK")
	}

	return result
}

func Example() {
	health := healthcheck.New()

	health.AddChecker("server-memory-1", &MemoryChecker{75})
	result := health.Check()
	fmt.Println(result.IsUp())

	health.AddChecker("server-memory-2", &MemoryChecker{95})
	result = health.Check()
	fmt.Println(result.IsUp())

	// Output:
	// true
	// false
}

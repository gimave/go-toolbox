package date

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func dateref() time.Time {
	ref, _ := time.Parse("2006-01-02", "2017-12-31")
	return ref
}

func TestMustParseBR(t *testing.T) {
	assert := assert.New(t)

	assert.Panics(func() { MustParseBR("date to parse") })
	assert.Panics(func() { MustParseBR("17/31/2017") })

	dt := MustParseBR("31/12/2017")
	assert.Equal(dateref(), dt)
}

func TestParseBR(t *testing.T) {
	dt, err := ParseBR("31/12/2017")
	assert.Equal(t, dateref(), dt)
	assert.Nil(t, err)

	dt, err = ParseBR("17/31/2017")
	assert.True(t, dt.IsZero())
	assert.NotNil(t, err)
}

func TestMustParseEUA(t *testing.T) {
	assert := assert.New(t)

	assert.Panics(func() { MustParseEUA("date to parse") })
	assert.Panics(func() { MustParseEUA("2017-17-31") })

	dt := MustParseEUA("2017-12-31")
	assert.Equal(dateref(), dt)
}

func TestParseEUA(t *testing.T) {
	dt, err := ParseEUA("2017-12-31")
	assert.Equal(t, dateref(), dt)
	assert.Nil(t, err)

	dt, err = ParseEUA("2017-31-17")
	assert.True(t, dt.IsZero())
	assert.NotNil(t, err)
}

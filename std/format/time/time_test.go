package time

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestHourMin(t *testing.T) {
	tempo := 75 * time.Minute // 01 hora e 15 minutos
	assert.Equal(t, "01:15", HourMin(tempo))

	tempo = 45 * time.Minute // 45 minutos
	assert.Equal(t, "00:45", HourMin(tempo))

	tempo = 30 * time.Second // 30 segundos
	assert.Equal(t, "00:00", HourMin(tempo))
}

func TestDefault(t *testing.T) {
	ref, _ := time.Parse("2006-01-02 15:04:05", "2017-12-31 20:30:45")
	assert.Equal(t, "20:30:45", Default(ref))
}

func TestShort(t *testing.T) {
	assert.Equal(t, "20:30", Short("20:30:45"))
}

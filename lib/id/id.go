// Package id disponibiliza métodos para gerenciamento de IDs.
package id

import (
	"errors"
	"fmt"
	"log"
	"time"

	generateuuid "github.com/satori/go.uuid"
)

const (
	uuid   = "uuid"
	number = "number"
)

// Options são as opções para geração de um ID.
type Options struct {
	// Type é o tipo principal para geração do ID.
	Type string

	// Fallback é a segunda opção de tipo para geração do ID.
	Fallback string
}

// Generate gera um ID. O ID será gerado no formato definido em options.Type,
// caso ocorra um erro será feita uma segunda tentativa de geração do ID
// usando o formato definido em option.Fallback.
//
// Exemplo:
//    id, err := id.Generate(id.Options{Type: "uuid", Fallback: "number"})
//
func Generate(options Options) (id string, err error) {
	switch options.Type {
	case uuid:
		var uuidV4 generateuuid.UUID

		if uuidV4, err = generateuuid.NewV4(); err != nil {
			if options.Fallback == "" {
				break
			}
			return Generate(Options{Type: options.Fallback})
		}

		id = uuidV4.String()
		return

	case number:
		id = fmt.Sprintf("%d", time.Now().UnixNano())
		return
	}

	err = errors.New("id: não foi possível gerar o id")
	return
}

// MustGenerate gera um ID. O ID será gerado no formato definido em options.Type,
// caso ocorra um erro será feita uma segunda tentativa de geração do ID
// usando o formato definido em option.Fallback. Se não for possível gerar um
// ID, um Panic será disparado.
//
// Exemplo:
//    id := id.MustGenerate(id.Options{Type: "uuid", Fallback: "number"})
//
func MustGenerate(options Options) (id string) {
	var err error

	if id, err = Generate(options); err != nil {
		log.Panic(err)
	}
	return
}

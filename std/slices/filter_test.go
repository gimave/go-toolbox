package slices

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFilter(t *testing.T) {
	ints := []interface{}{0, 1, 2, 3, 4, 5}

	result := Filter(ints, func(i interface{}) bool { return i.(int) > 2 })
	assert.Equal(t, []interface{}{3, 4, 5}, result)

	result = Filter(ints, func(i interface{}) bool { return i.(int) > 5 })
	assert.Equal(t, []interface{}{}, result)
}

func TestFilterInt(t *testing.T) {
	ints := []int{0, 1, 2, 3, 4, 5}

	result := FilterInt(ints, func(i int) bool { return i > 2 })
	assert.Equal(t, []int{3, 4, 5}, result)

	result = FilterInt(ints, func(i int) bool { return i > 5 })
	assert.Equal(t, []int{}, result)
}

func TestFilterFloat64(t *testing.T) {
	floats := []float64{0.5, 1.5, 2.5, 3.5, 4.5, 5.5}

	result := FilterFloat64(floats, func(f float64) bool { return f > 2 })
	assert.Equal(t, []float64{2.5, 3.5, 4.5, 5.5}, result)

	result = FilterFloat64(floats, func(f float64) bool { return f > 6 })
	assert.Equal(t, []float64{}, result)
}

func TestFilterString(t *testing.T) {
	strings := []string{"A", "B", "C", "D", "E", "F"}

	result := FilterString(strings, func(s string) bool { return s > "C" })
	assert.Equal(t, []string{"D", "E", "F"}, result)

	result = FilterString(strings, func(s string) bool { return s > "F" })
	assert.Equal(t, []string{}, result)
}

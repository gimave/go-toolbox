package authrequest_test

import (
	"bitbucket.org/eucatur/go-toolbox/lib/http/middleware/authrequest"

	"github.com/labstack/echo"
)

func ExampleByKeyService() {
	// define que a estratégia de autenticação será por chave.
	auth := authrequest.ByKey()
	// carrega as chaves de acesso de um arquivo no formato YAML ou JSON.
	// para YAML use:
	auth.LoadKeysFromFile(".api-keys")
	// para JSON use a extenção '.json' e o formato obviamente, ficando a assim:
	//auth.LoadKeysFromFile("api-keys.json")

	e := echo.New()

	// cria um grupo de rotas no echo com prefixo "admin"
	admin := e.Group("/admin")
	// somente os clientes presentes no grupo "admin" tem acesso
	admin.Use(auth.AllowAccessFor("admin"))

	// cria um grupo de rotas no echo com prefixo "api"
	api := e.Group("/api")
	// os clientes presentes nos grupos "admin" e "api" tem acesso
	api.Use(auth.AllowAccessFor("admin", "api"))

	e.GET("/public", func(ctx echo.Context) error {
		return ctx.String(200, "<public>")
	})

	admin.GET("/test", func(ctx echo.Context) error {
		return ctx.String(200, "<admin-test>")
	})

	api.GET("/test", func(ctx echo.Context) error {
		return ctx.String(200, "<api-test>")
	})
}
